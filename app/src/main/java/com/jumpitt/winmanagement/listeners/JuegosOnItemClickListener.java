package com.jumpitt.winmanagement.listeners;

import android.view.View;
import android.widget.AdapterView;

import com.jumpitt.winmanagement.dataModel.TipoJuegoDM;
import com.jumpitt.winmanagement.listeners.interfaces.OnGameSelectedListener;

public class JuegosOnItemClickListener implements AdapterView.OnItemClickListener {
    private OnGameSelectedListener listenerJuegos;

    public JuegosOnItemClickListener(OnGameSelectedListener listenerJuegos) {
        this.listenerJuegos = listenerJuegos;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        TipoJuegoDM selectedGame = (TipoJuegoDM) parent.getItemAtPosition(position);

        if (listenerJuegos != null) {
            listenerJuegos.onGameSelected(selectedGame);
        }
    }
}
