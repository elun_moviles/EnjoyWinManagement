package com.jumpitt.winmanagement.listeners;

import android.view.View;
import android.widget.AdapterView;

import com.jumpitt.winmanagement.dataModel.MesaDM;
import com.jumpitt.winmanagement.listeners.interfaces.OnTableSelectedListener;

public class MesasOnItemClickListener implements AdapterView.OnItemClickListener {
    private OnTableSelectedListener listenerMesas;

    public MesasOnItemClickListener(OnTableSelectedListener listenerMesas) {
        this.listenerMesas = listenerMesas;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        MesaDM selectedTable = (MesaDM) parent.getItemAtPosition(position);

        if (selectedTable != null) {
            selectedTable.setSelected(false);
        }

//        selectedTable = (MesaDM) parent.getItemAtPosition(position);
//        selectedTable.setSelected(false);

        listenerMesas.onTableSelected(selectedTable);
    }
}
