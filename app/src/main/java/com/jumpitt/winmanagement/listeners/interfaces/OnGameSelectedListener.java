package com.jumpitt.winmanagement.listeners.interfaces;

import com.jumpitt.winmanagement.dataModel.TipoJuegoDM;

public interface OnGameSelectedListener {
    public void onGameSelected(TipoJuegoDM juego);
}
