package com.jumpitt.winmanagement.listeners.interfaces;

import com.jumpitt.winmanagement.dataModel.MesaDM;

public interface OnTableSelectedListener {
    public void onTableSelected(MesaDM mesa);
}
