package com.jumpitt.winmanagement.databaseConnection;

import com.jumpitt.winmanagement.utils.GlobalData;
import com.orhanobut.logger.Logger;

public class ConnectionServer {
    public enum Environment { DESARROLLO, PRODUCTIVO, JUMPITT }

    private static String environment_type = GlobalData.EMPTY;

    // Conexion WebServices
    private static String IP_SERVER_DESARROLLO = "http://172.21.10.252"; // Desarrollo
    private static String IP_SERVER_PRODUCCION = "http://172.25.8.135"; // Productivo
    private static String IP_SERVER_JUMPITT = "http://jumpitt.ddns.net"; // Jumpitt

    private static String IPserver = GlobalData.EMPTY;
    private static String IPport = GlobalData.EMPTY;

    // WebService SQL Server
    public static String NAMESPACE = "http://webservices.enjoy.cl/";

    public static String URL_ws1 = GlobalData.EMPTY;
    public static String URL_ws_enjoy = GlobalData.EMPTY;
    public static String URL_ws_enjoy_comps = GlobalData.EMPTY;

    public static void setAppEnvironment(Environment environment) {
        environment_type = environment.toString();

        switch(environment) {
            case DESARROLLO:
                IPserver = IP_SERVER_DESARROLLO;
                IPport = ":80";
                URL_ws1 = IPserver + IPport + "/ws_enjoy_control_mesas/ServicioMesas.asmx";
//                URL_ws_enjoy = IPserver + IPport + "/ws_enjoy_control/ws_enjoy_control.asmx";
                URL_ws_enjoy = "http://172.25.8.135/ws_enjoycontrol_mcc/ws_enjoy_control.asmx";
                URL_ws_enjoy_comps = IPserver + IPport + "/ws_integrador/ServicioComps.asmx";
            break;

            case PRODUCTIVO:
                IPserver = IP_SERVER_PRODUCCION;
                IPport = ":80";
                URL_ws1 = IPserver + IPport + "/ws_enjoycontrol/ServicioMesas.asmx";
                URL_ws_enjoy = IPserver + IPport + "/ws_enjoycontrol_mcc/ws_enjoy_control.asmx";
                URL_ws_enjoy_comps = IPserver + IPport + "/ws_integrador/ServicioComps.asmx";
            break;

            case JUMPITT:
                IPserver = IP_SERVER_JUMPITT;
                IPport = ":81";
                URL_ws1 = IPserver + IPport + "/ServicioMesas.asmx";
                URL_ws_enjoy = GlobalData.EMPTY;
                URL_ws_enjoy_comps = GlobalData.EMPTY;
            break;
        }

        Logger.v("URL_ws1= " + URL_ws1 + "\n" +
                 "URL_ws_enjoy= " + URL_ws_enjoy + "\n" +
                 "URL_ws_enjoy_comps= " + URL_ws_enjoy_comps);
    }

    public static String getAppEnvironment() {
        return environment_type;
    }
}
