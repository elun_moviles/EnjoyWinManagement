package com.jumpitt.winmanagement.utils.controllers;

import android.app.Activity;
import android.app.Dialog;
import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.utils.interfaces.OnButtonPressedListener;
import com.jumpitt.winmanagement.utils.Utils;

public class ConfirmationDialogController implements View.OnClickListener {
    private Button btnAccept;
    private ImageView iconImg;
    private TextView confirmationTextView;
    private Dialog dialogConfirmOperation;
    private OnButtonPressedListener listener;

    public ConfirmationDialogController(Activity activity) {
        dialogConfirmOperation = Utils.getNewDialog(activity, R.layout.dialog_confirmacion_operacion, false, true, 600, ViewGroup.LayoutParams.MATCH_PARENT);

        iconImg = (ImageView) dialogConfirmOperation.findViewById(R.id.icon);
        confirmationTextView = (TextView) dialogConfirmOperation.findViewById(R.id.confirmationText);
        btnAccept = (Button) dialogConfirmOperation.findViewById(R.id.btnAccept);
        Button btnCancel = (Button) dialogConfirmOperation.findViewById(R.id.btnCancel);

        btnAccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    public void showConfirmationDialog(int icon, String confirmationText, OnButtonPressedListener onButtonPressedListener) {
        this.listener = onButtonPressedListener;

        if(icon != -1) iconImg.setImageResource(icon);
        else iconImg.setVisibility(View.GONE);
        confirmationTextView.setText(confirmationText);

        dialogConfirmOperation.show();
    }

    public void showConfirmationDialog(int icon, Spanned confirmationText, OnButtonPressedListener onButtonPressedListener) {
        this.listener = onButtonPressedListener;

        if(icon != -1) iconImg.setImageResource(icon);
        else iconImg.setVisibility(View.GONE);
        confirmationTextView.setText(confirmationText);

        dialogConfirmOperation.show();
    }

    @Override
    public void onClick(View view) {
        if(view == btnAccept) {
            listener.onButtonAcceptPressed();
        }

        dialogConfirmOperation.dismiss();
    }
}
