package com.jumpitt.winmanagement.utils.controllers;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.utils.interfaces.OnOptionSelectedListener;
import com.jumpitt.winmanagement.utils.TableOptions;
import com.jumpitt.winmanagement.utils.Utils;

public class OptionsDialogController implements View.OnClickListener{
    private Dialog dialogOptions;
    private Button actualizarImagenMesa;
    private Button obtenerParametros;
    private Button modificarMinMax;
    private Button recuentoOperativo;
    private Button cerrarMesa;
    private OnOptionSelectedListener listener;

    public OptionsDialogController(Activity activity, OnOptionSelectedListener onOptionSelectedListener) {
        listener = onOptionSelectedListener;

        this.dialogOptions = Utils.getNewDialog(activity, R.layout.dialog_opciones_mesa, true, true, 580, ViewGroup.LayoutParams.MATCH_PARENT);

        actualizarImagenMesa = (Button) dialogOptions.findViewById(R.id.btnActualizarImagenMesa);
        obtenerParametros = (Button) dialogOptions.findViewById(R.id.btnActualizarParametros);
        modificarMinMax = (Button) dialogOptions.findViewById(R.id.btnModificarMinMax);
        recuentoOperativo = (Button) dialogOptions.findViewById(R.id.btnRecuentoOperativo);
        cerrarMesa = (Button) dialogOptions.findViewById(R.id.btnCerrarMesa);

        actualizarImagenMesa.setOnClickListener(this);
        obtenerParametros.setOnClickListener(this);
        modificarMinMax.setOnClickListener(this);
        recuentoOperativo.setOnClickListener(this);
        cerrarMesa.setOnClickListener(this);
    }

    public void showOptionsDialog() {
        this.dialogOptions.show();
    }

    public void closeOptionsDialog() {
        this.dialogOptions.dismiss();
    }

    @Override
    public void onClick(View view) {
        if(view == actualizarImagenMesa) {
            listener.onOptionSelected(dialogOptions, TableOptions.REFRESH_TABLE_IMG);
        }
        else if(view == obtenerParametros) {
            listener.onOptionSelected(dialogOptions, TableOptions.GET_PARAMS);
        }
        else if(view == modificarMinMax) {
            listener.onOptionSelected(dialogOptions, TableOptions.UPDATE_MIN_MAX);
        }
        else if(view == recuentoOperativo) {
            listener.onOptionSelected(dialogOptions, TableOptions.OPERATIVE_COUNTING);
        }
        else if(view == cerrarMesa) {
            listener.onOptionSelected(dialogOptions, TableOptions.CLOSE_TABLE);
        }
    }
}
