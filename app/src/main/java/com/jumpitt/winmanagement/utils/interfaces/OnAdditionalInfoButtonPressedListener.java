package com.jumpitt.winmanagement.utils.interfaces;

public interface OnAdditionalInfoButtonPressedListener {
    void onAddButtonPressed(String idPedido);
}