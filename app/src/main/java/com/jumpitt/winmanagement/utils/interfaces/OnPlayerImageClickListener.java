package com.jumpitt.winmanagement.utils.interfaces;

import android.view.View;

import com.jumpitt.winmanagement.dataModel.JugadorDM;

public interface OnPlayerImageClickListener {
    void onSingleClick(View v, JugadorDM player);
    void onDoubleClick(View v, JugadorDM player);
    void onLongClick(View v, boolean playerIsPaused);
}