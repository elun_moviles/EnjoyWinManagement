package com.jumpitt.winmanagement.utils.controllers;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.HistorialClienteDM;
import com.jumpitt.winmanagement.dataModel.JugadorDM;
import com.jumpitt.winmanagement.dataModel.enums.PlayerTypes;
import com.jumpitt.winmanagement.lists.adapters.DetalleHistorialAdapter;
import com.jumpitt.winmanagement.utils.GlobalData;
import com.jumpitt.winmanagement.utils.Utils;

public class PlayerHistoryController implements View.OnClickListener {
    Dialog dialogHistorialCliente;
    ImageButton btnCloseDialog;
    Button btnResumen;
    Button btnDetalle;
    RelativeLayout resumenLayout;
    RelativeLayout detalleLayout;
    LinearLayout titulosLayout;
    TextView idClienteTxt;
    TextView tiempoJuegoTxt;
    TextView consumoTxt;
    TextView prodPreferidoTxt;
    TextView winTeoricoTxt;
    TextView cantCompsTxt;
    TextView montoTotalCambiosTxt;
    TextView idCliente;
    TextView tiempoJuego;
    TextView consumo;
    TextView prodPreferido;
    TextView winTeorico;
    TextView cantComps;
    TextView montoTotalCambios;
    TextView nomCliente;
    TextView emptyList;
    Activity activity;

    public PlayerHistoryController(Activity activity) {
        this.activity = activity;
        dialogHistorialCliente = Utils.getNewDialog(activity, R.layout.dialog_historial_jugador, false, true, 1100, ViewGroup.LayoutParams.WRAP_CONTENT);

        View idClienteView = dialogHistorialCliente.findViewById(R.id.infoResumen1);
        View tiempoJuegoView = dialogHistorialCliente.findViewById(R.id.infoResumen2);
        View consumoView = dialogHistorialCliente.findViewById(R.id.infoResumen3);
        View prodPreferidoView = dialogHistorialCliente.findViewById(R.id.infoResumen4);
        View winTeoricoView = dialogHistorialCliente.findViewById(R.id.infoResumen5);
        View cantCompsView = dialogHistorialCliente.findViewById(R.id.infoResumen6);
        View montoTotalCambiosView = dialogHistorialCliente.findViewById(R.id.infoResumen7);

        idClienteTxt = (TextView) idClienteView.findViewById(R.id.textoResumen);
        tiempoJuegoTxt = (TextView) tiempoJuegoView.findViewById(R.id.textoResumen);
        consumoTxt = (TextView) consumoView.findViewById(R.id.textoResumen);
        prodPreferidoTxt = (TextView) prodPreferidoView.findViewById(R.id.textoResumen);
        winTeoricoTxt = (TextView) winTeoricoView.findViewById(R.id.textoResumen);
        cantCompsTxt = (TextView) cantCompsView.findViewById(R.id.textoResumen);
        montoTotalCambiosTxt = (TextView) montoTotalCambiosView.findViewById(R.id.textoResumen);

        idCliente = (TextView) idClienteView.findViewById(R.id.valorResumen);
        tiempoJuego = (TextView) tiempoJuegoView.findViewById(R.id.valorResumen);
        consumo = (TextView) consumoView.findViewById(R.id.valorResumen);
        prodPreferido = (TextView) prodPreferidoView.findViewById(R.id.valorResumen);
        winTeorico = (TextView) winTeoricoView.findViewById(R.id.valorResumen);
        cantComps = (TextView) cantCompsView.findViewById(R.id.valorResumen);
        montoTotalCambios = (TextView) montoTotalCambiosView.findViewById(R.id.valorResumen);

        nomCliente = (TextView) dialogHistorialCliente.findViewById(R.id.nombreJugador);

        emptyList = (TextView) dialogHistorialCliente.findViewById(R.id.emptyList);

        btnCloseDialog = (ImageButton) dialogHistorialCliente.findViewById(R.id.close);
        resumenLayout = (RelativeLayout) dialogHistorialCliente.findViewById(R.id.resumen);
        detalleLayout = (RelativeLayout) dialogHistorialCliente.findViewById(R.id.detalle);
        titulosLayout = (LinearLayout) dialogHistorialCliente.findViewById(R.id.titulos);
        btnResumen = (Button) dialogHistorialCliente.findViewById(R.id.btnResumen);
        btnDetalle = (Button) dialogHistorialCliente.findViewById(R.id.btnDetalle);

        btnCloseDialog.setOnClickListener(this);
        btnResumen.setOnClickListener(this);
        btnDetalle.setOnClickListener(this);
    }
    
    public void showPlayerHistory(JugadorDM player, HistorialClienteDM userHistory) {
        idClienteTxt.setText(activity.getString(R.string.client_id_text));
        tiempoJuegoTxt.setText(activity.getString(R.string.game_time_text));
        consumoTxt.setText(activity.getString(R.string.consumption_text));
        prodPreferidoTxt.setText(activity.getString(R.string.preferred_product_text));
        winTeoricoTxt.setText(activity.getString(R.string.teoric_win_text));
        cantCompsTxt.setText(activity.getString(R.string.comps_quantity_text));
        montoTotalCambiosTxt.setText(activity.getString(R.string.money_changes_quantity_text));

        nomCliente.setText(player.getNombre_jugador());

        idCliente.setText((player.getPlayerType() == PlayerTypes.ANONYMOUS) ? userHistory.id_cliente : userHistory.id_cliente_mcc);
        tiempoJuego.setText(userHistory.tiempo_juego);
        consumo.setText(Utils.moneyFormat(userHistory.consumo_cliente));
        prodPreferido.setText(userHistory.producto_preferido);
        winTeorico.setText(Utils.moneyFormat(userHistory.win_teorico));
        cantComps.setText(String.valueOf(userHistory.cantidad_comps));

        String monto_cambios_dinero = Utils.moneyFormat(userHistory.monto_cambios_dinero);

        monto_cambios_dinero += " (" + userHistory.cantidad_cambios_dinero + " cambios en total)";

        if (userHistory.cantidad_cambios_dinero == 1)
            monto_cambios_dinero = monto_cambios_dinero.replace("s", GlobalData.EMPTY);

        montoTotalCambios.setText(monto_cambios_dinero);

        ListView lista = (ListView) dialogHistorialCliente.findViewById(R.id.listaDetalles);

        if (!userHistory.listaOperacionesCliente.isEmpty()) {
            lista.setAdapter(new DetalleHistorialAdapter(activity, userHistory.listaOperacionesCliente));

            emptyList.setVisibility(View.GONE);
        }
        else {
            lista.setAdapter(null);

            emptyList.setVisibility(View.VISIBLE);
        }

        dialogHistorialCliente.show();
    }

    public void setButtonState(Button btn, boolean pressed) {
        if (pressed) {
            btn.setTextColor(activity.getResources().getColor(R.color.blanco));
            btn.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
        } else {
            btn.setTextColor(activity.getResources().getColor(R.color.blanco));
            btn.setBackgroundColor(activity.getResources().getColor(R.color.dark_grey_text_color));
        }

    }

    public void setDefaultHistoryButton(Button resumen, Button detalle, RelativeLayout resumenView, RelativeLayout detalleView, LinearLayout botonesTituloView) {
        setButtonState(resumen, true);
        setButtonState(detalle, false);

        resumenView.setVisibility(View.VISIBLE);
        detalleView.setVisibility(View.GONE);
        botonesTituloView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        if (view == btnCloseDialog) {
            dialogHistorialCliente.dismiss();
        }
        else if(view == btnResumen) {
            setDefaultHistoryButton(btnResumen, btnDetalle, resumenLayout, detalleLayout, titulosLayout);
        }
        else if(view == btnDetalle) {
            setButtonState(btnResumen, false);
            setButtonState(btnDetalle, true);

            resumenLayout.setVisibility(View.GONE);
            detalleLayout.setVisibility(View.VISIBLE);
            titulosLayout.setVisibility(View.VISIBLE);
        }
    }
}
