package com.jumpitt.winmanagement.utils.controllers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.enums.KeyboardTypes;
import com.jumpitt.winmanagement.utils.GlobalData;
import com.jumpitt.winmanagement.utils.interfaces.OnValueEnteredListener;
import com.jumpitt.winmanagement.utils.Utils;

public class KeyboardController implements View.OnClickListener {
    private Context context;
    private Button btnUno;
    private Button btnDos;
    private Button btnTres;
    private Button btnCuatro;
    private Button btnCinco;
    private Button btnSeis;
    private Button btnSiete;
    private Button btnOcho;
    private Button btnNueve;
    private Button btnCero;
    private Button btnK;
    private Button btnAcceptTouchpadKeyboard;
    private TextView resultadoNumTextView;
    private ImageButton btnBackspace;
    private Dialog dialogTouchpadKeyboard;
    private KeyboardTypes keyboardType;
    private OnValueEnteredListener listener;

    public KeyboardController(Activity activity) {
        this.context = activity;
        dialogTouchpadKeyboard = Utils.getNewDialog(activity, R.layout.dialog_teclado_tactil, true, true, 850, ViewGroup.LayoutParams.MATCH_PARENT);

        resultadoNumTextView = (TextView) dialogTouchpadKeyboard.findViewById(R.id.resultadoNum);
        btnUno = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_uno);
        btnDos = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_dos);
        btnTres = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_tres);
        btnCuatro = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_cuatro);
        btnCinco = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_cinco);
        btnSeis = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_seis);
        btnSiete = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_siete);
        btnOcho = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_ocho);
        btnNueve = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_nueve);
        btnCero = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_cero);
        btnK = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_k);
        btnBackspace = (ImageButton) dialogTouchpadKeyboard.findViewById(R.id.btn_backspace);
        btnAcceptTouchpadKeyboard = (Button) dialogTouchpadKeyboard.findViewById(R.id.btn_accept);

        btnUno.setText(context.getString(R.string.number_one_text));
        btnDos.setText(context.getString(R.string.number_two_text));
        btnTres.setText(context.getString(R.string.number_three_text));
        btnCuatro.setText(context.getString(R.string.number_four_text));
        btnCinco.setText(context.getString(R.string.number_five_text));
        btnSeis.setText(context.getString(R.string.number_six_text));
        btnSiete.setText(context.getString(R.string.number_seven_text));
        btnOcho.setText(context.getString(R.string.number_eight_text));
        btnNueve.setText(context.getString(R.string.number_nine_text));
        btnCero.setText(context.getString(R.string.number_zero_text));
        btnK.setText(context.getString(R.string.letter_k_text));

        btnUno.setOnClickListener(this);
        btnDos.setOnClickListener(this);
        btnTres.setOnClickListener(this);
        btnCuatro.setOnClickListener(this);
        btnCinco.setOnClickListener(this);
        btnSeis.setOnClickListener(this);
        btnSiete.setOnClickListener(this);
        btnOcho.setOnClickListener(this);
        btnNueve.setOnClickListener(this);
        btnCero.setOnClickListener(this);
        btnK.setOnClickListener(this);
        btnBackspace.setOnClickListener(this);
        btnAcceptTouchpadKeyboard.setOnClickListener(this);

        resultadoNumTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (resultadoNumTextView.getText().toString().isEmpty()) {
                    btnAcceptTouchpadKeyboard.setEnabled(false);
                } else {
                    btnAcceptTouchpadKeyboard.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnBackspace.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                resultadoNumTextView.setText("");

                return false;
            }
        });

        dialogTouchpadKeyboard.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                resultadoNumTextView.setText(GlobalData.EMPTY);
                btnAcceptTouchpadKeyboard.setEnabled(false);
            }
        });
    }

    public void showKeyboard(KeyboardTypes keyboardType, OnValueEnteredListener onValueEnteredListener) {
        selectTouchpadKeyboardType(keyboardType);

        this.listener = onValueEnteredListener;

        dialogTouchpadKeyboard.show();
    }

    public void selectTouchpadKeyboardType(KeyboardTypes keyboardType) {
        this.keyboardType = keyboardType;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        switch(keyboardType) {
            case ID:
                btnK.setVisibility(View.GONE);

                params.width = (int) context.getResources().getDimension(R.dimen.size_button_touchpad_large);
                params.height = (int) context.getResources().getDimension(R.dimen.size_button_touchpad);
                params.rightMargin = 3;

                btnCero.setLayoutParams(params);

                btnAcceptTouchpadKeyboard.setText(context.getString(R.string.enter_client_id_text));
            break;

            case RUT:
                btnK.setVisibility(View.VISIBLE);

                params.width = (int) context.getResources().getDimension(R.dimen.size_button_touchpad);
                params.height = (int) context.getResources().getDimension(R.dimen.size_button_touchpad);
                params.rightMargin = 3;

                btnCero.setLayoutParams(params);

                btnAcceptTouchpadKeyboard.setText(context.getString(R.string.enter_client_rut_text));
            break;

            default:
                btnK.setVisibility(View.GONE);

                params.width = (int) context.getResources().getDimension(R.dimen.size_button_touchpad_large);
                params.height = (int) context.getResources().getDimension(R.dimen.size_button_touchpad);
                params.rightMargin = 3;

                btnCero.setLayoutParams(params);

                btnAcceptTouchpadKeyboard.setText(context.getString(R.string.enter_client_id_text));
            break;
        }
    }

    @Override
    public void onClick(View view) {
        if(view == btnUno) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(keyboardType == KeyboardTypes.ID) resultadoNumTextView.setText(tmpStr + context.getString(R.string.number_one_text));
            else if(keyboardType == KeyboardTypes.RUT && tmpStr.length() <= 11) resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.number_one_text)));
        }
        else if(view == btnDos) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(keyboardType == KeyboardTypes.ID) resultadoNumTextView.setText(tmpStr + context.getString(R.string.number_two_text));
            else if(keyboardType == KeyboardTypes.RUT && tmpStr.length() <= 11) resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.number_two_text)));
        }
        else if(view == btnTres) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(keyboardType == KeyboardTypes.ID) resultadoNumTextView.setText(tmpStr + context.getString(R.string.number_three_text));
            else if(keyboardType == KeyboardTypes.RUT && tmpStr.length() <= 11) resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.number_three_text)));
        }
        else if(view == btnCuatro) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(keyboardType == KeyboardTypes.ID) resultadoNumTextView.setText(tmpStr + context.getString(R.string.number_four_text));
            else if(keyboardType == KeyboardTypes.RUT && tmpStr.length() <= 11) resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.number_four_text)));
        }
        else if(view == btnCinco) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(keyboardType == KeyboardTypes.ID) resultadoNumTextView.setText(tmpStr + context.getString(R.string.number_five_text));
            else if(keyboardType == KeyboardTypes.RUT && tmpStr.length() <= 11) resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.number_five_text)));
        }
        else if(view == btnSeis) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(keyboardType == KeyboardTypes.ID) resultadoNumTextView.setText(tmpStr + context.getString(R.string.number_six_text));
            else if(keyboardType == KeyboardTypes.RUT && tmpStr.length() <= 11) resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.number_six_text)));
        }
        else if(view == btnSiete) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(keyboardType == KeyboardTypes.ID) resultadoNumTextView.setText(tmpStr + context.getString(R.string.number_seven_text));
            else if(keyboardType == KeyboardTypes.RUT && tmpStr.length() <= 11) resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.number_seven_text)));
        }
        else if(view == btnOcho) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(keyboardType == KeyboardTypes.ID) resultadoNumTextView.setText(tmpStr + context.getString(R.string.number_eight_text));
            else if(keyboardType == KeyboardTypes.RUT && tmpStr.length() <= 11) resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.number_eight_text)));
        }
        else if(view == btnNueve) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(keyboardType == KeyboardTypes.ID) resultadoNumTextView.setText(tmpStr + context.getString(R.string.number_nine_text));
            else if(keyboardType == KeyboardTypes.RUT && tmpStr.length() <= 11) resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.number_nine_text)));
        }
        else if(view == btnCero) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(tmpStr.length() > 0) {
                if (keyboardType == KeyboardTypes.ID) resultadoNumTextView.setText(tmpStr + context.getString(R.string.number_zero_text));
                else if(keyboardType == KeyboardTypes.RUT && tmpStr.length() <= 11) resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.number_zero_text)));
            }
        }
        else if(view == btnK) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(keyboardType == KeyboardTypes.RUT && tmpStr.length() >= 9 && tmpStr.length() <= 11) {
                resultadoNumTextView.setText(Utils.formatRUT(tmpStr + context.getString(R.string.letter_k_text)));
            }
        }
        else if(view == btnBackspace) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(tmpStr.length() > 0) resultadoNumTextView.setText(tmpStr.substring(0, tmpStr.length()-1));
        }
        else if(view == btnAcceptTouchpadKeyboard) {
            listener.onValueEntered(view, resultadoNumTextView.getText().toString());

            dialogTouchpadKeyboard.dismiss();
        }
    }
}
