package com.jumpitt.winmanagement.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.JugadorDM;
import com.jumpitt.winmanagement.dataModel.enums.PlayerTypes;
import com.jumpitt.winmanagement.utils.interfaces.OnPlayerImageClickListener;
import com.orhanobut.logger.Logger;

public class PlayerView {
    private Activity activity;
    private static final long DOUBLE_PRESS_INTERVAL = 200;
    private long lastPressTime = 0;
    private boolean isDoubleClick = false;
    private JugadorDM jugador;
    private View root;
    public TextView playerName;
    public TextView playerTimer;
    public TextView playerCategory;
    public ImageButton playerImage;
    public ImageView playerHasComp;
    public ImageView playerIsPaused;

    public PlayerView(Activity activity) {
        this.activity = activity;

        LayoutInflater vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        root = vi.inflate(R.layout.player_view, null);

        this.playerName = (TextView) root.findViewById(R.id.playerName);
        this.playerTimer = (TextView) root.findViewById(R.id.playerTimer);
        this.playerCategory = (TextView) root.findViewById(R.id.playerCategory);
        this.playerImage = (ImageButton) root.findViewById(R.id.playerImg);
        this.playerHasComp = (ImageView) root.findViewById(R.id.availableCompImg);
        this.playerIsPaused = (ImageView) root.findViewById(R.id.playerPausedImg);
    }

    private void setPlayerCategory(String category) {
        this.playerCategory.setText(category.isEmpty() ? GlobalData.UNKNOWN_CATEGORY : category);

        if(category.equals(GlobalData.CLASSIC_CATEGORY)) {
            this.playerCategory.setBackgroundResource(R.drawable.shape_player_category_classic);
        }
        else if(category.equals(GlobalData.SILVER_CATEGORY)) {
            this.playerCategory.setBackgroundResource(R.drawable.shape_player_category_silver);
        }
        else if(category.equals(GlobalData.GOLD_CATEGORY)) {
            this.playerCategory.setBackgroundResource(R.drawable.shape_player_category_gold);
        }
        else if(category.equals(GlobalData.PLATINUM_CATEGORY)) {
            this.playerCategory.setBackgroundResource(R.drawable.shape_player_category_platinum);
        }
        else if(category.equals(GlobalData.DIAMOND_CATEGORY)) {
            this.playerCategory.setBackgroundResource(R.drawable.shape_player_category_diamond);
        }
        else this.playerCategory.setBackgroundResource(R.drawable.shape_player_category_unknown);
    }

    public View getRootView() {
        return root;
    }

    public JugadorDM getJugador() { return jugador; }

    public void populateView(JugadorDM jugador, PlayerTypes pType, OnPlayerImageClickListener onPlayerImageClickListener) {
        if(onPlayerImageClickListener == null) Logger.e("onPlayerImageClickListener is NULL");

        if(jugador != null) {
            Bitmap imagenJugador = null;
            int imageBackgroundColor = 0;
            int categoryBackground = R.drawable.shape_player_category_unknown;

            playerTimer.setText(jugador.getTimer_jugador());
            setPlayerCategory(jugador.getCategoria_jugador());
            jugador.setPlayerType(pType);

            this.jugador = jugador;

            switch (pType) {
                case NULL:
                    root.setVisibility(View.INVISIBLE);
                break;

                case EMPTY:
                    root.setVisibility(View.VISIBLE);

                    jugador.setHasAvailableComp(false);
                    jugador.setIsPaused(false);
                    jugador.setTimer_jugador(activity.getString(R.string.null_timer_tracking));

                    playerImage.clearAnimation();
                    playerTimer.clearAnimation();

                    playerHasComp.setVisibility(View.GONE);
                    playerIsPaused.setVisibility(View.GONE);
                    playerTimer.setVisibility(View.INVISIBLE);
                    playerCategory.setVisibility(View.INVISIBLE);

                    playerImage.setScaleType(ImageView.ScaleType.CENTER); // scalar Imagen

                    imagenJugador = Utils.getBitmapFromDrawable(activity.getResources().getDrawable(R.drawable.ic_add_user_small));
                    imageBackgroundColor = R.drawable.bg_empty_player_image;

                    playerName.setBackgroundResource(R.drawable.shape_player_name);

                    playerImage.setOnClickListener(new PlayerViewOnClickListener(onPlayerImageClickListener));
                break;

                case REGISTERED:
                    root.setVisibility(View.VISIBLE);
                    playerHasComp.setVisibility(jugador.hasAvailableComp() ? View.VISIBLE : View.GONE);
                    playerIsPaused.setVisibility(jugador.isPaused() ? View.VISIBLE : View.GONE);
                    playerTimer.setVisibility(View.VISIBLE);
                    playerCategory.setVisibility(View.VISIBLE);


                    if(jugador.getFoto_jugador_b64().isEmpty()) {
                        imagenJugador = BitmapFactory.decodeResource(activity.getResources(), R.drawable.img_player_no_picture);
                    }
                    else {
                        imagenJugador = Utils.getBitmapFromBase64(jugador.getFoto_jugador_b64());
                        playerImage.setScaleType(ImageView.ScaleType.CENTER_CROP); // scalar Imagen

                    }

                    imageBackgroundColor = android.R.color.transparent;

                    String categoriaJugador = jugador.getCategoria_jugador().toUpperCase(Utils.locale);

                    if(categoriaJugador.equals(GlobalData.CLASSIC_CATEGORY)) {
                        categoryBackground = R.drawable.shape_player_category_classic;
                    }
                    else if(categoriaJugador.equals(GlobalData.SILVER_CATEGORY)) {
                        categoryBackground = R.drawable.shape_player_category_silver;
                    }
                    else if(categoriaJugador.equals(GlobalData.GOLD_CATEGORY)) {
                        categoryBackground = R.drawable.shape_player_category_gold;
                    }
                    else if(categoriaJugador.equals(GlobalData.PLATINUM_CATEGORY)) {
                        categoryBackground = R.drawable.shape_player_category_platinum;
                    }
                    else if(categoriaJugador.equals(GlobalData.DIAMOND_CATEGORY)) {
                        categoryBackground = R.drawable.shape_player_category_diamond;
                    }

                    playerImage.setOnClickListener(new PlayerViewOnClickListener(onPlayerImageClickListener));
                    playerImage.setOnLongClickListener(new PlayerViewOnLongClickListener(onPlayerImageClickListener));
                break;

                case ANONYMOUS:
                    root.setVisibility(View.VISIBLE);
                    playerHasComp.setVisibility(View.GONE);
                    playerIsPaused.setVisibility(jugador.isPaused() ? View.VISIBLE : View.GONE);
                    playerTimer.setVisibility(View.VISIBLE);
                    playerCategory.setVisibility(View.VISIBLE);

                    playerImage.setScaleType(ImageView.ScaleType.CENTER); // scalar Imagen

                    jugador.setHasAvailableComp(false);
                    jugador.setNombre_jugador(jugador.getNombre_jugador());
                    jugador.setCategoria_jugador(GlobalData.UNKNOWN_CATEGORY);
                    jugador.setFoto_jugador_b64(Utils.getBase64FromBitmap(Utils.getBitmapFromDrawable(activity.getResources().getDrawable(R.drawable.ic_unknown_person_small))));

                    imagenJugador = Utils.getBitmapFromBase64(jugador.getFoto_jugador_b64());
                    imageBackgroundColor = R.color.colorAccentDark;
                    categoryBackground = R.drawable.shape_player_category_unknown;

                    playerImage.setOnClickListener(new PlayerViewOnClickListener(onPlayerImageClickListener));
                    playerImage.setOnLongClickListener(new PlayerViewOnLongClickListener(onPlayerImageClickListener));
                break;
            }

            playerName.setText(jugador.getNombre_jugador());
            playerTimer.setText(jugador.getTimer_jugador());
            playerCategory.setText(jugador.getCategoria_jugador());
            playerCategory.setBackgroundResource(categoryBackground);
            playerImage.setImageBitmap(imagenJugador);
            playerImage.setBackgroundResource(imageBackgroundColor);
        }
    }

    private class PlayerViewOnClickListener implements View.OnClickListener {
        private OnPlayerImageClickListener listener;

        public PlayerViewOnClickListener(OnPlayerImageClickListener listener) {
            this.listener = listener;
        }

        @Override
        public void onClick(View view) {
            if(listener == null) Logger.e("listener is null");

            if(view == playerImage && jugador != null && listener != null) {
                long pressTime = System.currentTimeMillis();

                if (pressTime - lastPressTime <= DOUBLE_PRESS_INTERVAL) {
                    isDoubleClick = true;

                    if(!jugador.getNombre_jugador().equals(activity.getString(R.string.empty_text))) listener.onDoubleClick(root, jugador);
                }
                else {
                    final View v = view;

                    isDoubleClick = false;

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!isDoubleClick) {
                                listener.onSingleClick(v, jugador);
                            }
                        }
                    }, DOUBLE_PRESS_INTERVAL);
                }

                lastPressTime = pressTime;
            }
        }
    }

    private class PlayerViewOnLongClickListener implements View.OnLongClickListener {
        private OnPlayerImageClickListener listener;

        public PlayerViewOnLongClickListener(OnPlayerImageClickListener listener) {
            this.listener = listener;
        }

        @Override
        public boolean onLongClick(View view) {
            if(listener != null && jugador != null && jugador.getPlayerType() != PlayerTypes.EMPTY) {
                listener.onLongClick(view, jugador.isPaused());
            }

            return false;
        }
    }
}
