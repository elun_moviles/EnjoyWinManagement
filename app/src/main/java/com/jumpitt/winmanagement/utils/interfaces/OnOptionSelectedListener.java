package com.jumpitt.winmanagement.utils.interfaces;

import android.app.Dialog;

import com.jumpitt.winmanagement.utils.TableOptions;

public interface OnOptionSelectedListener {
    void onOptionSelected(Dialog optionsDialog, TableOptions optionSelected);
}