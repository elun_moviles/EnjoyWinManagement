package com.jumpitt.winmanagement.utils.interfaces;

public interface OnMinMaxValuesEnteredListener {
    void onValuesEntered(int minValue, int maxValue);
}