package com.jumpitt.winmanagement.utils.interfaces;

import android.view.View;

import com.jumpitt.winmanagement.lists.classes.SelectedItemListaComp;

public interface OnProductSelectedListener {
    void onProductSelected(View view, SelectedItemListaComp selectedItem);
}