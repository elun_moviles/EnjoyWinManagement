package com.jumpitt.winmanagement.utils.interfaces;

import android.view.View;

public interface OnValueEnteredListener {
    void onValueEntered(View v, String value);
}