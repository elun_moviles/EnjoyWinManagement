package com.jumpitt.winmanagement.utils.controllers;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.lists.adapters.ListaMaxAdapter;
import com.jumpitt.winmanagement.lists.adapters.ListaMinAdapter;
import com.jumpitt.winmanagement.utils.Utils;
import com.jumpitt.winmanagement.utils.interfaces.OnMinMaxValuesEnteredListener;

public class MinMaxValuesController implements View.OnClickListener {
    private Dialog dialogSeleccionarMinMax;
    private Button btnAcceptMinMax;
    private Button btnCancelMinMax;
    private TextView minValue;
    private TextView maxValue;
    private OnMinMaxValuesEnteredListener listener;

    public MinMaxValuesController(Activity activity, int minValues, int maxValues) {
        dialogSeleccionarMinMax = Utils.getNewDialog(activity, R.layout.dialog_seleccionar_min_max, false, 600, ViewGroup.LayoutParams.MATCH_PARENT);

        btnAcceptMinMax = (Button) dialogSeleccionarMinMax.findViewById(R.id.btnAccept);
        btnCancelMinMax = (Button) dialogSeleccionarMinMax.findViewById(R.id.btnCancel);
        minValue = (TextView) dialogSeleccionarMinMax.findViewById(R.id.minValue);
        maxValue = (TextView) dialogSeleccionarMinMax.findViewById(R.id.maxValue);
        ListView listViewMin = (ListView) dialogSeleccionarMinMax.findViewById(R.id.listMin);
        ListView listViewMax = (ListView) dialogSeleccionarMinMax.findViewById(R.id.listMax);

        listViewMin.setAdapter(new ListaMinAdapter(activity, minValues, dialogSeleccionarMinMax));
        listViewMax.setAdapter(new ListaMaxAdapter(activity, maxValues, dialogSeleccionarMinMax));

        btnAcceptMinMax.setOnClickListener(this);
        btnCancelMinMax.setOnClickListener(this);

        dialogSeleccionarMinMax.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                setEnabledAcceptButton(true);
            }
        });
    }

    public void showMinMaxDialog(OnMinMaxValuesEnteredListener onMinMaxValuesEnteredListener) {
        this.listener = onMinMaxValuesEnteredListener;

        dialogSeleccionarMinMax.show();
    }

    public void closeMinMaxDialog() {
        dialogSeleccionarMinMax.dismiss();
    }

    public void setEnabledAcceptButton(boolean enable) {
        btnAcceptMinMax.setEnabled(enable);
    }

    @Override
    public void onClick(View view) {
        if(view == btnAcceptMinMax) {
            int min = Integer.parseInt(Utils.getCleanString(minValue.getText().toString()));
            int max = Integer.parseInt(Utils.getCleanString(maxValue.getText().toString()));

            listener.onValuesEntered(min, max);
        }
        else if(view == btnCancelMinMax) {
            dialogSeleccionarMinMax.dismiss();
        }
    }
}
