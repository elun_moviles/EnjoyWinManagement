package com.jumpitt.winmanagement.utils;

import android.content.Context;

import com.jumpitt.winmanagement.dataModel.MesaDM;
import com.jumpitt.winmanagement.dataModel.UsuarioDM;

import java.io.Serializable;

public class GlobalData implements Serializable {
    private UsuarioDM jefeMesa;
    private UsuarioDM croupier;
    private MesaDM mesa;

    public static String ID_UNIDAD = "1";

    public static boolean MUST_CHECK_IF_AVAILABLE_COMPS = false;

    public static String EMPTY = "";
    public static String LISTA_ORIGINAL_INGREDIENTES = "ListaOriginalIngredientes";
    public static String LISTA_ORIGINAL_INFO_EXTRA = "ListaOriginalInfoExtra";
    public static int INTERVALO_RECUENTO_OPERATIVO = 30000; // Milisegundos
    public static int OPERATING_COUNTING = 0;
    public static int MIN_CARD_INDEX = 5;
    public static int MAX_CARD_INDEX = 10;
    public static int MAX_CARD_LENGTH = 10;

    public static int TABLE_SIZE_WIDTH = 4;
    public static int TABLE_SIZE_HEIGHT = 10;

    // Tipos de usuario
    public static String USUARIO_JEFE_SALON = "Jefe de Salon";
    public static String USUARIO_JEFE_MESA = "Jefe de Mesa";
    public static String USUARIO_CROUPIER = "Croupier";
    public static String USUARIO_ANONIMO = "Anónimo";

    // Nombres y tipos de mesas
    public static final String BLACKJACK = "Blackjack";
    public static final String BLACKJACK_TIPO_MESA = "blackjack";
    public static final String BLACKJACK_SIGLA = "BJ";

    public static final String RULETA = "Ruleta";
    public static final String RULETA_TIPO_MESA = "ruleta";
    public static final String RULETA_SIGLA = "RU";

    public static final String CANAL_21 = "Canal 21";
    public static final String CANAL_21_TIPO_MESA = "canal_21";
    public static final String CANAL_21_SIGLA = "C21";

    public static final String POKER = "Poker";
    public static final String POKER_TIPO_MESA = "poker";

    public static final String GO_POKER = "Go Poker";
    public static final String GO_POKER_TIPO_MESA = "go_poker";
    public static final String GO_POKER_SIGLA = "GP";

    public static final String HOLDEM_POKER = "Hold'em Poker";
    public static final String HOLDEM_POKER_TIPO_MESA = "hold'em_poker";
    public static final String HOLDEM_POKER_SIGLA = "HP";

    public static final String DRAW_POKER = "Draw Poker";
    public static final String DRAW_POKER_TIPO_MESA = "draw_poker";
    public static final String DRAW_POKER_SIGLA = "DP";

    public static final String CARIBBEAN_POKER = "Caribbean Poker";
    public static final String CARIBBEAN_POKER_TIPO_MESA = "caribbean_poker";
    public static final String CARIBBEAN_POKER_SIGLA = "CP";

    public static final String CRAPS = "Craps";
    public static final String CRAPS_TIPO_MESA = "ic_dices_small";
    public static final String CRAPS_SIGLA = "CR";

    public static final String MINI_BACCARAT = "Mini Baccarat";
    public static final String MINI_BACCARAT_TIPO_MESA = "mini_baccarat";
    public static final String MINI_BACCARAT_SIGLA = "MB";

    public static final String PUNTO_BANCA = "Punto y Banca";
    public static final String PUNTO_BANCA_TIPO_MESA = "punto_banca";
    public static final String PUNTO_BANCA_SIGLA = "PYB";

    public static final String BACCARAT_AMERICANO = "Baccarat Americano";
    public static final String BACCARAT_AMERICANO_TIPO_MESA = "baccarat_americano";
    public static final String BACCARAT_AMERICANO_SIGLA = "BA";

    // Categorias clientes
    public static String ANONYMOUS_CATEGORY = "ANONIMO";
    public static String CLASSIC_CATEGORY = "CLASSIC";
    public static String SILVER_CATEGORY = "SILVER";
    public static String GOLD_CATEGORY = "GOLD";
    public static String PLATINUM_CATEGORY = "PLATINUM";
    public static String DIAMOND_CATEGORY = "DIAMOND";
    public static String UNKNOWN_CATEGORY = "DESCONOCIDO";

    // Tipos de operaciones en las mesas
    public static String RECUENTO_OPERATIVO = "RECUENTO OPERATIVO";

    // Parametros BD
    public static int LIMITE_PEDIDOS_COMPS;
    public static int LIMITE_INGREDIENTES_COMPS;
    public static int LIMITE_INFO_EXTRA_COMPS;
    public static int LIMITE_INFERIOR_TRACKING;
    public static int LIMITE_SUPERIOR_TRACKING;
    public static String TOKEN_PRODUCTOS_COMPS;

    public static String LIMITE_PEDIDOS_COMPS_TEXT = "LIMITE_PEDIDOS_COMPS";
    public static String LIMITE_INGREDIENTES_COMPS_TEXT = "LIMITE_INGREDIENTES_COMPS";
    public static String LIMITE_INFO_EXTRA_COMPS_TEXT = "LIMITE_INFO_EXTRA_COMPS";
    public static String LIMITE_INFERIOR_TRACKING_TEXT = "LIMITE_INFERIOR_TRACKING";
    public static String LIMITE_SUPERIOR_TRACKING_TEXT = "LIMITE_SUPERIOR_TRACKING";
    public static String TOKEN_PRODUCTOS_COMPS_TEXT = "TOKEN_PRODUCTOS_COMPS";

    // Errores y Mensajes webservices
    public static String ERROR_GETTING_DATA = "Error al obtener los datos";
    public static String ERROR_GETTING_TABLES = "Error al obtener las mesas del Casino";
    public static String ERROR_GETTING_COMPS_LIST = "Error al obtener la lista de productos";
    public static String ERROR_GETTING_INGREDIENTS_COMPS_LIST = "Error al obtener la lista de ingredientes";
    public static String ERROR_GETTING_ADDITIONAL_INFO_COMPS_LIST = "Error al obtener la lista de info. extra";
    public static String ERROR_AUTHORIZATION_REASONS_LIST = "Error al obtener la lista de motivos de autorización de comps";
    public static String ERROR_GETTING_DEVICE_MAC_ADDRESS = "Error al obtener la dirección MAC del dispositivo";
    public static String ERROR_GETTING_PARAMS = "Error obteniendo los Parámetros de las funciones";
    public static String ERROR_GETTING_CLIENT_HISTORY = "Error al obtener historial del cliente";
    public static String ERROR_GETTING_CLIENT_INFO = "Error al obtener la información del jugador";
    public static String ERROR_GETTING_TABLE_IMAGES = "Error al obtener la imagen y el fondo de la mesa";
    public static String ERROR_SIGN_IN_ANONYMOUS_CLIENT = "Error al registrar al cliente anónimo";
    public static String ERROR_SIGN_IN_REGISTERED_CLIENT = "Error al registrar al cliente en la mesa. Intente nuevamente";
    public static String ERROR_CREATING_SESSION = "Error al crear la sesión";
    public static String ERROR_TABLET_NOT_REGISTERED_OR_DISABLED = "Tablet no se encuentra Registrada o no esta Activa";
    public static String ERROR_REGISTERING_PRICE = "Error al registrar el Premio. Intente nuevamente";
    public static String ERROR_REGISTERING_CLUB_HOST_REQUEST = "Error al ingresar la solicitud";
    public static String ERROR_REGISTERING_CAMBIO_DINERO = "Error al registrar el Cambio de Dinero. Intente nuevamente";
    public static String ERROR_REGISTERING_TRACKING = "Error al registrar el Tracking. Intente nuevamente";
    public static String ERROR_REGISTERING_RELIEF_OF_USER = "Error al ingresar relevo de Usuario";
    public static String ERROR_CLOSING_SESSION_JEFE_MESA = "Error al cerrar sesión de Jefe de Mesa. Intente nuevamente";
    public static String ERROR_CLOSING_TABLE = "Error al cerrar la Mesa. Intente nuevamente";
    public static String ERROR_UPDATING_MIN_MAX = "Error al actualizar Mínimo y Máximo de la Mesa";
    public static String ERROR_UPDATING_PLAYER_POSITION = "Error al actualizar la posición del jugador. Intente nuevamente.";
    public static String ERROR_VALUE_MIN_MAX_IS_ZERO = "Mínimo y Máximo de la Mesa deben ser superiores a $0";
    public static String ERROR_VALUE_IS_ZERO_OR_EMPTY = "Debe ingresar un monto";
    public static String ERROR_VALUE_MIN_IS_HIGHER_THAN_MAX = "El valor Mínimo debe ser inferior al Máximo";
    public static String ERROR_VALUE_MAX_IS_LOWER_THAN_MIN = "El valor Máximo debe ser superior al Mínimo";
    public static String ERROR_VALUE_TRACKING_IS_HIGHER_THAN_MAX = "El valor del Tracking debe ser inferior o igual a #_MAX_VALUE_#";
    public static String ERROR_VALUE_TRACKING_IS_LOWER_THAN_MIN = "El valor del Tracking debe ser superior o igual a #_MIN_VALUE_#";
    public static String ERROR_DELETING_PLAYER_FROM_TABLE = "Error al eliminar al jugador de la mesa. Intente nuevamente";
    public static String ERROR_DELETING_PRODUCT_OF_LIST = "Error al eliminar producto de la lista";
    public static String ERROR_CHECKING_IF_AVAILABLE_COMP = "Error al consultar por comps disponibles";
    public static String ERROR_SENDING_COMP = "Error al enviar el pedido de Comps";
    public static String ERROR_DOING_OPERATIVE_COUNTING = "Error al realizar Recuento Operativo. Intente nuevamente";
    public static String ERROR_COMPS_LIMIT_REACHED = "El Límite Máximo es de: #_COMPS_LIMIT_VALUE_# comps";
    public static String ERROR_ESTIMATION_LIMIT_REACHED = "Presupuesto insuficiente";
    public static String ERROR_UNKNOWN_REQUEST_TYPE = "Error al procesar el tipo de solicitud";
    public static String ERROR_PLAYER_PAUSED_CAN_NOT_OPEN_MENU = "Para acceder al menú del jugador éste no debe estar pausado";
    public static String ERROR_PLAYER_PAUSED_CAN_NOT_BE_MOVED = "Para modificar las posiciones de los jugadores ni el origen ni el destino deben estar pausados";
    public static String SUCCESS_UPDATE_MIN_MAX = "Mínimo y Máximo modificados correctamente";
    public static String SUCCESS_GETTING_PARAMS = "Parámetros actualizados correctamente";
    public static String SUCCESS_REFRESHING_TABLE_IMAGES = "Imágenes actualizadas correctamente";
    public static String SUCCESS_REGISTERING_OPERATIVE_COUNTING = "Recuento Operativo registrado correctamente";
    public static String SUCCESS_COMP_SENT = "Pedido enviado correctamente";

    // Tipos de comps
    public static String ID_CATEGORIA_COMP_BEBIDA_CON_ALCOHOL = "1";
    public static String ID_CATEGORIA_COMP_BEBIDA_SIN_ALCOHOL = "2";
    public static String ID_CATEGORIA_COMP_CAFETERIA = "3";

    // Tipos de solicitudes
    public static String TIPO_SOLICITUD_CLUB = "1";
    public static String TIPO_SOLICITUD_HOST = "2";

    public GlobalData(Context context) {
//        Utils.DEFAULT_BG_TABLE = BitmapFactory.decodeResource(context.getResources(), R.drawable.img_default_bg_table);
//        Utils.UNKNOWN_IMG_TABLE = BitmapFactory.decodeResource(context.getResources(), R.drawable.img_unknown_table);
    }

    public UsuarioDM getJefeMesa() {
        return jefeMesa;
    }

    public void setJefeMesa(UsuarioDM jefeMesa) {
        this.jefeMesa = jefeMesa;
    }

    public UsuarioDM getCroupier() {
        return croupier;
    }

    public void setCroupier(UsuarioDM croupier) {
        this.croupier = croupier;
    }

    public MesaDM getMesa() {
        return mesa;
    }

    public void setMesa(MesaDM mesa) {
        this.mesa = mesa;
    }
}
