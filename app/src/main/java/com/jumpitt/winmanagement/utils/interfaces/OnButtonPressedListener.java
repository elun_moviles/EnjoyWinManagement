package com.jumpitt.winmanagement.utils.interfaces;

public interface OnButtonPressedListener {
    void onButtonAcceptPressed();
}