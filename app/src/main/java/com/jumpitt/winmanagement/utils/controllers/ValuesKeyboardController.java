package com.jumpitt.winmanagement.utils.controllers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.enums.KeyboardTypes;
import com.jumpitt.winmanagement.utils.GlobalData;
import com.jumpitt.winmanagement.utils.interfaces.OnValueEnteredListener;
import com.jumpitt.winmanagement.utils.Utils;

public class ValuesKeyboardController implements View.OnClickListener {
    private Context context;
    private boolean autoClose;
    private LinearLayout presetValues;
    private Button btnUno;
    private Button btnDos;
    private Button btnTres;
    private Button btnCuatro;
    private Button btnCinco;
    private Button btnSeis;
    private Button btnSiete;
    private Button btnOcho;
    private Button btnNueve;
    private Button btnCero;
    private Button btnDobleCero;
    private Button btnTripleCero;
    private Button btnMonto1;
    private Button btnMonto2;
    private Button btnMonto3;
    private Button btnMonto4;
    private Button btnMonto5;
    private Button btnMonto6;
    private Button btnAcceptTouchpadKeyboard;
    private TextView operationTitle;
    private TextView resultadoNumTextView;
    private ImageButton btnBackspace;
    private Dialog dialogTouchpadKeyboard;
    private OnValueEnteredListener listener;

    public ValuesKeyboardController(Activity activity, boolean autoClose) {
        this.context = activity;
        this.autoClose = autoClose;

        this.dialogTouchpadKeyboard = Utils.getNewDialog(activity, R.layout.dialog_teclado_tactil_montos, true, true, 910, ViewGroup.LayoutParams.WRAP_CONTENT);

        presetValues = (LinearLayout) this.dialogTouchpadKeyboard.findViewById(R.id.botonesPredeterminados);
        operationTitle = (TextView) this.dialogTouchpadKeyboard.findViewById(R.id.title);
        resultadoNumTextView = (TextView) this.dialogTouchpadKeyboard.findViewById(R.id.resultadoNum);
        btnUno = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_uno);
        btnDos = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_dos);
        btnTres = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_tres);
        btnCuatro = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_cuatro);
        btnCinco = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_cinco);
        btnSeis = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_seis);
        btnSiete = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_siete);
        btnOcho = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_ocho);
        btnNueve = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_nueve);
        btnCero = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_cero);
        btnDobleCero = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_doble_cero);
        btnTripleCero = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_triple_cero);
        btnMonto1 = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btnMonto1);
        btnMonto2 = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btnMonto2);
        btnMonto3 = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btnMonto3);
        btnMonto4 = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btnMonto4);
        btnMonto5 = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btnMonto5);
        btnMonto6 = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btnMonto6);
        btnBackspace = (ImageButton) this.dialogTouchpadKeyboard.findViewById(R.id.btn_backspace);
        btnAcceptTouchpadKeyboard = (Button) this.dialogTouchpadKeyboard.findViewById(R.id.btn_accept);

        btnUno.setText(context.getString(R.string.number_one_text));
        btnDos.setText(context.getString(R.string.number_two_text));
        btnTres.setText(context.getString(R.string.number_three_text));
        btnCuatro.setText(context.getString(R.string.number_four_text));
        btnCinco.setText(context.getString(R.string.number_five_text));
        btnSeis.setText(context.getString(R.string.number_six_text));
        btnSiete.setText(context.getString(R.string.number_seven_text));
        btnOcho.setText(context.getString(R.string.number_eight_text));
        btnNueve.setText(context.getString(R.string.number_nine_text));
        btnCero.setText(context.getString(R.string.number_zero_text));
        btnDobleCero.setText(context.getString(R.string.number_double_zero_text));
        btnTripleCero.setText(context.getString(R.string.number_triple_zero_text));
        btnMonto1.setText(Utils.moneyFormat(context.getString(R.string.number_five_thousand)));
        btnMonto2.setText(Utils.moneyFormat(context.getString(R.string.number_seven_thousand)));
        btnMonto3.setText(Utils.moneyFormat(context.getString(R.string.number_ten_thousand)));
        btnMonto4.setText(Utils.moneyFormat(context.getString(R.string.number_fiveteen_thousand)));
        btnMonto5.setText(Utils.moneyFormat(context.getString(R.string.number_twenty_thousand)));
        btnMonto6.setText(Utils.moneyFormat(context.getString(R.string.number_fifty_thousand)));

        btnUno.setOnClickListener(this);
        btnDos.setOnClickListener(this);
        btnTres.setOnClickListener(this);
        btnCuatro.setOnClickListener(this);
        btnCinco.setOnClickListener(this);
        btnSeis.setOnClickListener(this);
        btnSiete.setOnClickListener(this);
        btnOcho.setOnClickListener(this);
        btnNueve.setOnClickListener(this);
        btnCero.setOnClickListener(this);
        btnDobleCero.setOnClickListener(this);
        btnTripleCero.setOnClickListener(this);
        btnMonto1.setOnClickListener(this);
        btnMonto2.setOnClickListener(this);
        btnMonto3.setOnClickListener(this);
        btnMonto4.setOnClickListener(this);
        btnMonto5.setOnClickListener(this);
        btnMonto6.setOnClickListener(this);
        btnBackspace.setOnClickListener(this);
        btnAcceptTouchpadKeyboard.setOnClickListener(this);

        resultadoNumTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (resultadoNumTextView.getText().toString().isEmpty()) {
                    btnAcceptTouchpadKeyboard.setEnabled(false);
                }
                else {
                    btnAcceptTouchpadKeyboard.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnBackspace.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                resultadoNumTextView.setText("");

                return false;
            }
        });

        this.dialogTouchpadKeyboard.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                resultadoNumTextView.setText(GlobalData.EMPTY);
                btnAcceptTouchpadKeyboard.setEnabled(false);
            }
        });
    }

    public void showKeyboard(KeyboardTypes keyboardType, OnValueEnteredListener onValueEnteredListener) {
        selectTouchpadKeyboardType(keyboardType);

        this.listener = onValueEnteredListener;

        if(!dialogTouchpadKeyboard.isShowing()) {
            dialogTouchpadKeyboard.show();

            if(keyboardType == KeyboardTypes.OPERATIVE_COUNTING) {
                dialogTouchpadKeyboard.setCanceledOnTouchOutside(false);
                dialogTouchpadKeyboard.setCancelable(false);
            }
            else {
                dialogTouchpadKeyboard.setCanceledOnTouchOutside(true);
                dialogTouchpadKeyboard.setCancelable(true);
            }
        }
    }

    public void selectTouchpadKeyboardType(KeyboardTypes keyboardType) {
        switch(keyboardType) {
            case TRACKING:
                operationTitle.setText(Utils.getFormattedClientName(context.getString(R.string.tracking_text)));

                presetValues.setVisibility(View.VISIBLE);

                btnAcceptTouchpadKeyboard.setText(context.getString(R.string.perform_tracking_text));
            break;

            case MONEY_CHANGE:
                operationTitle.setText(Utils.getFormattedClientName(context.getString(R.string.money_change_text)));

                presetValues.setVisibility(View.VISIBLE);

                btnAcceptTouchpadKeyboard.setText(context.getString(R.string.change_money_text));
            break;

            case PRIZE:
                operationTitle.setText(Utils.getFormattedClientName(context.getString(R.string.prize_text)));

                presetValues.setVisibility(View.VISIBLE);

                btnAcceptTouchpadKeyboard.setText(context.getString(R.string.enter_prize_text));
            break;

            case OPERATIVE_COUNTING:
                operationTitle.setText(Utils.getFormattedClientName(context.getString(R.string.operative_counting_text)));

                presetValues.setVisibility(View.GONE);

                btnAcceptTouchpadKeyboard.setText(context.getString(R.string.enter_operative_counting_text));
            break;

            case OPERATIVE_COUNTING_ON_DEMAND:
                operationTitle.setText(Utils.getFormattedClientName(context.getString(R.string.operative_counting_text)));

                presetValues.setVisibility(View.GONE);

                btnAcceptTouchpadKeyboard.setText(context.getString(R.string.enter_operative_counting_text));
            break;

            default:
                operationTitle.setText(Utils.getFormattedClientName(context.getString(R.string.tracking_text)));

                presetValues.setVisibility(View.VISIBLE);

                btnAcceptTouchpadKeyboard.setText(context.getString(R.string.perform_tracking_text));
            break;
        }
    }

    public void closeKeyboard() {
        dialogTouchpadKeyboard.dismiss();
    }

    @Override
    public void onClick(View view) {
        if(view == btnUno) {
            String tmpStr = resultadoNumTextView.getText().toString();

            resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_one_text)));
        }
        else if(view == btnDos) {
            String tmpStr = resultadoNumTextView.getText().toString();

            resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_two_text)));
        }
        else if(view == btnTres) {
            String tmpStr = resultadoNumTextView.getText().toString();

            resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_three_text)));
        }
        else if(view == btnCuatro) {
            String tmpStr = resultadoNumTextView.getText().toString();

            resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_four_text)));
        }
        else if(view == btnCinco) {
            String tmpStr = resultadoNumTextView.getText().toString();

            resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_five_text)));
        }
        else if(view == btnSeis) {
            String tmpStr = resultadoNumTextView.getText().toString();

            resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_six_text)));
        }
        else if(view == btnSiete) {
            String tmpStr = resultadoNumTextView.getText().toString();

            resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_seven_text)));
        }
        else if(view == btnOcho) {
            String tmpStr = resultadoNumTextView.getText().toString();

            resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_eight_text)));
        }
        else if(view == btnNueve) {
            String tmpStr = resultadoNumTextView.getText().toString();

            resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_nine_text)));
        }
        else if(view == btnCero) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(tmpStr.length() > 0) {
                resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_zero_text)));
            }
        }
        else if(view == btnDobleCero) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(!tmpStr.isEmpty()) {if(tmpStr.length() <= 9) {
                    resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_double_zero_text)));
                }
                else if(tmpStr.length() == 10) {
                    resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_zero_text)));
                }
            }
        }
        else if(view == btnTripleCero) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(!tmpStr.isEmpty()) {
                if(tmpStr.length() <= 7) {
                    resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_triple_zero_text)));
                }
                else if(tmpStr.length() == 9) {
                    resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_double_zero_text)));
                }
                else if(tmpStr.length() == 10) {
                    resultadoNumTextView.setText(Utils.numberFormat(tmpStr + context.getString(R.string.number_zero_text)));
                }
            }
        }
        else if(view == btnMonto1) {
            resultadoNumTextView.setText(Utils.numberFormat(context.getString(R.string.number_five_thousand)));
        }
        else if(view == btnMonto2) {
            resultadoNumTextView.setText(Utils.numberFormat(context.getString(R.string.number_seven_thousand)));
        }
        else if(view == btnMonto3) {
            resultadoNumTextView.setText(Utils.numberFormat(context.getString(R.string.number_ten_thousand)));
        }
        else if(view == btnMonto4) {
            resultadoNumTextView.setText(Utils.numberFormat(context.getString(R.string.number_fiveteen_thousand)));
        }
        else if(view == btnMonto5) {
            resultadoNumTextView.setText(Utils.numberFormat(context.getString(R.string.number_twenty_thousand)));
        }
        else if(view == btnMonto6) {
            resultadoNumTextView.setText(Utils.numberFormat(context.getString(R.string.number_fifty_thousand)));
        }
        else if(view == btnBackspace) {
            String tmpStr = resultadoNumTextView.getText().toString();

            if(!tmpStr.isEmpty()) {
                String newStr = tmpStr.substring(0, tmpStr.length()-1);

                if(newStr.length() > 1) {
                    String lastChar = String.valueOf(newStr.charAt(newStr.length() - 1));

                    if(lastChar.equals(".")) newStr = newStr.replace(lastChar, GlobalData.EMPTY);
                }

                resultadoNumTextView.setText((newStr.length() >= 1) ? Utils.numberFormat(newStr) : GlobalData.EMPTY );
            }
        }
        else if(view == btnAcceptTouchpadKeyboard) {
            listener.onValueEntered(view, Utils.getCleanString(resultadoNumTextView.getText().toString()));

            if(autoClose) dialogTouchpadKeyboard.dismiss();
        }
    }
}
