package com.jumpitt.winmanagement.utils;

public enum TableOptions { REFRESH_TABLE_IMG, GET_PARAMS, UPDATE_MIN_MAX, OPERATIVE_COUNTING, CLOSE_TABLE };