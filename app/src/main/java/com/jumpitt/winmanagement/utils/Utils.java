package com.jumpitt.winmanagement.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.ParametroDM;
import com.jumpitt.winmanagement.webservices.ServicioMesas;
import com.orhanobut.logger.Logger;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static Locale locale = Locale.getDefault();
    private static boolean successGetParams = false;

    public static String getAppVersion(Activity activity) {
        String version = "1.0";

        try {
            version = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return version;
    }

    public static void setMargins(View v, int left, int top, int right, int bottom) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            v.requestLayout();
        }
    }

    public static void setCurrentTime24Hours(final TextView clockButton) {
        final Handler getCurrentTimeHandler = new Handler();

        Runnable getCurrentTimeRunnable = new Runnable() {
            public void run() {
                clockButton.setText(Utils.getCurrentTime());

                getCurrentTimeHandler.postDelayed(this, 1000);
            }
        };

        getCurrentTimeHandler.postDelayed(getCurrentTimeRunnable, 1000);
    }

    public static void startViewAnimation(Context c, View v, int anim) {
        v.startAnimation(AnimationUtils.loadAnimation(c, anim));
    }

    public static void stopViewAnimation(View v) {
        v.clearAnimation();
    }

    public static void restartViewAnimation(View v) {
        if(v.getAnimation() != null) v.getAnimation().reset();
    }

    public static boolean isAlfanumeric(String digit) {
        Pattern pat = Pattern.compile("^([0-9a-zA-Z])$");
        Matcher mat = pat.matcher(digit);

        if (mat.find()) {
            System.out.println("[" + mat.group() + "]");
            return true;
        }
        else{
            return false;
        }
    }

    public static String getDiffFechas(String fecha1, String fecha2) {
        String res, hr5, min5, seg5;
        String[] fecha1Completa, fecha2Completa, fecha1Str, fecha2Str, hora1Str, hora2Str;
        int dia1, dia2, mes1, mes2, anio1, anio2, hr1, hr2, min1, min2, seg1, seg2, tiempoTotal1, tiempoTotal2, anios3, meses3, dias3, hr3, min3, seg3;
        double tiempoTotalFinal;

        fecha1Completa = fecha1.split(" ");
        fecha2Completa = fecha2.split(" ");

        fecha1Str = fecha1Completa[0].split("-");
        fecha2Str = fecha2Completa[0].split("-");

        hora1Str = fecha1Completa[1].split(":");
        hora2Str = fecha2Completa[1].split(":");

        dia1 = Integer.parseInt(fecha1Str[2]);
        dia2 = Integer.parseInt(fecha2Str[2]);

        mes1 = Integer.parseInt(fecha1Str[1]);
        mes2 = Integer.parseInt(fecha2Str[1]);

        anio1 = Integer.parseInt(fecha1Str[0]);
        anio2 = Integer.parseInt(fecha2Str[0]);

        hr1 = Integer.parseInt(hora1Str[0]);
        hr2 = Integer.parseInt(hora2Str[0]);

        min1 = Integer.parseInt(hora1Str[1]);
        min2 = Integer.parseInt(hora2Str[1]);

        seg1 = Integer.parseInt(hora1Str[2]);
        seg2 = Integer.parseInt(hora2Str[2]);

        tiempoTotal1 = seg1 + (min1*60) + (hr1*3600) + (dia1*3600*24) + (mes1*3600*24*30) + (anio1*3600*24*30*12);
        tiempoTotal2 = seg2 + (min2*60) + (hr2*3600) + (dia2*3600*24) + (mes2*3600*24*30) + (anio2*3600*24*30*12);

        tiempoTotalFinal = tiempoTotal1 - tiempoTotal2;

        anios3 = (int) (tiempoTotalFinal / (3600 * 24 * 30 * 12));
        tiempoTotalFinal = tiempoTotalFinal - (3600 * 24 * 30 * 12 * anios3);

        meses3 = (int) (tiempoTotalFinal / (3600 * 24 * 30));
        tiempoTotalFinal = tiempoTotalFinal - (3600 * 24 * 30 * meses3);

        dias3 = (int) (tiempoTotalFinal / (3600 * 24));
        tiempoTotalFinal = tiempoTotalFinal - (3600 * 24 * dias3);

        hr3 = (int) (tiempoTotalFinal / 3600);
        tiempoTotalFinal = tiempoTotalFinal - (3600 * hr3);

        min3 = (int) (tiempoTotalFinal / 60);
        tiempoTotalFinal = tiempoTotalFinal - (60 * min3);

        seg3 = (int) tiempoTotalFinal;

        long anios4,meses4,dias4,hr4,min4,seg4;

        anios4 = (long) anios3;
        meses4 = (long) meses3;
        dias4 = (long) dias3;
        hr4 = (long) hr3;
        min4 = (long) min3;
        seg4 = (long) seg3;

        hr5 = ((hr4 < 10) ? "0" + hr4 + ":" : hr4 + ":");
        min5 = ((min4 < 10) ? "0" + min4 + ":" : min4 + ":");
        seg5 = ((seg4 < 10) ? "0" + seg4 : seg4 + GlobalData.EMPTY);

        res = ((anios4 > 0) ? anios4 + "a " : GlobalData.EMPTY) +
                ((meses4 > 0) ? meses4 + "m " : GlobalData.EMPTY) +
                ((dias4 > 0) ? dias4 + "d " : GlobalData.EMPTY) +
                ((hr4 > 0) ? hr5 : "00:") +
                ((min4 > 0) ? min5 : "00:") +
                ((seg4 > 0) ? seg5 : "00");

        return res;
    }

    public static String getCleanString(String input) {
        return input.replace(".", GlobalData.EMPTY).replace(",", GlobalData.EMPTY).replace(";", GlobalData.EMPTY).replace("$", GlobalData.EMPTY).replace(" ", GlobalData.EMPTY).replace("-", GlobalData.EMPTY);
    }

    public static int StrToInt(String num) {
        return Integer.parseInt(num.replace(".", GlobalData.EMPTY).replace("$", GlobalData.EMPTY).replace(" ", GlobalData.EMPTY).replace(GlobalData.EMPTY, GlobalData.EMPTY));
    }

    public static String IntToStr(int num) {
        return String.valueOf(num);
    }

    public static String numberFormat(int num) {
        String v = String.valueOf(num);

        if (v.length() <= 11) {
            NumberFormat numberFormat = new DecimalFormat("#,###,###");
            v = numberFormat.format(Long.valueOf(v));
            v = v.replace(",", ".");
        }

        return v;
    }

    public static String numberFormat(String num) {
        if (num.length() <= 11) {
            NumberFormat numberFormat = new DecimalFormat("#,###,###");
            num = numberFormat.format(Long.valueOf(num.replace(".", GlobalData.EMPTY)));
            num = num.replace(",", ".");
        }

        return num;
    }

    public static String moneyFormat(long num) {
        NumberFormat numberFormat = new DecimalFormat("#,###,###");
        String v = String.valueOf(num);
        v = numberFormat.format(Long.valueOf(v));
        v = v.replace(",", ".");
        v = "$" + v;

        return v;
    }

    public static String moneyFormat(int num) {
        NumberFormat numberFormat = new DecimalFormat("#,###,###");
        String v = String.valueOf(num);
        v = numberFormat.format(Long.valueOf(v));
        v = v.replace(",", ".");
        v = "$" + v;

        return v;
    }

    public static String moneyFormat(String num) {
        NumberFormat numberFormat = new DecimalFormat("#,###,###");
        num = numberFormat.format(Long.valueOf(num.replace(".", GlobalData.EMPTY)));
        num = num.replace(",", ".");
        num = "$" + num;

        return num;
    }

    public static boolean getParamsFromDB(String id_unidad) {
        ArrayList<ParametroDM> resultGetParams = ServicioMesas.getParamsFromDB(id_unidad);

        if(!resultGetParams.isEmpty()) {
            for (ParametroDM param : resultGetParams) {
                String valorParam = param.getValorParametro().replace(",", ".");

                if (param.getNombreParametro().equals(GlobalData.LIMITE_PEDIDOS_COMPS_TEXT))
                    GlobalData.LIMITE_PEDIDOS_COMPS = Integer.parseInt(valorParam);
                else if (param.getNombreParametro().equals(GlobalData.LIMITE_INGREDIENTES_COMPS_TEXT))
                    GlobalData.LIMITE_INGREDIENTES_COMPS = Integer.parseInt(valorParam);
                else if (param.getNombreParametro().equals(GlobalData.LIMITE_INFO_EXTRA_COMPS_TEXT))
                    GlobalData.LIMITE_INFO_EXTRA_COMPS = Integer.parseInt(valorParam);
                else if (param.getNombreParametro().equals(GlobalData.LIMITE_INFERIOR_TRACKING_TEXT))
                    GlobalData.LIMITE_INFERIOR_TRACKING = Integer.parseInt(valorParam);
                else if (param.getNombreParametro().equals(GlobalData.LIMITE_SUPERIOR_TRACKING_TEXT))
                    GlobalData.LIMITE_SUPERIOR_TRACKING = Integer.parseInt(valorParam);
                else if (param.getNombreParametro().equals(GlobalData.TOKEN_PRODUCTOS_COMPS_TEXT))
                    GlobalData.TOKEN_PRODUCTOS_COMPS = valorParam;
            }

            Logger.d("GlobalData.LIMITE_PEDIDOS_COMPS= "+GlobalData.LIMITE_PEDIDOS_COMPS+"\n"+
                    "GlobalData.LIMITE_INGREDIENTES_COMPS= "+GlobalData.LIMITE_INGREDIENTES_COMPS+"\n"+
                    "GlobalData.LIMITE_INFO_EXTRA_COMPS= "+GlobalData.LIMITE_INFO_EXTRA_COMPS+"\n"+
                    "GlobalData.LIMITE_INFERIOR_TRACKING= "+GlobalData.LIMITE_INFERIOR_TRACKING+"\n"+
                    "GlobalData.LIMITE_SUPERIOR_TRACKING= "+GlobalData.LIMITE_SUPERIOR_TRACKING+"\n"+
                    "GlobalData.TOKEN_PRODUCTOS_COMPS= "+GlobalData.TOKEN_PRODUCTOS_COMPS);

            return true;
        }

        return false;
    }

    public static String formatDecimal(Double number, int numDecimals)
    {
        String formattedDecimal = number.toString();

        try
        {
            Formatter formatter = new Formatter();

            formattedDecimal = formatter.format("%."+numDecimals+"f", number).toString();

            formatter.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return formattedDecimal;
    }

    public static String SHA1(String text){
        MessageDigest md;
        String result = GlobalData.EMPTY;

        try {
            md = MessageDigest.getInstance("SHA-1");

            md.update(text.getBytes("utf-8"), 0, text.length());

            byte[] sha1hash = md.digest();

            StringBuilder buf = new StringBuilder();

            for(byte b : sha1hash){
                int halfbyte = (b >>> 4) & 0x0F;
                int two_halfs = 0;

                do{
                    buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                    halfbyte = b & 0x0F;
                }
                while(two_halfs++ < 1);
            }

            result = buf.toString();
        }
        catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String getSecurityToken() {
        return SHA1(getCurrentDate(GlobalData.EMPTY));
    }

    public static String getMacAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        String macAddress = GlobalData.EMPTY;

        try {
            WifiInfo wInfo = wifiManager.getConnectionInfo();

            macAddress = wInfo.getMacAddress().toUpperCase(locale);
        } catch (Exception e) {
            Logger.e(e.toString());
        }

        return macAddress;
    }

    public static Bitmap getBitmapFromDrawable(Drawable drawable) {
        return ((BitmapDrawable) drawable).getBitmap();
    }

    public static Drawable getDrawableFromBase64(Context c, String imgBase64) {
        Bitmap bitmap = null;

        try {
            byte[] encodeByte = Base64.decode(imgBase64, Base64.DEFAULT);

            bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        }
        catch (Exception e) {
            e.printStackTrace();

            bitmap = null;
        }

        return new BitmapDrawable(c.getResources(), bitmap);
    }

    public static Drawable getDrawableFromBitmap(Context c, Bitmap bitmap) {
        return new BitmapDrawable(c.getResources(), bitmap);
    }

    public static Bitmap getBitmapFromBlob(byte[] blob) {
        try {
            return BitmapFactory.decodeByteArray(blob, 0, blob.length);
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static Bitmap getBitmapFromBase64(String imgBase64) {
        try {
            byte[] encodeByte = Base64.decode(imgBase64, Base64.DEFAULT);

            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        }
        catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static String getBase64FromBitmap(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();

        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public static String getCurrentDate(String separator) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd" + separator + "MM" + separator + "yyyy", locale);

        return sdf.format(new Date());
    }

    public static String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", locale);

        return sdf.format(new Date());
    }

    public static String getDateFormat(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale);

        return sdf.format(new Date());
    }

    public static long dateToUnix(String date, String format) {
        SimpleDateFormat formatter;
        Date dt = null;

        formatter = new SimpleDateFormat(format, Utils.locale);

        try {
            dt = formatter.parse(date);
        }
        catch (ParseException ex) {
            ex.printStackTrace();
        }

        return (dt != null ? dt.getTime() / 1000L : -1);
    }

    public static String unixToStringDate(long t, String format) {
        Date date = new Date(t * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat(format, Utils.locale); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-3"));

        return sdf.format(date);
    }

    public static String formatDate(String date, String dateFormat, String newFormat) {
        String now = date;

        try {
            SimpleDateFormat newDateFormat = new SimpleDateFormat(newFormat, locale);
            SimpleDateFormat originalDateFormat = new SimpleDateFormat(dateFormat, locale);
            Date parsedDate = originalDateFormat.parse(date);

            Calendar calendar = Calendar.getInstance();

            calendar.setTime(parsedDate);

            long startTime = calendar.getTimeInMillis();

            Date convertedDate = new Date(startTime);

            now = newDateFormat.format(convertedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return now;
    }

    public static void toast(Context contexto, String msj) {
        Toast.makeText(contexto, msj, Toast.LENGTH_SHORT).show();
    }

    public static void toast(Context contexto, String msj, int duracion) {
        switch (duracion) {
            case 0:
                Toast.makeText(contexto, msj, Toast.LENGTH_SHORT).show();
                break;

            case 1:
                Toast.makeText(contexto, msj, Toast.LENGTH_LONG).show();
                break;
        }
    }

    public static void toast(Context contexto, int msj, int duracion) {
        switch (duracion) {
            case 0:
                Toast.makeText(contexto, msj, Toast.LENGTH_SHORT).show();
                break;

            case 1:
                Toast.makeText(contexto, msj, Toast.LENGTH_LONG).show();
                break;
        }
    }

    public static Dialog getAbout(Activity activity, int width, int height, boolean canceledOnTouchOutside) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_acerca_de, null);

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(width, height));
        dialog.setCanceledOnTouchOutside(canceledOnTouchOutside);
        dialog.setCancelable(canceledOnTouchOutside);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView versionApp = (TextView) dialog.findViewById(R.id.versionApp);

        String version = getAppVersion(activity);

        versionApp.setText("v" + version);

        if(!canceledOnTouchOutside) {
            dialoglayout.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent motionEvent) {
                    v.performClick();

                    dialog.dismiss();

                    return false;
                }
            });
        }

        dialog.onBackPressed();

        return dialog;
    }

    public static void showInformationDialog(Activity activity, boolean cancelableOnTouchOutside, String message, int timer) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_informacion, null);

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(600, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView msg = (TextView) dialog.findViewById(R.id.message);

        msg.setText(message.isEmpty() ? activity.getString(R.string.try_again) : message.toUpperCase(locale));

        dialog.onBackPressed();

        dialog.show();

        Handler h = new Handler();

        Runnable r = new Runnable() {
            public void run() {
                try {
                    if(dialog.isShowing()) dialog.dismiss();
                }
                catch(Exception e) {
                    Logger.e(e.getMessage());
                }
            }
        };

        if (!cancelableOnTouchOutside) h.postDelayed(r, timer);
    }

    public static void showSuccessOperationDialog(Activity activity, boolean cancelableOnTouchOutside, String message, int timer) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_operacion_realizada_correctamente, null);

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(600, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView msg = (TextView) dialog.findViewById(R.id.message);

        msg.setText(message.isEmpty() ? activity.getString(R.string.success_operation) : message.toUpperCase(locale) );

        dialog.onBackPressed();

        dialog.show();

        Handler h = new Handler();

        Runnable r = new Runnable() {
            public void run() {
                dialog.dismiss();
            }
        };

        if (!cancelableOnTouchOutside) h.postDelayed(r, timer);
    }

    public static Dialog getSuccessOperationDialog(Activity activity, boolean cancelableOnTouchOutside, String message) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_operacion_realizada_correctamente, null);

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(600, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView msg = (TextView) dialog.findViewById(R.id.message);

        msg.setText(message.isEmpty() ? activity.getString(R.string.success_operation) : message.toUpperCase(locale) );

        dialog.onBackPressed();

        return dialog;
    }

    public static void showTemporalDialog(Activity activity, int layout, boolean cancelableOnTouchOutside, boolean removeBackground, int width, int height, int timeToDismiss) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(layout, null);

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(width, height));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        if (removeBackground) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        dialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, timeToDismiss);
    }

    public static void showTemporalDialog(final Dialog dialog, int timeToDismiss) {
        dialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, timeToDismiss);
    }

    public static void newDialog(Activity activity, int layout, boolean cancelableOnTouchOutside) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(layout, null);

        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
    }

    public static void newDialog(Activity activity, int layout, boolean cancelableOnTouchOutside, int width, int height) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(layout, null);

        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(width, height));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
    }

    public static void newDialog(Activity activity, int layout, boolean cancelableOnTouchOutside, boolean removeBackground) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(layout, null);

        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        if (removeBackground) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        dialog.show();
    }

    public static void newDialog(Activity activity, int layout, boolean cancelableOnTouchOutside, boolean removeBackground, int width, int height) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(layout, null);

        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(width, height));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        if (removeBackground) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        dialog.show();
    }

    public static Dialog getNewDialog(Activity activity, int layout, boolean cancelableOnTouchOutside) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(layout, null);

        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }

    public static Dialog getNewDialog(Activity activity, int layout, boolean cancelableOnTouchOutside, int width, int height) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(layout, null);

        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(width, height));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }

    public static Dialog getNewDialog(Activity activity, int layout, boolean cancelableOnTouchOutside, boolean removeBackground) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(layout, null);

        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        if (removeBackground) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        return dialog;
    }

    public static Dialog getNewDialog(Activity activity, int layout, boolean cancelableOnTouchOutside, boolean removeBackground, int width, int height) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialoglayout = inflater.inflate(layout, null);

        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.addContentView(dialoglayout, new ViewGroup.LayoutParams(width, height));
        dialog.setCanceledOnTouchOutside(cancelableOnTouchOutside);
        dialog.setCancelable(cancelableOnTouchOutside);

        if (removeBackground) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        return dialog;
    }

    // ProgressBar
    public static void newProgressDialog(Context context, String mensaje, boolean onTouchOutSide) {
        ProgressDialog pd = new ProgressDialog(context);
        pd.setMessage(mensaje);
        pd.setCanceledOnTouchOutside(onTouchOutSide);
        pd.setCancelable(onTouchOutSide);

        pd.show();
    }

    public static void newProgressDialog(Context context, int mensaje, boolean onTouchOutSide) {
        ProgressDialog pd = new ProgressDialog(context);
        pd.setMessage(context.getResources().getString(mensaje));
        pd.setCanceledOnTouchOutside(onTouchOutSide);
        pd.setCancelable(onTouchOutSide);

        pd.show();
    }

    public static ProgressDialog getNewProgressDialog(Context context, String mensaje, boolean onTouchOutSide) {
        ProgressDialog pd = new ProgressDialog(context);
        pd.setMessage(mensaje);
        pd.setCanceledOnTouchOutside(onTouchOutSide);
        pd.setCancelable(onTouchOutSide);

        return pd;
    }

    public static ProgressDialog getNewProgressDialog(Context context, int mensaje, boolean onTouchOutSide) {
        ProgressDialog pd = new ProgressDialog(context);
        pd.setMessage(context.getResources().getString(mensaje));
        pd.setCanceledOnTouchOutside(onTouchOutSide);
        pd.setCancelable(onTouchOutSide);

        return pd;
    }

    public static String getOrderedUserName(String fullName) {
        String fixedName = fullName.trim();

        try {
            String[] nameArray = fullName.split(" ");

            switch (nameArray.length) {
                case 2:
                    fixedName = nameArray[1] + " " + nameArray[0].substring(0, 1) + ".";
                break;

                case 3:
                    fixedName = nameArray[2] + " " + nameArray[0].substring(0, 1) + ".";
                break;

                case 4:
                    fixedName = nameArray[2] + " " + nameArray[0].substring(0, 1) + ".";
                break;

                case 5:
                    fixedName = nameArray[2] + " " + nameArray[0].substring(0, 1) + ".";
                break;

                case 6:
                    fixedName = nameArray[3] + " " + nameArray[0].substring(0, 1) + ".";
                break;

                default:
                    fixedName = fullName.trim();
                break;
            }
        }
        catch(Exception e) {
            e.printStackTrace();

            return fullName;
        }

        return fixedName;
    }

    public static String getFormattedClientName(String fullName) {
        String fixedName = fullName.trim();

        try {
            String[] nameArray = fullName.toLowerCase(locale).split(" ");

            switch (nameArray.length) {
                case 1:
                    fixedName = nameArray[0].substring(0, 1).toUpperCase(locale) + nameArray[0].substring(1);
                    break;

                case 2:
                    fixedName = nameArray[0].substring(0, 1).toUpperCase(locale) + nameArray[0].substring(1) + " " +
                            nameArray[1].substring(0, 1).toUpperCase(locale) + nameArray[1].substring(1);
                    break;

                case 3:
                    fixedName = nameArray[0].substring(0, 1).toUpperCase(locale) + nameArray[0].substring(1) + " " +
                            nameArray[1].substring(0, 1).toUpperCase(locale) + nameArray[1].substring(1) + " " +
                            nameArray[2].substring(0, 1).toUpperCase(locale) + nameArray[2].substring(1);
                    break;

                case 4:
                    fixedName = nameArray[0].substring(0, 1).toUpperCase(locale) + nameArray[0].substring(1) + " " +
                            nameArray[1].substring(0, 1).toUpperCase(locale) + nameArray[1].substring(1) + " " +
                            nameArray[2].substring(0, 1).toUpperCase(locale) + nameArray[2].substring(1) + " " +
                            nameArray[3].substring(0, 1).toUpperCase(locale) + nameArray[3].substring(1);
                    break;

                case 5:
                    fixedName = nameArray[0].substring(0, 1).toUpperCase(locale) + nameArray[0].substring(1) + " " +
                            nameArray[1].substring(0, 1).toUpperCase(locale) + nameArray[1].substring(1) + " " +
                            nameArray[2].substring(0, 1).toUpperCase(locale) + nameArray[2].substring(1) + " " +
                            nameArray[3].substring(0, 1).toUpperCase(locale) + nameArray[3].substring(1) + " " +
                            nameArray[4].substring(0, 1).toUpperCase(locale) + nameArray[4].substring(1);
                    break;

                case 6:
                    fixedName = nameArray[0].substring(0, 1).toUpperCase(locale) + nameArray[0].substring(1) + " " +
                            nameArray[1].substring(0, 1).toUpperCase(locale) + nameArray[1].substring(1) + " " +
                            nameArray[2].substring(0, 1).toUpperCase(locale) + nameArray[2].substring(1) + " " +
                            nameArray[3].substring(0, 1).toUpperCase(locale) + nameArray[3].substring(1) + " " +
                            nameArray[4].substring(0, 1).toUpperCase(locale) + nameArray[4].substring(1) + " " +
                            nameArray[5].substring(0, 1).toUpperCase(locale) + nameArray[5].substring(1);
                    break;

                case 7:
                    fixedName = nameArray[0].substring(0, 1).toUpperCase(locale) + nameArray[0].substring(1) + " " +
                            nameArray[1].substring(0, 1).toUpperCase(locale) + nameArray[1].substring(1) + " " +
                            nameArray[2].substring(0, 1).toUpperCase(locale) + nameArray[2].substring(1) + " " +
                            nameArray[3].substring(0, 1).toUpperCase(locale) + nameArray[3].substring(1) + " " +
                            nameArray[4].substring(0, 1).toUpperCase(locale) + nameArray[4].substring(1) + " " +
                            nameArray[5].substring(0, 1).toUpperCase(locale) + nameArray[5].substring(1) + " " +
                            nameArray[6].substring(0, 1).toUpperCase(locale) + nameArray[6].substring(1);
                    break;

                default:
                    fixedName = fullName.trim();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fixedName;
    }

    public static String formatRUT(String rut) {
        int cont = 0;
        String format;

        rut = rut.replace(".", GlobalData.EMPTY).replace("-", GlobalData.EMPTY);

        format = "-" + rut.substring(rut.length() - 1);

        for (int i = rut.length() - 2; i >= 0; i--) {
            format = rut.substring(i, i + 1) + format;

            cont++;

            if (cont == 3 && i != 0) {
                format = "." + format;
                cont = 0;
            }
        }

        return format;
    }

    public static boolean isEvenNumber(int number) {
        return (number%2 == 0);
    }

    public static void generateNoteOnSD(String sFileName, String fileExtension, String sBody) {
        String DIR_NAME = "Notes";

        try {
            File root = new File(Environment.getExternalStorageDirectory(), DIR_NAME);

            if (!root.exists()) {
                root.mkdirs();
            }

            File file = new File(root, String.format("%s.%s", sFileName, fileExtension));

            StringBuilder text = new StringBuilder();

            if(file.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();

                sBody = text.toString() + sBody;
            }

            FileWriter writer = new FileWriter(file);
            writer.append(sBody);
            writer.flush();
            writer.close();

            Logger.d("Log saved!");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createErrorFile(Exception e) {
        String error = GlobalData.EMPTY;

        error += "Fecha y hora: " + Utils.getDateFormat("dd-MM-yyyy HH:mm:ss") + "\n\n" + e.toString() + "\n";

        for(StackTraceElement stackTraceElement : e.getStackTrace()) {
            error += stackTraceElement + "\n";
        }

        error  += "\n\n";

        Logger.e(error);

        generateNoteOnSD("Error-Log_"+Utils.getDateFormat("dd-MM-yyyy"), "txt", error);
    }
}
