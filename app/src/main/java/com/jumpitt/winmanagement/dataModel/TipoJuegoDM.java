package com.jumpitt.winmanagement.dataModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TipoJuegoDM implements Serializable {
	private String nombre_tipo_juego;
	private String imagen_b64;
	private List<MesaDM> mesas;

	public TipoJuegoDM() {
		this.nombre_tipo_juego = "";
		this.imagen_b64 = "";
		this.mesas = new ArrayList<MesaDM>();
	}

	public TipoJuegoDM(String nombre_tipo_juego, String imagen_b64, List<MesaDM> mesas) {
		this.nombre_tipo_juego = nombre_tipo_juego;
		this.imagen_b64 = imagen_b64;
		this.mesas = mesas;
	}

	public String getNombre_tipo_juego() {
		return nombre_tipo_juego;
	}

	public void setNombre_tipo_juego(String nombre_tipo_juego) {
		this.nombre_tipo_juego = nombre_tipo_juego;
	}

	public String getImagen_b64() {
		return imagen_b64;
	}

	public void setImagen_b64(String imagen_b64) {
		this.imagen_b64 = imagen_b64;
	}

	public List<MesaDM> getMesas() {
		return mesas;
	}

	public void setMesas(List<MesaDM> mesas) {
		this.mesas = mesas;
	}
}