package com.jumpitt.winmanagement.dataModel;

import java.util.ArrayList;

public class ResultCompsDM {
    public ResultListCompsDM resultListComps;
    public ArrayList<ModificadorCompDM> ingredientsComps;
    public ArrayList<ModificadorCompDM> additionalInfoComps;

    public ResultCompsDM() {
        this.resultListComps = null;
        this.ingredientsComps = new ArrayList<>();
        this.additionalInfoComps = new ArrayList<>();
    }

    public ResultCompsDM(ResultListCompsDM resultListComps, ArrayList<ModificadorCompDM> ingredientsComps,
                         ArrayList<ModificadorCompDM> additionalInfoComps) {
        this.resultListComps = resultListComps;
        this.ingredientsComps = ingredientsComps;
        this.additionalInfoComps = additionalInfoComps;
    }
}
