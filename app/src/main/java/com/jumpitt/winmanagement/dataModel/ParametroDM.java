package com.jumpitt.winmanagement.dataModel;

import java.io.Serializable;

public class ParametroDM implements Serializable
{
	private static final long serialVersionUID = -8851814568024780812L;
	
	String idParametro;
	String nombreParametro;
	String valorParametro;
	
	public String getIdParametro()
	{
		return idParametro;
	}
	
	public void setIdParametro(String idParametro)
	{
		this.idParametro = idParametro;
	}

	public String getNombreParametro()
	{
		return nombreParametro;
	}

	public void setNombreParametro(String nombreParametro)
	{
		this.nombreParametro = nombreParametro;
	}

	public String getValorParametro()
	{
		return valorParametro;
	}

	public void setValorParametro(String valorParametro)
	{
		this.valorParametro = valorParametro;
	}	
}
