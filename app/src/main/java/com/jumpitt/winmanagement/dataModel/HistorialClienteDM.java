package com.jumpitt.winmanagement.dataModel;

import com.jumpitt.winmanagement.utils.GlobalData;

import java.util.ArrayList;

public class HistorialClienteDM {
    public String id_cliente;
    public String id_cliente_mcc;
    public String tiempo_juego;
    public String producto_preferido;
    public long consumo_cliente;
    public long win_teorico;
    public long monto_cambios_dinero;
    public int cantidad_cambios_dinero;
    public int cantidad_comps;
    public ArrayList<ItemDetalleHistorialDM> listaOperacionesCliente;

    public HistorialClienteDM() {
        this.id_cliente = GlobalData.EMPTY;
        this.id_cliente_mcc = GlobalData.EMPTY;
        this.tiempo_juego = GlobalData.EMPTY;
        this.producto_preferido = GlobalData.EMPTY;
        this.consumo_cliente = 0;
        this.win_teorico = 0;
        this.monto_cambios_dinero = 0;
        this.cantidad_cambios_dinero = 0;
        this.cantidad_comps = 0;
        this.listaOperacionesCliente = new ArrayList<ItemDetalleHistorialDM>();
    }

    public HistorialClienteDM(String id_cliente, String id_cliente_mcc, String tiempo_juego, String producto_preferido, long consumo_cliente,
                              long win_teorico, long monto_cambios_dinero, int cantidad_cambios_dinero, int cantidad_comps,
                              ArrayList<ItemDetalleHistorialDM> listaOperacionesCliente) {
        this.id_cliente = id_cliente;
        this.id_cliente_mcc = id_cliente_mcc;
        this.tiempo_juego = tiempo_juego;
        this.producto_preferido = producto_preferido;
        this.consumo_cliente = consumo_cliente;
        this.win_teorico = win_teorico;
        this.monto_cambios_dinero = monto_cambios_dinero;
        this.cantidad_cambios_dinero = cantidad_cambios_dinero;
        this.cantidad_comps = cantidad_comps;
        this.listaOperacionesCliente = listaOperacionesCliente;
    }
}
