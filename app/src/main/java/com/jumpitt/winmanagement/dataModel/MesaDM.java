package com.jumpitt.winmanagement.dataModel;

import com.jumpitt.winmanagement.utils.GlobalData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MesaDM implements Serializable {
    public enum OrientacionesMesa { ARRIBA, ABAJO, IZQUIERDA, DERECHA, CENTRO };
    private OrientacionesMesa orientacion_mesa;
    private String id_mesa;
    private String id_sesion;
    private String fecha_actual;
    private String clase_mesa;
    private String nombre_completo_mesa;
    private String nombre_juego;
    private String numero_mesa;
    private String bg_mesa_b64;
    private String imagen_mesa_b64;
    private int min_mesa;
    private int max_mesa;
    private int cant_asientos;
    private boolean selected;
    private List<JugadorDM> jugadores;
    private List<PosicionesJugadoresMesaDM> posiciones_jugadores;

    public MesaDM() {
        this.orientacion_mesa = OrientacionesMesa.ABAJO;
        this.id_mesa = GlobalData.EMPTY;
        this.id_sesion = GlobalData.EMPTY;
        this.clase_mesa = GlobalData.EMPTY;
        this.fecha_actual = GlobalData.EMPTY;
        this.nombre_completo_mesa = GlobalData.EMPTY;
        this.nombre_juego = GlobalData.EMPTY;
        this.numero_mesa = GlobalData.EMPTY;
        this.bg_mesa_b64 = GlobalData.EMPTY;
        this.imagen_mesa_b64 = GlobalData.EMPTY;
        this.min_mesa = 0;
        this.max_mesa = 0;
        this.selected = false;
        this.jugadores = new ArrayList<>();
        this.posiciones_jugadores = new ArrayList<>();
        this.orientacion_mesa = OrientacionesMesa.ABAJO;
    }

    public MesaDM(String id_mesa, String id_sesion, String clase_mesa, String fecha_actual, OrientacionesMesa orientacion_mesa, String nombre_completo_mesa, String bg_mesa_b64,
                  String imagen_mesa_b64, int min_mesa, int max_mesa, int cant_asientos, boolean selected, ArrayList<PosicionesJugadoresMesaDM> posiciones_jugadores) {
        this.id_mesa = id_mesa;
        this.id_sesion = id_sesion;
        this.clase_mesa = clase_mesa;
        this.fecha_actual = fecha_actual;
        this.nombre_completo_mesa = nombre_completo_mesa;
        this.cant_asientos = cant_asientos;
        this.bg_mesa_b64 = bg_mesa_b64;
        this.imagen_mesa_b64 = imagen_mesa_b64;
        this.min_mesa = min_mesa;
        this.max_mesa = max_mesa;
        this.selected = selected;
        this.jugadores = new ArrayList<>();
        this.posiciones_jugadores = posiciones_jugadores;
        this.orientacion_mesa = orientacion_mesa;

        String[] nombre_mesa_array = nombre_completo_mesa.split(" ");

        this.numero_mesa = nombre_mesa_array[nombre_mesa_array.length-1];
        this.nombre_juego = this.nombre_completo_mesa.replace(this.numero_mesa, GlobalData.EMPTY).trim();

        createArrayListJugadores();
    }

    private void createArrayListJugadores() {
        for(int i=0; i<this.cant_asientos; i++) jugadores.add(new JugadorDM());
    }

    public String getId_mesa() {
        return id_mesa;
    }

    public void setId_mesa(String id_mesa) {
        this.id_mesa = id_mesa;
    }

    public String getId_sesion() {
        return id_sesion;
    }

    public void setId_sesion(String id_sesion) {
        this.id_sesion = id_sesion;
    }

    public String getClase_mesa() {
        return clase_mesa;
    }

    public void setClase_mesa(String clase_mesa) {
        this.clase_mesa = clase_mesa;
    }

    public String getFecha_actual() {
        return fecha_actual;
    }

    public void setFecha_actual(String fecha_actual) {
        this.fecha_actual = fecha_actual;
    }

    public String getNombre_completo_mesa() {
        return nombre_completo_mesa;
    }

    public int getMin_mesa() {
        return min_mesa;
    }

    public void setMin_mesa(int min_mesa) {
        this.min_mesa = min_mesa;
    }

    public int getMax_mesa() {
        return max_mesa;
    }

    public void setMax_mesa(int max_mesa) {
        this.max_mesa = max_mesa;
    }

    public void setNombre_completo_mesa(String nombre_completo_mesa) {
        this.nombre_completo_mesa = nombre_completo_mesa;

        String[] nombre_mesa_array = nombre_completo_mesa.split(" ");

        this.numero_mesa = nombre_mesa_array[nombre_mesa_array.length-1];
        this.nombre_juego = this.nombre_completo_mesa.replace(this.numero_mesa, GlobalData.EMPTY).trim();
    }

    public int getCant_asientos() {
        return cant_asientos;
    }

    public void setCant_asientos(int cant_asientos) {
        this.cant_asientos = cant_asientos;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getNombre_juego() {
        return nombre_juego;
    }

    public void setNombre_juego(String nombre_juego) {
        this.nombre_juego = nombre_juego;
    }

    public String getNumero_mesa() {
        return numero_mesa;
    }

    public void setNumero_mesa(String numero_mesa) {
        this.numero_mesa = numero_mesa;
    }

    public String getBg_mesa_b64() {
        return bg_mesa_b64;
    }

    public void setBg_mesa_b64(String bg_mesa_b64) {
        this.bg_mesa_b64 = bg_mesa_b64;
    }

    public String getImagen_mesa_b64() {
        return imagen_mesa_b64;
    }

    public void setImagen_mesa_b64(String imagen_mesa_b64) {
        this.imagen_mesa_b64 = imagen_mesa_b64;
    }

    public List<JugadorDM> getJugadores() {
        return jugadores;
    }

    public void setJugadores(ArrayList<JugadorDM> jugadores) {
        this.jugadores = jugadores;
    }

    public void setJugador(int posicion, JugadorDM jugador) {
        this.jugadores.set(posicion, jugador);
    }

    public OrientacionesMesa getOrientacion_mesa() {
        return orientacion_mesa;
    }

    public void setOrientacion_mesa(OrientacionesMesa orientacion_mesa) {
        this.orientacion_mesa = orientacion_mesa;
    }

    public List<PosicionesJugadoresMesaDM> getPosiciones_jugadores() {
        return posiciones_jugadores;
    }

    public void setPosiciones_jugadores(ArrayList<PosicionesJugadoresMesaDM> posiciones_jugadores) {
        this.posiciones_jugadores = posiciones_jugadores;
    }
}