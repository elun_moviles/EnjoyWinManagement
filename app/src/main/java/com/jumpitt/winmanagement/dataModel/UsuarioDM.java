package com.jumpitt.winmanagement.dataModel;

import java.io.Serializable;

public class UsuarioDM implements Serializable {
    private String nombre_usuario;
    private String num_tarjeta;
    private String id_usuario;
    private String casino_id_casino;

    public UsuarioDM() {
        this.nombre_usuario = "";
        this.num_tarjeta = "";
        this.id_usuario = "";
        this.casino_id_casino = "";
    }

    public UsuarioDM(String nombre_usuario, String num_tarjeta, String id_usuario, String id_casino) {
        this.nombre_usuario = nombre_usuario;
        this.num_tarjeta = num_tarjeta;
        this.id_usuario = id_usuario;
        this.casino_id_casino = id_casino;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getNum_tarjeta() {
        return num_tarjeta;
    }

    public void setNum_tarjeta(String num_tarjeta) {
        this.num_tarjeta = num_tarjeta;
    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getCasino_id_casino() {
        return casino_id_casino;
    }

    public void setCasino_id_casino(String casino_id_casino) {
        this.casino_id_casino = casino_id_casino;
    }
}
