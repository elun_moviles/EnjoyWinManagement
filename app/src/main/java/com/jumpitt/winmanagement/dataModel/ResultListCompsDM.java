package com.jumpitt.winmanagement.dataModel;

import java.util.ArrayList;

public class ResultListCompsDM
{
	public String codigo_rvc;
	public long presupuesto_cliente;
	private ArrayList<CategoriaCompsDM> listaCategoriasComps;

	public ResultListCompsDM()
	{
		this.codigo_rvc = "";
		this.presupuesto_cliente = 0;
		this.listaCategoriasComps = new ArrayList<CategoriaCompsDM>();
	}
	
	public ResultListCompsDM(String codigo_rvc, long presupuesto_cliente, ArrayList<CategoriaCompsDM> listaCategoriaComps)
	{
		this.codigo_rvc = codigo_rvc;
		this.presupuesto_cliente = presupuesto_cliente;
		this.listaCategoriasComps = listaCategoriaComps;
		
	}
	
	public ArrayList<CategoriaCompsDM> getListaCategoriaComps()
	{
		return listaCategoriasComps;
	}

	public void setListaCategoriasComps(ArrayList<CategoriaCompsDM> listaCategoriasComps)
	{
		this.listaCategoriasComps = listaCategoriasComps;
	}
}
