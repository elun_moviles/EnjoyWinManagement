package com.jumpitt.winmanagement.dataModel;

public class CompDM {
    private String idComp;
    private String nombreComp;
    private String categoriaComp;
    private int cantDisponibleComp;
    private long precioComp;
    private boolean compDestacado;
    private boolean permiteIngredientes;
    private boolean permiteInfoExtra;
//    private ArrayList<ModificadorCompDM> listaIngredientes;
//    private ArrayList<ModificadorCompDM> listaAgregados;

    public CompDM() {
        this.idComp = "";
        this.nombreComp = "";
        this.categoriaComp = "";
        this.cantDisponibleComp = 0;
        this.precioComp = 0;
        this.compDestacado = false;
        this.permiteIngredientes = false;
        this.permiteInfoExtra = false;
//        this.listaIngredientes = new ArrayList<>();
//        this.listaAgregados = new ArrayList<>();
    }

    public CompDM(String idComp, String nombreComp, String categoriaComp, int cantDisponibleComp, long precioTotalComp,
                  boolean compDestacado, boolean permiteIngredientes, boolean permiteInfoExtra/*,
                  ArrayList<ModificadorCompDM> listaIngredientes, ArrayList<ModificadorCompDM> listaAgregados*/) {
        this.idComp = idComp;
        this.nombreComp = nombreComp;
        this.categoriaComp = categoriaComp;
        this.cantDisponibleComp = cantDisponibleComp;
        this.precioComp = precioTotalComp;
        this.compDestacado = compDestacado;
        this.permiteIngredientes = permiteIngredientes;
        this.permiteInfoExtra = permiteInfoExtra;
//        this.listaIngredientes = listaIngredientes;
//        this.listaAgregados = listaAgregados;
    }

    public String getIdComp() {
        return idComp;
    }

    public void setIdComp(String idComp) {
        this.idComp = idComp;
    }

    public String getNombreComp() {
        return nombreComp;
    }

    public void setNombreComp(String nombreComp) {
        this.nombreComp = nombreComp;
    }

    public String getCategoriaComp() {
        return categoriaComp;
    }

    public void setCategoriaComp(String categoriaComp) {
        this.categoriaComp = categoriaComp;
    }

    public int getCantDisponibleComp() {
        return cantDisponibleComp;
    }

    public void setCantDisponibleComp(int cantDisponibleComp) {
        this.cantDisponibleComp = cantDisponibleComp;
    }

    public long getPrecioTotalComp() {
        long precioFinal = precioComp;

//        for (ModificadorCompDM modificador : listaIngredientes) {
//            precioFinal += modificador.getPrecioAdicionalModificador();
//        }
//
//        for (ModificadorCompDM modificador : listaAgregados) {
//            precioFinal += modificador.getPrecioAdicionalModificador();
//        }

        return precioFinal;
    }

    public void setPrecioComp(long precioComp) {
        this.precioComp = precioComp;
    }

    public boolean isCompDestacado() {
        return compDestacado;
    }

    public void setCompDestacado(boolean compDestacado) {
        this.compDestacado = compDestacado;
    }

    public boolean isPermiteIngredientes() {
        return permiteIngredientes;
    }

    public void setPermiteIngredientes(boolean permiteIngredientes) {
        this.permiteIngredientes = permiteIngredientes;
    }

    public boolean isPermiteInfoExtra() {
        return permiteInfoExtra;
    }

    public void setPermiteInfoExtra(boolean permiteInfoExtra) {
        this.permiteInfoExtra = permiteInfoExtra;
    }

//    public ArrayList<ModificadorCompDM> getListaIngredientes() {
//        return listaIngredientes;
//    }
//
//    public void setListaIngredientes(ArrayList<ModificadorCompDM> listaIngredientes) {
//        this.listaIngredientes = listaIngredientes;
//    }
//
//    public ArrayList<ModificadorCompDM> getListaAgregados() {
//        return listaAgregados;
//    }
//
//    public void setListaAgregados(ArrayList<ModificadorCompDM> listaAgregados) {
//        this.listaAgregados = listaAgregados;
//    }
}
