package com.jumpitt.winmanagement.dataModel;

import com.jumpitt.winmanagement.dataModel.enums.PlayerTypes;
import com.jumpitt.winmanagement.utils.GlobalData;

import java.io.Serializable;

public class JugadorDM implements Serializable {
    private PlayerTypes playerType;
    private String id_jugador;
    private String id_mcc_jugador;
    private String id_tarjeta_jugador;
    private String rut_jugador;
    private String id_juego;
    private String nombre_jugador;
    private String id_categoria_jugador;
    private String categoria_jugador;
    private String timer_jugador;
    private String foto_jugador_b64;
    private String estado_jugador;
    private String mensaje_validacion;
    private int puntos_jornada;
    private int posX;
    private int posY;
    private int numero_jugador;
    private boolean isPaused;
    private boolean hasAvailableComp;

    public JugadorDM() {
        this.playerType = PlayerTypes.NULL;
        this.id_jugador = GlobalData.EMPTY;
        this.id_mcc_jugador = GlobalData.EMPTY;
        this.id_tarjeta_jugador = GlobalData.EMPTY;
        this.rut_jugador = GlobalData.EMPTY;
        this.id_juego = GlobalData.EMPTY;
        this.nombre_jugador = GlobalData.EMPTY;
        this.id_categoria_jugador = GlobalData.EMPTY;
        this.categoria_jugador = GlobalData.EMPTY;
        this.timer_jugador = GlobalData.EMPTY;
        this.foto_jugador_b64 = GlobalData.EMPTY;
        this.mensaje_validacion = GlobalData.EMPTY;
        this.estado_jugador = GlobalData.EMPTY;
        this.puntos_jornada = 0;
        this.posX = -1;
        this.posY = -1;
        this.numero_jugador = 0;
        this.isPaused = false;
        this.hasAvailableComp = false;
    }

    public JugadorDM(PlayerTypes playerType, String id_jugador, String id_mcc_jugador, String id_tarjeta_jugador, String rut_jugador, String id_juego,
                     String nombre_jugador, String id_categoria_jugador, String categoria_jugador, String timer_jugador,
                     String foto_jugador_b64, String mensaje_validacion, String estado_jugador, int puntos_jornada, int posX, int posY,
                     int numero_jugador, boolean isPaused, boolean hasAvailableComp) {
        this.playerType = playerType;
        this.id_jugador = id_jugador;
        this.id_mcc_jugador = id_mcc_jugador;
        this.id_tarjeta_jugador = id_tarjeta_jugador;
        this.rut_jugador = rut_jugador;
        this.id_juego = id_juego;
        this.nombre_jugador = nombre_jugador;
        this.id_categoria_jugador = id_categoria_jugador;
        this.categoria_jugador = categoria_jugador;
        this.timer_jugador = timer_jugador;
        this.foto_jugador_b64 = foto_jugador_b64;
        this.mensaje_validacion = mensaje_validacion;
        this.estado_jugador = estado_jugador;
        this.puntos_jornada = puntos_jornada;
        this.posX = posX;
        this.posY = posY;
        this.numero_jugador = numero_jugador;
        this.isPaused = isPaused;
        this.hasAvailableComp = hasAvailableComp;
    }

    public PlayerTypes getPlayerType() {
        return playerType;
    }

    public void setPlayerType(PlayerTypes playerType) {
        this.playerType = playerType;
    }

    public String getId_jugador() {
        return id_jugador;
    }

    public void setId_jugador(String id_jugador) {
        this.id_jugador = id_jugador;
    }

    public String getId_mcc_jugador() {
        return id_mcc_jugador;
    }

    public void setId_mcc_jugador(String id_mcc_jugador) {
        this.id_mcc_jugador = id_mcc_jugador;
    }

    public String getId_tarjeta_jugador() {
        return id_tarjeta_jugador;
    }

    public void setId_tarjeta_jugador(String id_tarjeta_jugador) {
        this.id_tarjeta_jugador = id_tarjeta_jugador;
    }

    public String getRut_jugador() {
        return rut_jugador;
    }

    public void setRut_jugador(String rut_jugador) {
        this.rut_jugador = rut_jugador;
    }

    public String getId_juego() {
        return id_juego;
    }

    public void setId_juego(String id_juego) {
        this.id_juego = id_juego;
    }

    public String getNombre_jugador() {
        return nombre_jugador;
    }

    public void setNombre_jugador(String nombre_jugador) {
        this.nombre_jugador = nombre_jugador;
    }

    public String getId_categoria_jugador() {
        return id_categoria_jugador;
    }

    public void setId_categoria_jugador(String id_categoria_jugador) {
        this.id_categoria_jugador = id_categoria_jugador;
    }

    public String getCategoria_jugador() {
        return categoria_jugador;
    }

    public void setCategoria_jugador(String categoria_jugador) {
        this.categoria_jugador = categoria_jugador;
    }

    public String getTimer_jugador() {
        return timer_jugador;
    }

    public void setTimer_jugador(String timer_jugador) {
        this.timer_jugador = timer_jugador;
    }

    public String getFoto_jugador_b64() {
        return foto_jugador_b64;
    }

    public void setFoto_jugador_b64(String foto_jugador_b64) {
        this.foto_jugador_b64 = foto_jugador_b64;
    }

    public String getEstado_jugador() {
        return estado_jugador;
    }

    public void setEstado_jugador(String estado_jugador) {
        this.estado_jugador = estado_jugador;
    }

    public String getMensaje_validacion() {
        return mensaje_validacion;
    }

    public void setMensaje_validacion(String mensaje_validacion) {
        this.mensaje_validacion = mensaje_validacion;
    }

    public int getPuntos_jornada() {
        return puntos_jornada;
    }

    public void setPuntos_jornada(int puntos_jornada) {
        this.puntos_jornada = puntos_jornada;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getNumero_jugador() {
        return numero_jugador;
    }

    public void setNumero_jugador(int numero_jugador) {
        this.numero_jugador = numero_jugador;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void setIsPaused(boolean isPaused) {
        this.isPaused = isPaused;
    }

    public boolean hasAvailableComp() {
        return hasAvailableComp;
    }

    public void setHasAvailableComp(boolean hasAvailableComp) {
        this.hasAvailableComp = hasAvailableComp;
    }
}
