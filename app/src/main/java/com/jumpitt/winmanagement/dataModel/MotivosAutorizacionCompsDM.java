package com.jumpitt.winmanagement.dataModel;

public class MotivosAutorizacionCompsDM
{
	public String id_motivo_autorizacion;
	public String descripcion_motivo_autorizacion;
	
	public MotivosAutorizacionCompsDM()
	{
		this.id_motivo_autorizacion = "";
		this.descripcion_motivo_autorizacion = "";
	}
	
	public MotivosAutorizacionCompsDM(String id_motivo_autorizacion, String descripcion_motivo_autorizacion)
	{
		this.id_motivo_autorizacion = id_motivo_autorizacion;
		this.descripcion_motivo_autorizacion = descripcion_motivo_autorizacion;
	}
}
