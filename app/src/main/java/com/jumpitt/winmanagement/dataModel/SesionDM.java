package com.jumpitt.winmanagement.dataModel;

import com.jumpitt.winmanagement.utils.GlobalData;

public class SesionDM {
    public String id_sesion;
    public String fecha_actual;

    public SesionDM() {
        this.id_sesion = GlobalData.EMPTY;
        this.fecha_actual = GlobalData.EMPTY;
    }

    public SesionDM(String id_sesion, String fecha_actual) {
        this.id_sesion = id_sesion;
        this.fecha_actual = fecha_actual;
    }
}
