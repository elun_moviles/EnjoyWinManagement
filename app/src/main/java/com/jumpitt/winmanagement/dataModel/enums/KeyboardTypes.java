package com.jumpitt.winmanagement.dataModel.enums;

public enum KeyboardTypes {
    ID,
    RUT,
    TRACKING,
    MONEY_CHANGE,
    PRIZE,
    OPERATIVE_COUNTING_ON_DEMAND,
    OPERATIVE_COUNTING
}
