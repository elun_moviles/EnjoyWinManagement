package com.jumpitt.winmanagement.dataModel;

public class ItemDetalleHistorialDM
{
	public String fecha_operacion;
	public String tipo_operacion;
	public String info_adicional_operacion;
	public long costo;
	public int numero_operacion;
	
	public ItemDetalleHistorialDM()
	{
		this.fecha_operacion = "";
		this.tipo_operacion = "";
		this.info_adicional_operacion = "";
		this.costo = 0;
		this.numero_operacion = 0;
	}
	
	public ItemDetalleHistorialDM(String fecha_operacion, String tipo_operacion, String info_adicional_operacion, 
								  long costo, int numero_operacion)
	{
		this.fecha_operacion = fecha_operacion;
		this.tipo_operacion = tipo_operacion;
		this.info_adicional_operacion = info_adicional_operacion;
		this.costo = costo;
		this.numero_operacion = numero_operacion;
	}
}
