package com.jumpitt.winmanagement.dataModel.enums;

public enum PlayersSearchCriteria {
    MCC,
    RUT,
    TAR
};