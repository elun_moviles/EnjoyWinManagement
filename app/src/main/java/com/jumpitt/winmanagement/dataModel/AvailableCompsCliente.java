package com.jumpitt.winmanagement.dataModel;

public class AvailableCompsCliente
{
	public boolean client_has_comp;
	public long presupuesto_comps;
	
	public AvailableCompsCliente()
	{
		this.client_has_comp = false;
		this.presupuesto_comps = 0;
	}
	
	public AvailableCompsCliente(boolean client_has_comp, long presupuesto_comps)
	{
		this.client_has_comp = client_has_comp;
		this.presupuesto_comps = presupuesto_comps;
	}
}
