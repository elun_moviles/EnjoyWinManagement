package com.jumpitt.winmanagement.dataModel;

import java.io.Serializable;
import java.util.Date;

public class JuegoDM implements Serializable {
	private int id_juego;
	private int cliente_id_cliente;
	private int sesion_id_sesion;
	private Date hora_inicio;
	private Date hora_termino;
	private int posicion;
	private String estado;

	public JuegoDM() {
		this.id_juego = 0;
		this.cliente_id_cliente = 0;
		this.sesion_id_sesion = 0;
		this.hora_inicio = new Date(Long.MIN_VALUE);
		this.hora_termino = new Date(Long.MIN_VALUE);
		this.posicion = 0;
		this.estado = "";
	}

	public JuegoDM(int id_juego, int cliente_id_cliente, int sesion_id_sesion, Date hora_inicio, Date hora_termino, int posicion, String estado) {
		this.id_juego = id_juego;
		this.cliente_id_cliente = cliente_id_cliente;
		this.sesion_id_sesion = sesion_id_sesion;
		this.hora_inicio = hora_inicio;
		this.hora_termino = hora_termino;
		this.posicion = posicion;
		this.estado = estado;
	}

	public int getId_juego() {
		return id_juego;
	}

	public void setId_juego(int id_juego) {
		this.id_juego = id_juego;
	}

	public int getCliente_id_cliente() {
		return cliente_id_cliente;
	}

	public void setCliente_id_cliente(int cliente_id_cliente) {
		this.cliente_id_cliente = cliente_id_cliente;
	}

	public int getSesion_id_sesion() {
		return sesion_id_sesion;
	}

	public void setSesion_id_sesion(int sesion_id_sesion) {
		this.sesion_id_sesion = sesion_id_sesion;
	}

	public Date getHora_inicio() {
		return hora_inicio;
	}

	public void setHora_inicio(Date hora_inicio) {
		this.hora_inicio = hora_inicio;
	}

	public Date getHora_termino() {
		return hora_termino;
	}

	public void setHora_termino(Date hora_termino) {
		this.hora_termino = hora_termino;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
}