package com.jumpitt.winmanagement.dataModel;

import java.util.ArrayList;

public class CategoriaCompsDM
{
	public String id_categoria;
	public String codigo_categoria;
	public String nombre_categoria;
	private ArrayList<CompDM> listaComps;
	
	public CategoriaCompsDM()
	{
		this.id_categoria = "";
		this.codigo_categoria = "";
		this.nombre_categoria = "";
		this.listaComps = new ArrayList<CompDM>();
	}

	public CategoriaCompsDM(String id_categoria, String codigo_categoria, String nombre_categoria, ArrayList<CompDM> listaComps)
	{
		this.id_categoria = id_categoria;
		this.codigo_categoria = codigo_categoria;
		this.nombre_categoria = nombre_categoria;
		this.listaComps = listaComps;
	}
	
	public ArrayList<CompDM> getListaComps()
	{
		return listaComps;
	}

	public void setListaComps(ArrayList<CompDM> listaComps)
	{
		this.listaComps = listaComps;
	}
}
