package com.jumpitt.winmanagement.dataModel.enums;

public enum PlayerTypes {
    NULL,
    EMPTY,
    REGISTERED,
    ANONYMOUS
}
