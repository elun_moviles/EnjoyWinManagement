package com.jumpitt.winmanagement.dataModel;

import com.jumpitt.winmanagement.utils.GlobalData;

public class InfoAdicionalCompsDM {
    public String id;
    public String nombre;
    public int cantidad;
    public int precio;

    public InfoAdicionalCompsDM() {
        this.id = GlobalData.EMPTY;
        this.nombre = GlobalData.EMPTY;
        this.cantidad = 0;
        this.precio = 0;
    }

    public InfoAdicionalCompsDM(String id, String nombre, int cantidad, int precio) {
        this.id = id;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precio = precio;
    }
}
