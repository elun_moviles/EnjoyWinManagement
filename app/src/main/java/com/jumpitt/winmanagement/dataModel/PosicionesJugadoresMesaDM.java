package com.jumpitt.winmanagement.dataModel;

import java.io.Serializable;

public class PosicionesJugadoresMesaDM implements Serializable {
    private int posX;
    private int posY;
    private int numero_posicion;

    public PosicionesJugadoresMesaDM() {
        this.posX = 0;
        this.posY = 0;
        this.numero_posicion = 0;
    }

    public PosicionesJugadoresMesaDM(int posX, int posY, int numero_posicion) {
        this.posX = posX;
        this.posY = posY;
        this.numero_posicion = numero_posicion;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getNumero_posicion() {
        return numero_posicion;
    }

    public void setNumero_posicion(int numero_posicion) {
        this.numero_posicion = numero_posicion;
    }
}
