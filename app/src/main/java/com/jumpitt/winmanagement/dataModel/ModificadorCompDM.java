package com.jumpitt.winmanagement.dataModel;

import com.jumpitt.winmanagement.ui.comps.TipoModificadorComps;

public class ModificadorCompDM {
    private int idModificador;
    private TipoModificadorComps tipoModificador;
    private String nombreModificador;
    private long precioAdicionalModificador;
    private boolean selected;

    public ModificadorCompDM() {
        this.idModificador = -1;
        this.tipoModificador = TipoModificadorComps.NINGUNO;
        this.nombreModificador = "";
        this.precioAdicionalModificador = 0;
        this.selected = false;
    }

    public ModificadorCompDM(int idModificador, TipoModificadorComps tipoModificador, String nombreModificador, long precioAdicionalModificador, boolean selected) {
        this.idModificador = idModificador;
        this.tipoModificador = tipoModificador;
        this.nombreModificador = nombreModificador;
        this.precioAdicionalModificador = precioAdicionalModificador;
        this.selected = selected;
    }

    public int getIdModificador() {
        return idModificador;
    }

    public void setIdModificador(int idModificador) {
        this.idModificador = idModificador;
    }

    public TipoModificadorComps getTipoModificador() {
        return tipoModificador;
    }

    public void setTipoModificador(TipoModificadorComps tipoModificador) {
        this.tipoModificador = tipoModificador;
    }

    public String getNombreModificador() {
        return nombreModificador;
    }

    public void setNombreModificador(String nombreModificador) {
        this.nombreModificador = nombreModificador;
    }

    public long getPrecioAdicionalModificador() {
        return precioAdicionalModificador;
    }

    public void setPrecioAdicionalModificador(long precioAdicionalModificador) {
        this.precioAdicionalModificador = precioAdicionalModificador;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
