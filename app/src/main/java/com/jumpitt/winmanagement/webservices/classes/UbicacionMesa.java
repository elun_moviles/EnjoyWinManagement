package com.jumpitt.winmanagement.webservices.classes;

import com.jumpitt.winmanagement.utils.GlobalData;

public class UbicacionMesa {
    public String estado;
    public String nombre_casino;
    public String nombre_salon;
    public String nombre_corral;
    public String nombre_mesa;
    public String posicion;
    public String foto;
    public String nombre_cliente;

    public UbicacionMesa() {
        this.estado = GlobalData.EMPTY;
        this.nombre_casino = GlobalData.EMPTY;
        this.nombre_salon = GlobalData.EMPTY;
        this.nombre_corral = GlobalData.EMPTY;
        this.nombre_mesa = GlobalData.EMPTY;
        this.posicion = GlobalData.EMPTY;
        this.foto = GlobalData.EMPTY;
        this.nombre_cliente = GlobalData.EMPTY;
    }

    public UbicacionMesa(String estado, String nombre_casino, String nombre_salon, String nombre_corral,
                         String nombre_mesa, String posicion, String foto, String nombre_cliente) {
        this.estado = estado;
        this.nombre_casino = nombre_casino;
        this.nombre_salon = nombre_salon;
        this.nombre_corral = nombre_corral;
        this.nombre_mesa = nombre_mesa;
        this.posicion = posicion;
        this.foto = foto;
        this.nombre_cliente = nombre_cliente;
    }
}