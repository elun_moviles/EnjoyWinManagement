package com.jumpitt.winmanagement.webservices.classes;

import com.jumpitt.winmanagement.utils.GlobalData;

public class CompAvailabilityDM {
	private String response;
	private boolean is_comp_available;

	public CompAvailabilityDM() {
		this.response = GlobalData.EMPTY;
		this.is_comp_available = false;
	}

	public CompAvailabilityDM(String response, boolean is_comp_available) {
		this.response = response;
		this.is_comp_available = is_comp_available;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public boolean is_comp_available() {
		return is_comp_available;
	}

	public void setIs_comp_available(boolean is_comp_available) {
		this.is_comp_available = is_comp_available;
	}
}