package com.jumpitt.winmanagement.webservices.classes;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class Cliente implements KvmSerializable {
	public int id_cliente;
	public int cod_cliente;
	public String rut_cliente;
	public String nombre_cliente;
	public int tiempo_relevo;
	public String estado;

	public Cliente()
	{
		this.id_cliente = 0;
		this.cod_cliente = 0;
		this.rut_cliente = "";
		this.nombre_cliente = "";
		this.tiempo_relevo = 0;
		this.estado = "";
	}

	public Cliente(int id_cliente, int cod_cliente, String rut_cliente, 
				   String nombre_cliente, int tiempo_relevo, String estado)
	{
		this.id_cliente = id_cliente;
		this.cod_cliente = cod_cliente;
		this.rut_cliente = rut_cliente;
		this.nombre_cliente = nombre_cliente;
		this.tiempo_relevo = tiempo_relevo;
		this.estado = estado;
	}

	@Override
	public Object getProperty(int arg0) {

		switch(arg0)
        {
        case 0:
            return id_cliente;
        case 1:
            return cod_cliente;
        case 2:
            return rut_cliente;
        case 3:
            return nombre_cliente;
        case 4:
            return tiempo_relevo;
        case 5:
            return estado;
        }

		return null;
	}

	@Override
	public int getPropertyCount() {
		return 6;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void getPropertyInfo(int ind, Hashtable ht, PropertyInfo info) {
		switch(ind)
        {
	        case 0:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Id cliente";
	            break;
	        case 1:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Cod Cliente";
	            break;
	        case 2:
	            info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Rut cliente";
	            break;
	        case 3:
	            info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Nombre cliente";
	            break;
	        case 4:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Tiempo relevo";
	            break;
	        case 5:
	            info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Estado cliente";
	            break;
	        default:break;
        }
	}

	@Override
	public void setProperty(int ind, Object val) {
		switch(ind)
        {
	        case 0:
	            id_cliente = Integer.parseInt(val.toString());
	            break;
	        case 1:
	            cod_cliente = Integer.parseInt(val.toString());
	            break;
	        case 2:
	            rut_cliente = val.toString();
	            break;
	        case 3:
	            nombre_cliente = val.toString();
	            break;
	        case 4:
	            tiempo_relevo = Integer.parseInt(val.toString());
	            break;
	        case 5:
	            estado = val.toString();
	            break;
	        default:
	            break;
        }
	}
}