package com.jumpitt.winmanagement.webservices.classes;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class EnjoyClient implements KvmSerializable {
	public int id_cliente;
	public int cod_cliente;
	public String rut_cliente;
	public int id_categoria;
	public String categoria_cliente;
	public String id_card_cliente;
	public String nombre_cliente;
	public String apellido_cliente;
	public int puntos_jornada;
	public byte[] foto;
	public String foto_b64;
	public String mensaje_validacion;
	

	public EnjoyClient()
	{
		this.id_cliente = 0;
		this.cod_cliente = 0;
		this.rut_cliente = "";
		this.id_categoria = 0;
		this.categoria_cliente = "";
		this.id_card_cliente = "";
		this.nombre_cliente = "";
		this.apellido_cliente = "";
		this.puntos_jornada = 0;
		this.foto = "".getBytes();
		this.foto_b64 = "";
		this.mensaje_validacion = "";
	}

	public EnjoyClient(int id_cliente, String id_card_cliente, int cod_cliente, int id_categoria, String rut_cliente, String nombre_cliente,
					   String apellido_cliente, String categoria_cliente, int puntos_jornada, String mensaje_validacion, byte[] foto)
	{
		this.id_cliente = id_cliente;
		this.cod_cliente = cod_cliente;
		this.rut_cliente = rut_cliente;
		this.id_categoria = id_categoria;
		this.categoria_cliente = categoria_cliente;
		this.id_card_cliente = id_card_cliente;
		this.nombre_cliente = nombre_cliente;
		this.apellido_cliente = apellido_cliente;
		this.puntos_jornada = puntos_jornada;
		this.foto = foto;
		this.mensaje_validacion = mensaje_validacion;
	}
	
	public EnjoyClient(int id_cliente, String id_card_cliente, int cod_cliente, int id_categoria, String rut_cliente, String nombre_cliente,
			   		   String apellido_cliente, String categoria_cliente, int puntos_jornada, String mensaje_validacion, String foto_b64)
	{
		this.id_cliente = id_cliente;
		this.cod_cliente = cod_cliente;
		this.rut_cliente = rut_cliente;
		this.id_categoria = id_categoria;
		this.categoria_cliente = categoria_cliente;
		this.id_card_cliente = id_card_cliente;
		this.nombre_cliente = nombre_cliente;
		this.apellido_cliente = apellido_cliente;
		this.puntos_jornada = puntos_jornada;
		this.foto_b64 = foto_b64;
		this.mensaje_validacion = mensaje_validacion;
	}

	@Override
	public Object getProperty(int arg0) {

		switch(arg0)
        {
        case 0:
            return id_cliente;
        case 1:
            return id_card_cliente;
        case 2:
            return cod_cliente;
        case 3:
        	return id_categoria;
        case 4:
            return rut_cliente;
        case 5:
            return nombre_cliente;
        case 6:
            return apellido_cliente;
        case 7:
            return categoria_cliente;
        case 8:
            return puntos_jornada;
        case 9:
            return mensaje_validacion;
        case 10:
	        return foto;
        case 11:
	        return foto_b64;
	    }

		return null;
	}

	@Override
	public int getPropertyCount() {
		return 12;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void getPropertyInfo(int ind, Hashtable ht, PropertyInfo info) {
		switch(ind)
        {
	        case 0:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Id cliente";
	            break;
	        case 1:
	        	info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Cod Cliente";
	            break;
	        case 2:
	        	info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Rut cliente";
	            break;
	        case 3:
	        	info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Id Categoria";
	            break;
	        case 4:
	        	info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Categoria cliente";
	            break;
	        case 5:
	        	info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Id Card Cliente";
	            break;
	        case 6:
	        	info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Nombre cliente";
	            break;
	        case 7:
	        	info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Apellido cliente";
	            break;
	        case 8:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Puntos jornada";
	            break;
	        case 9:
	        	info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Foto";
	            break;
	        case 10:
	            info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Foto Base 64";
	            break;
	        case 11:
	        	info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Mensaje validacion";
	            break;
	        default:break;
        }
	}

	@Override
	public void setProperty(int ind, Object val) {
		switch(ind)
        {
	        case 0:
	            id_cliente = Integer.parseInt(val.toString());
	            break;
	        case 1:
	        	cod_cliente = Integer.parseInt(val.toString());
	            break;
	        case 2:
	        	rut_cliente = val.toString();
	            break;
	        case 3:
	        	id_categoria = Integer.parseInt(val.toString());
	            break;
	        case 4:
	        	categoria_cliente = val.toString();
	            break;
	        case 5:
	        	id_card_cliente = val.toString();
	            break;
	        case 6:
	        	nombre_cliente = val.toString();
	            break;
	        case 7:
	        	apellido_cliente = val.toString();
	            break;
	        case 8:
	            puntos_jornada = Integer.parseInt(val.toString());
	            break;
	        case 9:
	        	foto = val.toString().getBytes();
	            break;
	        case 10:
	        	foto_b64 = val.toString();
	            break;
	        case 11:
	            mensaje_validacion = val.toString();
	            break;
	        default:
	            break;
        }
	}
}