package com.jumpitt.winmanagement.webservices.classes;

import com.jumpitt.winmanagement.webservices.classes.SignIn;
import com.jumpitt.winmanagement.webservices.classes.UbicacionMesa;

public class SignInTable {
    public SignIn signInClass;
    public UbicacionMesa tableUbicationClass;

    public SignInTable() {
        this.signInClass = null;
        this.tableUbicationClass = null;
    }

    public SignInTable(SignIn signInClass, UbicacionMesa tableUbicationClass) {
        this.signInClass = signInClass;
        this.tableUbicationClass = tableUbicationClass;
    }
}
