package com.jumpitt.winmanagement.webservices.classes;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class Mesa implements KvmSerializable {
    public int id_mesa;
    public int manos_mesa_id_mesa;
    public int corral_id_corral;
    public String nombre_mesa;
    public int cant_asientos;
    public String clase_mesa;
    public String estado;

    public Mesa() {
        this.id_mesa = 0;
        this.manos_mesa_id_mesa = 0;
        this.corral_id_corral = 0;
        this.nombre_mesa = "";
        this.cant_asientos = 0;
        this.clase_mesa = "";
        this.estado = "";
    }

    public Mesa(int id_mesa, int manos_mesa_id_mesa, int corral_id_corral, String nombre_mesa, int cant_asientos, String clase_mesa, String estado) {
        this.id_mesa = id_mesa;
        this.manos_mesa_id_mesa = manos_mesa_id_mesa;
        this.corral_id_corral = corral_id_corral;
        this.nombre_mesa = nombre_mesa;
        this.cant_asientos = cant_asientos;
        this.clase_mesa = "";
        this.estado = "";
    }

    @Override
    public Object getProperty(int arg0) {

        switch (arg0) {
            case 0:
                return id_mesa;
            case 1:
                return manos_mesa_id_mesa;
            case 2:
                return corral_id_corral;
            case 3:
                return nombre_mesa;
            case 4:
                return cant_asientos;
            case 5:
                return clase_mesa;
            case 6:
                return estado;
        }

        return null;
    }

    @Override
    public int getPropertyCount() {
        return 7;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void getPropertyInfo(int ind, Hashtable ht, PropertyInfo info) {
        switch (ind) {
            case 0:
                info.type = PropertyInfo.INTEGER_CLASS;
                info.name = "Id mesa";
                break;
            case 1:
                info.type = PropertyInfo.INTEGER_CLASS;
                info.name = "Id Corral";
                break;
            case 2:
                info.type = PropertyInfo.INTEGER_CLASS;
                info.name = "Id manos mesa";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Nombre mesa";
                break;
            case 4:
                info.type = PropertyInfo.INTEGER_CLASS;
                info.name = "Cantidad asientos";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Clase de mesa";
                break;
            case 6:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Estado";
                break;
            default:
                break;
        }
    }

    @Override
    public void setProperty(int ind, Object val) {
        switch (ind) {
            case 0:
                id_mesa = Integer.parseInt(val.toString());
                break;
            case 1:
                manos_mesa_id_mesa = Integer.parseInt(val.toString());
                break;
            case 2:
                corral_id_corral = Integer.parseInt(val.toString());
                break;
            case 3:
                nombre_mesa = val.toString();
                break;
            case 4:
                cant_asientos = Integer.parseInt(val.toString());
                break;
            case 5:
                clase_mesa = val.toString();
                break;
            case 6:
                estado = val.toString();
                break;
            default:
                break;
        }
    }
}