package com.jumpitt.winmanagement.webservices.classes;

import android.annotation.SuppressLint;

import com.jumpitt.winmanagement.utils.GlobalData;

@SuppressLint("SimpleDateFormat")
public class SignIn {
    public String id_cliente;
    public String id_juego;
    public String id_cliente_mcc;

    public SignIn() {
        this.id_cliente = GlobalData.EMPTY;
        this.id_juego = GlobalData.EMPTY;
        this.id_cliente_mcc = GlobalData.EMPTY;
    }

    public SignIn(String id_cliente, String id_juego, String id_cliente_mcc) {
        this.id_cliente = id_cliente;
        this.id_juego = id_juego;
        this.id_cliente_mcc = id_cliente_mcc;
    }
}