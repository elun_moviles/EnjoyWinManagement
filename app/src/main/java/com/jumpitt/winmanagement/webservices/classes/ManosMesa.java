package com.jumpitt.winmanagement.webservices.classes;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class ManosMesa implements KvmSerializable {
	public int id_mesa;
	public String tipo_juego;
	public int pases_hora;

	public ManosMesa()
	{
		this.id_mesa = 0;
		this.tipo_juego = "";
		this.pases_hora = 0;
	}

	public ManosMesa(int id_mesa, String tipo_juego, int pases_hora)
	{
		this.id_mesa = id_mesa;
		this.tipo_juego = tipo_juego;
		this.pases_hora = pases_hora;
	}

	@Override
	public Object getProperty(int arg0) {

		switch(arg0)
        {
        case 0:
            return id_mesa;
        case 1:
            return tipo_juego;
        case 2:
            return pases_hora;
        }

		return null;
	}

	@Override
	public int getPropertyCount() {
		return 3;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void getPropertyInfo(int ind, Hashtable ht, PropertyInfo info) {
		switch(ind)
        {
	        case 0:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Id mesa";
	            break;
	        case 1:
	            info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Tipo juego";
	            break;
	        case 2:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Pases hora";
	            break;
	        default:break;
        }
	}

	@Override
	public void setProperty(int ind, Object val) {
		switch(ind)
        {
	        case 0:
	            id_mesa = Integer.parseInt(val.toString());
	            break;
	        case 1:
	            tipo_juego = val.toString();
	            break;
	        case 2:
	            pases_hora = Integer.parseInt(val.toString());
	            break;
	        default:
	            break;
        }
	}
}