package com.jumpitt.winmanagement.webservices.classes;

import android.annotation.SuppressLint;

import com.jumpitt.winmanagement.utils.GlobalData;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

@SuppressLint("SimpleDateFormat")
public class ResultInsertCompsOrder implements KvmSerializable {
    public String resultado;
    public String mensaje_resultado;

    public ResultInsertCompsOrder() {
        this.resultado = GlobalData.EMPTY;
        this.mensaje_resultado = GlobalData.EMPTY;
    }

    public ResultInsertCompsOrder(String resultado, String mensaje_resultado) {
        this.resultado = resultado;
        this.mensaje_resultado = mensaje_resultado;
    }

    @Override
    public Object getProperty(int arg0) {
        switch (arg0) {
            case 0:
                return resultado;
            case 1:
                return mensaje_resultado;
        }

        return null;
    }

    @Override
    public int getPropertyCount() {
        return 2;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void getPropertyInfo(int ind, Hashtable ht, PropertyInfo info) {
        switch (ind) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Resultado";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Mensaje Resultado";
                break;
            default:
                break;
        }
    }

    @Override
    public void setProperty(int ind, Object val) {
        switch (ind) {
            case 0:
                resultado = val.toString();
                break;
            case 1:
                mensaje_resultado = val.toString();
                break;
            default:
                break;
        }
    }
}