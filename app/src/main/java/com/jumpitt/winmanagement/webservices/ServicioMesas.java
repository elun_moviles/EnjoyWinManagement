package com.jumpitt.winmanagement.webservices;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.jumpitt.winmanagement.dataModel.CategoriaCompsDM;
import com.jumpitt.winmanagement.dataModel.CompDM;
import com.jumpitt.winmanagement.dataModel.HistorialClienteDM;
import com.jumpitt.winmanagement.dataModel.ItemDetalleHistorialDM;
import com.jumpitt.winmanagement.dataModel.JuegoDM;
import com.jumpitt.winmanagement.dataModel.JugadorDM;
import com.jumpitt.winmanagement.dataModel.MesaDM;
import com.jumpitt.winmanagement.dataModel.ModificadorCompDM;
import com.jumpitt.winmanagement.dataModel.MotivosAutorizacionCompsDM;
import com.jumpitt.winmanagement.dataModel.ParametroDM;
import com.jumpitt.winmanagement.dataModel.PosicionesJugadoresMesaDM;
import com.jumpitt.winmanagement.dataModel.ResultCompsDM;
import com.jumpitt.winmanagement.dataModel.ResultListCompsDM;
import com.jumpitt.winmanagement.dataModel.SesionDM;
import com.jumpitt.winmanagement.dataModel.TipoJuegoDM;
import com.jumpitt.winmanagement.dataModel.UsuarioDM;
import com.jumpitt.winmanagement.databaseConnection.ConnectionServer;
import com.jumpitt.winmanagement.ui.comps.TipoModificadorComps;
import com.jumpitt.winmanagement.utils.GlobalData;
import com.jumpitt.winmanagement.utils.Utils;
import com.jumpitt.winmanagement.webservices.classes.Cliente;
import com.jumpitt.winmanagement.webservices.classes.Comp;
import com.jumpitt.winmanagement.webservices.classes.CompAvailabilityDM;
import com.jumpitt.winmanagement.webservices.classes.ManosMesa;
import com.jumpitt.winmanagement.webservices.classes.ResultInsertCompsOrder;
import com.jumpitt.winmanagement.webservices.classes.SignIn;
import com.jumpitt.winmanagement.webservices.classes.SignInTable;
import com.jumpitt.winmanagement.webservices.classes.UbicacionMesa;
import com.orhanobut.logger.Logger;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

@SuppressWarnings("ALL")
@SuppressLint("SimpleDateFormat")
public class ServicioMesas {
    // checkIfTabletIsActive
    public static String checkIfTabletIsActive(String macAddress) {
        checkIfTabletIsActive cta = new checkIfTabletIsActive();
        String result = GlobalData.EMPTY;

        try {
            result = cta.execute(macAddress).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    // getSession
    public static String getSession(String id_mesa, String hora_inicio) {
        getSession cs = new getSession();
        String result = GlobalData.EMPTY;

        try {
            result = cs.execute(id_mesa, hora_inicio).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return result;
    }

    // createGame
    public static String createGame(String id_cliente, String id_sesion, String posicion, String hora_inicio) {
        createGame cg = new createGame();
        String result = GlobalData.EMPTY;

        try {
            result = cg.execute(id_cliente, id_sesion, posicion, hora_inicio).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    // getGame
    public static JuegoDM getGame(String id_sesion, String hora_inicio) {
        getGame cg = new getGame();
        JuegoDM result = null;

        try {
            result = cg.execute(id_sesion, hora_inicio).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    // signInAnonCliente
    public static String signInAnonCliente(String id_sesion, String hora_inicio, String posicion,
                                           String tipo_mesa, String id_casino) {
        signInAnonCliente1 siac = new signInAnonCliente1();
        String result = GlobalData.EMPTY;

        try {
            result = siac.execute(id_sesion, hora_inicio, posicion, tipo_mesa, id_casino).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    // getPuntosTracking
    public static String getPuntosTracking(String id_juego, float tiempo_juego, String apuesta) {
        getPuntosTracking gpt = new getPuntosTracking();
        String result = GlobalData.EMPTY;

        try {
            result = gpt.execute(id_juego, String.valueOf(tiempo_juego), apuesta).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    // updateEnjoyPoints
    public static String updateEnjoyPoints(String id_unidad, String id_cliente, String codigo_mesa, String puntos, String token) {
        updateEnjoyPoints uep = new updateEnjoyPoints();
        String result = GlobalData.EMPTY;

        try {
            result = uep.execute(id_unidad, id_cliente, codigo_mesa, puntos, token).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    // getParamsFromDB
    public static ArrayList<ParametroDM> getParamsFromDB(String id_unidad) {
        GetParamsAsyncTask gmm = new GetParamsAsyncTask();
        ArrayList<ParametroDM> result = new ArrayList<>();

        try {
            result = gmm.execute(id_unidad).get();
        }
        catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    // getManosMesa
    public static String getManosMesa(String tipo_juego) {
        getManosMesa gmm = new getManosMesa();
        String result = GlobalData.EMPTY;

        try {
            result = gmm.execute(tipo_juego).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    // getCompCategory
    public static String getCompCategory(String tipo_comp) {
        getCompCategory gcc = new getCompCategory();
        String result = GlobalData.EMPTY;

        try {
            result = gcc.execute(tipo_comp).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    //******************************************************************************************//

    // checkIfTabletIsActive
    private static class checkIfTabletIsActive extends AsyncTask<String, Void, String> {
        private ArrayList<String> datos = new ArrayList<String>();
        private String METHOD_NAME = "getUsuario";
        private String PROPERTY_NAME = "Dispositivo";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_dispositivo", params[0]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null || resSoap.getPropertyCount() < 1) return datos.toString();

                String tablet_activa = resSoap.getProperty("id_dispositivo").toString();

                datos.add(tablet_activa);

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return datos.toString();
        }
    }

    public static class GetTableImageAndBgAsyncTask extends AsyncTask<String, Void, MesaDM> {
        private MesaDM mesa = new MesaDM();
        private String METHOD_NAME = "getImagenesMesa";
        private String PROPERTY_NAME = "MesaDM";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public GetTableImageAndBgAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected MesaDM doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_mesa", params[0]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null || resSoap.getPropertyCount() < 3) return null;

                mesa.setImagen_mesa_b64(resSoap.getPropertyAsString("imagen_mesa"));
//                mesa.setBg_mesa_b64(resSoap.getPropertyAsString(""));
                mesa.setBg_mesa_b64(GlobalData.EMPTY);
            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                return null;
            }

            return mesa;
        }

        @Override
        protected void onPostExecute(MesaDM result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // TODO: getUsuario
    public static class GetUsuarioAsyncTask extends AsyncTask<String, Void, UsuarioDM> {
        private UsuarioDM user = new UsuarioDM();
        private String METHOD_NAME = "getUsuario";
        private String PROPERTY_NAME = "UsuarioDM";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public GetUsuarioAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected UsuarioDM doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_casino", params[0]);
            request.addProperty("id_card", params[1]);
            request.addProperty("nombre_rol", params[2]);


            Log.i("informacion reuqes params",request.toString());

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                Log.i("resSoap ",resSoap.toString());

                if (resSoap == null || resSoap.getPropertyCount() < 3) return null;

                user.setId_usuario(resSoap.getPropertyAsString("id_usuario"));
                user.setCasino_id_casino(resSoap.getPropertyAsString("casino_id_casino"));
                user.setNombre_usuario(resSoap.getPropertyAsString("nombre_usuario"));
                user.setNum_tarjeta(params[0]);
            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                return null;
            }

            return user;
        }

        @Override
        protected void onPostExecute(UsuarioDM result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // TODO: createSession
    public static class CreateSessionAsyncTask extends AsyncTask<String, Void, SesionDM> {
        private SesionDM infoSesion = new SesionDM();
        private String METHOD_NAME = "createSession";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public CreateSessionAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected SesionDM doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_jefe_mesa", params[0]);
            request.addProperty("id_croupier", params[1]);
            request.addProperty("id_mesa", params[2]);
            request.addProperty("id_dispositivo", params[3]);

//            request.addProperty("id_dispositivo", "TABLET-TEST");
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null) return null;

                infoSesion.id_sesion = resSoap.getPropertyAsString("id_sesion");
                infoSesion.fecha_actual = resSoap.getPropertyAsString("fecha_actual").replace("T", " ");
            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                infoSesion = null;
            }

            return infoSesion;
        }

        @Override
        protected void onPostExecute(SesionDM result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // getSession
    private static class getSession extends AsyncTask<String, Void, String> {
        private ArrayList<String> datos = new ArrayList<String>();
        private String METHOD_NAME = "getSession";
        private String PROPERTY_NAME = "Sesion";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("mesa_id_mesa", params[0]);
            request.addProperty("hora_inicio", params[1]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                SesionDM ses = new SesionDM();
                ses.id_sesion = resSoap.getPropertyAsString("id_sesion");

                datos.add(String.valueOf(ses.id_sesion));

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return datos.toString();
        }
    }

    // createGame
    private static class createGame extends AsyncTask<String, Void, String> {

        private String METHOD_NAME = "createGame";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private String res = GlobalData.EMPTY;

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_cliente", params[0]);
            request.addProperty("id_sesion", params[1]);
            request.addProperty("posicion", params[2]);
            request.addProperty("hora_inicio", params[3]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return res;

                res = resSoap.toString();
            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return res;
        }
    }

    // TODO: finishGame
    public static class FinishGameAsyncTask extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "finishGame";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public FinishGameAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_sesion", params[0]);
            request.addProperty("id_cliente", params[1]);
            request.addProperty("hora_termino", params[2]);
            request.addProperty("tiempo", params[3]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(
                    ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if(resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // getGame
    private static class getGame extends AsyncTask<String, Void, JuegoDM> {
        private JuegoDM juego = new JuegoDM();
        private String METHOD_NAME = "getGame";
        private String PROPERTY_NAME = "Juego";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;

        @Override
        protected JuegoDM doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_sesion", params[0]);
            request.addProperty("hora_inicio", params[1]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null) return null;

                juego.setId_juego(Integer.parseInt(resSoap.getPropertyAsString(0)));

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return juego;
        }
    }

    // signInAnonCliente1
    private static class signInAnonCliente1 extends AsyncTask<String, Void, String> {

        private String METHOD_NAME = "signInAnonCliente1";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private String res = GlobalData.EMPTY;

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_sesion", params[0]);
            request.addProperty("hora_inicio", params[1]);
            request.addProperty("posicion", params[2]);
            request.addProperty("tipo_mesa", params[3]);
            request.addProperty("id_casino", params[4]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(
                    ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();
                res = resSoap.toString();

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return res;
        }
    }

    // TODO: signInMesaCliente
    public static class SignInTableClientAsyncTask extends AsyncTask<String, Void, SignInTable> {
        private SignInTable signInTable = new SignInTable();
        private SignIn signIn = new SignIn();
        private UbicacionMesa ubicacionMesa = new UbicacionMesa();
        private String METHOD_NAME = "signInMesaCliente";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public SignInTableClientAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected SignInTable doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_casino", params[0]);
            request.addProperty("id_sesion", params[1]);
            request.addProperty("rut_cliente", params[2]);
            request.addProperty("nombre_cliente", params[3]);
            request.addProperty("posicion", params[4]);
            request.addProperty("categoria_cliente", params[5]);
            request.addProperty("puntos_jornada", params[6]);
            request.addProperty("id_cliente_mcc", params[7]);
            request.addProperty("tipo_cliente", params[8]);

            Logger.d("id_casino= " + params[0] + "\n" +
                    "id_sesion= " + params[1] + "\n" +
                    "rut_cliente= " + params[2] + "\n" +
                    "nombre_cliente= " + params[3] + "\n" +
                    "posicion= " + params[4] + "\n" +
                    "categoria_cliente= " + params[5] + "\n" +
                    "puntos_jornada= " + params[6] + "\n" +
                    "id_cliente_mcc= " + params[7] + "\n" +
                    "tipo_cliente= " + params[8]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if(resSoap == null) return null;

                boolean clienteOcupado = Boolean.parseBoolean(resSoap.getPropertyAsString("cliente_ocupado"));

                SoapObject signInObject = null;
                SoapObject tableUbicationObject = null;

                if(clienteOcupado) {
                    tableUbicationObject = (SoapObject) resSoap.getProperty("ubicacion");
                }
                else {
                    signInObject = (SoapObject) resSoap.getProperty("juego");
                }

                if(signInObject == null && tableUbicationObject == null) return null;

                if(signInObject != null) {
                    signIn.id_cliente = signInObject.getPropertyAsString("cliente_id_cliente");
                    signIn.id_cliente_mcc = params[7];
                    signIn.id_juego = signInObject.getPropertyAsString("id_juego");

                    ubicacionMesa = null;
                }
                else if(tableUbicationObject != null) {
                    ubicacionMesa.nombre_casino = tableUbicationObject.getPropertyAsString("nom_casino");
                    ubicacionMesa.nombre_salon = tableUbicationObject.getPropertyAsString("nombre_salon");
                    ubicacionMesa.nombre_corral = tableUbicationObject.getPropertyAsString("nombre_corral");
                    ubicacionMesa.nombre_mesa = tableUbicationObject.getPropertyAsString("nombre_mesa");

                    int pos = Integer.parseInt(tableUbicationObject.getPropertyAsString("posicion"));

                    ubicacionMesa.posicion = (pos < 10) ? "0" + pos : String.valueOf(pos);
                    ubicacionMesa.nombre_cliente = tableUbicationObject.getPropertyAsString("nombre_cliente");
//                    ubicacionMesa.foto = tableUbicationObject.getPropertyAsString("foto");

                    signIn = null;
                }
                else {
                    signIn = null;
                    ubicacionMesa = null;
                }

                signInTable.signInClass = signIn;
                signInTable.tableUbicationClass = ubicacionMesa;
            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                return null;
            }

            return signInTable;
        }

        @Override
        protected void onPostExecute(SignInTable result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // TODO: signInCliente
    public static class SignInClientAsyncTask extends AsyncTask<String, Void, SignIn> {
        private SignIn signIn = new SignIn();
        private String METHOD_NAME = "signInCliente";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public SignInClientAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected SignIn doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_sesion", params[0]);
            request.addProperty("rut_cliente", params[1]);
            request.addProperty("nombre_cliente", params[2]);
            request.addProperty("posicion", params[3]);
            request.addProperty("categoria_cliente", params[4]);
            request.addProperty("puntos_jornada", params[5]);
            request.addProperty("id_cliente_mcc", params[6]);
            request.addProperty("tipo_cliente", params[7]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null) return null;

                signIn.id_cliente = resSoap.getPropertyAsString("cliente_id_cliente");
                signIn.id_cliente_mcc = params[6];
                signIn.id_juego = resSoap.getPropertyAsString("id_juego");

                Logger.d("id_cliente= "+signIn.id_cliente+"\n"+
                        "id_cliente_mcc= "+signIn.id_cliente_mcc+"\n"+
                        "id_juego= "+signIn.id_juego);
            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                return null;
            }

            return signIn;
        }

        @Override
        protected void onPostExecute(SignIn result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // signInCardCliente
    private static class getPuntosTracking extends AsyncTask<String, Void, String> {

        private String METHOD_NAME = "getPuntosTracking";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private String res = GlobalData.EMPTY;

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_juego", params[0]);
            request.addProperty("tiempo_juego", params[1]);
            request.addProperty("apuesta", params[2]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();
                res = resSoap.toString();

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return res;
        }
    }

    // getCliente
    private static class getCliente extends AsyncTask<String, Void, String> {
        private Cliente[] listaCliente;
        private ArrayList<String> datos = new ArrayList<String>();
        private String METHOD_NAME = "getCliente";
        private String PROPERTY_NAME = "Cliente";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("nombre_cliente", params[0]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                listaCliente = new Cliente[resSoap.getPropertyCount()];

                for (int i = 0; i < listaCliente.length; i++) {
                    SoapObject ic = (SoapObject) resSoap.getProperty(i);

                    Cliente cli = new Cliente();
                    cli.id_cliente = Integer.parseInt(ic.getProperty(0).toString());

                    listaCliente[i] = cli;
                    datos.add("" + listaCliente[i].id_cliente);
                }

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return datos.toString();
        }
    }

    // TODO: getEnjoyClient
    public static class GetEnjoyClientAsyncTask extends AsyncTask<String, Void, JugadorDM> {
        JugadorDM player = new JugadorDM();
        private String METHOD_NAME = "getCliente";
        private String PROPERTY_NAME = "EnjoyClient";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public GetEnjoyClientAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected JugadorDM doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_unidad", params[0]);
            request.addProperty("parametro_busqueda", params[1]);
            request.addProperty("tipo_busqueda", params[2]);
            request.addProperty("token", params[3]);

            Logger.d("id_unidad= " + params[0] + "\n" +
                    "parametro_busqueda= " + params[1] + "\n" +
                    "tipo_busqueda= " + params[2] + "\n" +
                    "token= " + params[3]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws_enjoy);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null) {
                    player.setMensaje_validacion(GlobalData.ERROR_GETTING_CLIENT_INFO);

                    return player;
                }

                String mensajeValidacion = resSoap.getProperty("Mensaje_validacion").toString();

                if(mensajeValidacion.isEmpty()) {
                    mensajeValidacion = GlobalData.ERROR_GETTING_CLIENT_INFO;
                }
                else if (mensajeValidacion.equals("OK")) {
                    player.setId_jugador(resSoap.getPropertyAsString("Id_mcc"));
                    player.setId_mcc_jugador(resSoap.getPropertyAsString("Id_mcc"));
                    player.setRut_jugador(resSoap.getPropertyAsString("Doc_identificacion"));
                    player.setId_categoria_jugador(resSoap.getPropertyAsString("Id_categoria"));
                    player.setCategoria_jugador(resSoap.getPropertyAsString("Categoria"));
                    player.setId_tarjeta_jugador("1234567890");
                    player.setNombre_jugador(Utils.getFormattedClientName(resSoap.getPropertyAsString("Nombre") + " " + resSoap.getPropertyAsString("Apellido")));
                    player.setPuntos_jornada(Integer.parseInt(resSoap.getPropertyAsString("Puntos_jornada")));

                    String fotob64 = resSoap.getPropertyAsString("Fotografia_base64");

                    if(fotob64 != null && !fotob64.equals("anyType{}") && !fotob64.isEmpty()) {
                        player.setFoto_jugador_b64(fotob64);
                    }
                    else {
                        player.setFoto_jugador_b64(GlobalData.EMPTY);
                    }

                    Logger.d("Id= "+resSoap.getPropertyAsString("Id_mcc")+"\n"+
                            "Id_mcc= "+resSoap.getPropertyAsString("Id_mcc")+"\n"+
                            "Doc_identificacion= "+resSoap.getPropertyAsString("Doc_identificacion")+"\n"+
                            "Id_categoria= "+resSoap.getPropertyAsString("Id_categoria")+"\n"+
                            "Categoria= "+resSoap.getPropertyAsString("Categoria")+"\n"+
                            "Puntos_jornada= "+resSoap.getPropertyAsString("Puntos_jornada")+"\n"+
                            "Fotografia_base64= "+fotob64);
                }

                player.setMensaje_validacion(mensajeValidacion.replace("ERROR;", GlobalData.EMPTY));
            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                player.setMensaje_validacion(GlobalData.ERROR_GETTING_CLIENT_INFO);
            }

            return player;
        }

        @Override
        protected void onPostExecute(JugadorDM result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // insertTracking
    public static class InsertTrackingAsyncTask extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "insertTracking";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public InsertTrackingAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_juego", params[0]);
            request.addProperty("tiempo_juego", params[1]);
            request.addProperty("demora", params[2]);
            request.addProperty("apuesta_promedio", params[3]);
            request.addProperty("token", params[4]);

            Logger.d("id_juego= "+params[0]+
                     "\ntiempo_juego= "+params[1]+
                     "\ndemora= "+params[2]+
                     "\napuesta_promedio= "+params[3]+
                     "\ntoken= "+params[4]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // insertCambioDinero
    public static class InsertMoneyChangeAsyncTask extends AsyncTask<String, Void, Integer> {

        private String METHOD_NAME = "insertCambioDinero";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public InsertMoneyChangeAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_juego", params[0]);
            request.addProperty("monto", params[1]);
            request.addProperty("fecha_registro", params[2]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // updateEnjoyPoints
    private static class updateEnjoyPoints extends AsyncTask<String, Void, String> {

        private String METHOD_NAME = "registrarTrackingCliente";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private String res = GlobalData.EMPTY;

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_unidad", params[0]);
            request.addProperty("id_cliente", params[1]);
            request.addProperty("codigo_mesa", params[2]);
            request.addProperty("puntos", params[3]);
            request.addProperty("token", params[4]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws_enjoy);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();
                res = resSoap.toString();

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return res;
        }
    }

    // getManosMesa
    private static class getManosMesa extends AsyncTask<String, Void, String> {

        private ManosMesa[] listaManosMesa;
        private ArrayList<String> datos = new ArrayList<String>();
        private String METHOD_NAME = "getManosMesa";
        private String PROPERTY_NAME = "ManosMesa";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("tipo_juego", params[0]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(
                    ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                listaManosMesa = new ManosMesa[resSoap.getPropertyCount()];

                for (int i = 0; i < listaManosMesa.length; i++) {
                    SoapObject ic = (SoapObject) resSoap.getProperty(i);

                    ManosMesa mm = new ManosMesa();
                    mm.pases_hora = Integer.parseInt(ic.getProperty(1).toString());

                    listaManosMesa[i] = mm;
                    datos.add("" + listaManosMesa[i].pases_hora);
                }

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return datos.toString();
        }
    }

    // getCompCategory
    private static class getCompCategory extends AsyncTask<String, Void, String> {
        private Comp[] listaComp;
        private ArrayList<String> datos = new ArrayList<String>();
        private String METHOD_NAME = "getCompCategory";
        private String PROPERTY_NAME = "Comp";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("tipo_comp", params[0]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                listaComp = new Comp[resSoap.getPropertyCount()];

                for (int i = 0; i < listaComp.length; i++) {
                    SoapObject ic = (SoapObject) resSoap.getProperty(i);

                    Comp cp = new Comp();
                    cp.nom_comp = ic.getProperty("nom_comp").toString();

                    listaComp[i] = cp;
                    datos.add(listaComp[i].nom_comp);
                }

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return datos.toString();
        }
    }

    // getComps
    public static class GetCompsAsyncTask extends AsyncTask<String, Void, ResultCompsDM> {
        private ResultCompsDM resultComps = new ResultCompsDM();
        private ArrayList<CategoriaCompsDM> listaCategoriasComps = new ArrayList<CategoriaCompsDM>();
        private String METHOD_NAME = "getProductosComp";
        private String PROPERTY_NAME = "Comps";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public GetCompsAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected ResultCompsDM doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_casino", params[0]);
            request.addProperty("id_mesa", params[1]);
            request.addProperty("id_cliente", params[2]);
            request.addProperty("id_categoria", params[3]);
            request.addProperty("token", params[4]);

            Logger.d("id_casino= "+params[0]+"\n"+
                    "id_mesa= "+params[1]+"\n"+
                    "id_cliente= "+params[2]+"\n"+
                    "id_categoria= "+params[3]+"\n"+
                    "token= "+params[4]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if(resSoap == null || resSoap.getPropertyCount() < 3) return null;

                SoapObject resultListCompsObject = (SoapObject) resSoap.getProperty("productos");
                SoapObject ingredientsCompsObject = (SoapObject) resSoap.getProperty("ingredientes");
                SoapObject additionalInfoCompsObject = (SoapObject) resSoap.getProperty("info_extra");

                ResultListCompsDM resultListComps = new ResultListCompsDM();
                ArrayList<ModificadorCompDM> ingredientsList = new ArrayList<>();
                ArrayList<ModificadorCompDM> additionalInfoList = new ArrayList<>();

//                String ok = resultListCompsObject.getPropertyAsString("Resultado");
//
//                Logger.e("resultListCompsObject.hasProperty(\"Resultado\")= "+resultListCompsObject.hasProperty("Resultado"));
//                Logger.e("ok= "+"\""+ok+"\"");
//                Logger.e("resultListCompsObject.hasProperty(\"PresupuestoCliente\")= "+resultListCompsObject.hasProperty("PresupuestoCliente"));
//                Logger.e("resultListCompsObject.hasProperty(\"Categorias\")= "+resultListCompsObject.hasProperty("Categorias"));

                // PRODUCTOS
//                boolean resultadoExiste = resultListCompsObject.hasProperty("Resultado");
//                boolean resultadoOk = /*(!resultadoExiste) ? false : */ok.equals("OK");
//                boolean presupuestoExiste = resultListCompsObject.hasProperty("PresupuestoCliente");
//                boolean categoriasExiste = resultListCompsObject.hasProperty("Categorias");

//                if(/*!resultadoExiste ||*/
//                    !resultadoOk ||
//                    !presupuestoExiste ||
//                    !categoriasExiste) return null;

                resultListComps.presupuesto_cliente = Long.parseLong(resultListCompsObject.getPropertyAsString("PresupuestoCliente"));
                resultListComps.codigo_rvc = resultListCompsObject.getPropertyAsString("CodigoRvc");

                SoapObject listaCategorias = (SoapObject) resultListCompsObject.getProperty("Categorias");

                for (int i = 0; i < listaCategorias.getPropertyCount(); i++) {
                    SoapObject categoriaComp = (SoapObject) listaCategorias.getProperty(i);

                    CategoriaCompsDM categoria = new CategoriaCompsDM();

                    categoria.id_categoria = categoriaComp.getPropertyAsString("Id");
                    categoria.codigo_categoria = categoriaComp.getPropertyAsString("CodigoAplicacion");
                    categoria.nombre_categoria = categoriaComp.getPropertyAsString("Nombre");

                    ArrayList<CompDM> listaComps = new ArrayList<CompDM>();

                    SoapObject listaProductos = null;

                    if (categoriaComp.hasProperty("Productos")) {
                        listaProductos = (SoapObject) categoriaComp.getProperty("Productos");
                    }

                    for (int k = 0; listaProductos != null && k < listaProductos.getPropertyCount(); k++) {
                        SoapObject producto = (SoapObject) listaProductos.getProperty(k);

                        CompDM comp = new CompDM();

                        comp.setIdComp(producto.getPropertyAsString("Id"));
                        comp.setNombreComp(producto.getPropertyAsString("Nombre"));
                        comp.setPrecioComp(Long.parseLong(Utils.formatDecimal(Double.parseDouble(producto.getPropertyAsString("Precio")), 0)));
                        comp.setPermiteIngredientes(Boolean.parseBoolean(producto.getPropertyAsString("PermiteIngredientes")));
                        comp.setPermiteInfoExtra(Boolean.parseBoolean(producto.getPropertyAsString("PermiteInfoExtra")));

                        listaComps.add(comp);
                    }

                    categoria.setListaComps(listaComps);

                    listaCategoriasComps.add(categoria);
                }

                resultListComps.setListaCategoriasComps(listaCategoriasComps);

                // INGREDIENTES
//                if(!ingredientsCompsObject.getPropertyAsString("Resultado").equals("OK")) return null;

                SoapObject listaIngredientes = (SoapObject) ingredientsCompsObject.getProperty("Productos");

                for (int i = 0; listaIngredientes != null && i < listaIngredientes.getPropertyCount(); i++) {
                    SoapObject ingredienteComp = (SoapObject) listaIngredientes.getProperty(i);

//                    InfoAdicionalCompsDM ingrediente = new InfoAdicionalCompsDM();
                    ModificadorCompDM ingrediente = new ModificadorCompDM();

                    ingrediente.setIdModificador(Integer.parseInt(ingredienteComp.getPropertyAsString("Id")));
                    ingrediente.setNombreModificador(ingredienteComp.getPropertyAsString("Nombre"));
                    ingrediente.setPrecioAdicionalModificador((long) Double.parseDouble(ingredienteComp.getPropertyAsString("Precio")));
                    ingrediente.setTipoModificador(TipoModificadorComps.INGREDIENTE);
//                    ingrediente.cantidad = 1;

                    ingredientsList.add(ingrediente);
                }

                // INFO. EXTRA
//                if(!additionalInfoCompsObject.getPropertyAsString("Resultado").equals("OK")) return null;

                SoapObject listaInfoExtra = (SoapObject) additionalInfoCompsObject.getProperty("Productos");

                for (int i = 0; listaInfoExtra != null && i < listaInfoExtra.getPropertyCount(); i++) {
                    SoapObject infoExtraComp = (SoapObject) listaInfoExtra.getProperty(i);

//                    InfoAdicionalCompsDM infoExtra = new InfoAdicionalCompsDM();
                    ModificadorCompDM infoExtra = new ModificadorCompDM();

                    infoExtra.setIdModificador(Integer.parseInt(infoExtraComp.getPropertyAsString("Id")));
                    infoExtra.setNombreModificador(infoExtraComp.getPropertyAsString("Nombre"));
                    infoExtra.setPrecioAdicionalModificador((long) Double.parseDouble(infoExtraComp.getPropertyAsString("Precio")));
                    infoExtra.setTipoModificador(TipoModificadorComps.INFO_EXTRA);
//                    infoExtra.cantidad = 1;

                    additionalInfoList.add(infoExtra);
                }

                resultComps.resultListComps = resultListComps;
                resultComps.ingredientsComps = ingredientsList;
                resultComps.additionalInfoComps = additionalInfoList;

//                String txt = "";
//
//                for(ModificadorCompDM modificadorCompDM : resultComps.ingredientsComps) {
//                    txt += modificadorCompDM.getNombreModificador() + "\n";
//                }
//
//                Logger.d("Lista de Ingredientes:\n\n"+txt);
//
//                txt = "";
//
//                for(ModificadorCompDM modificadorCompDM : resultComps.additionalInfoComps) {
//                    txt += modificadorCompDM.getNombreModificador() + "\n";
//                }
//
//                Logger.d("Lista de Info. Extra:\n\n"+txt);
            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                exception.printStackTrace();

                return null;
            }

            return resultComps;
        }

        @Override
        protected void onPostExecute(ResultCompsDM result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // TODO: getLimiteComps
    private static class GetParamsAsyncTask extends AsyncTask<String, Void, ArrayList<ParametroDM>> {
        private ArrayList<ParametroDM> listaParametros = new ArrayList<ParametroDM>();
        private String METHOD_NAME = "getParametrosMesas";
        private String PROPERTY_NAME = "ParametrosFunciones";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;

        @Override
        protected ArrayList<ParametroDM> doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            request.addProperty("id_casino", params[0]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null) return listaParametros;

                for (int i = 0; i < resSoap.getPropertyCount(); i++) {
                    SoapObject parametro = (SoapObject) resSoap.getProperty(i);

                    ParametroDM param = new ParametroDM();

                    param.setIdParametro(parametro.getProperty("id_parametro").toString());
                    param.setNombreParametro(parametro.getProperty("nombre_parametro").toString());
                    param.setValorParametro(parametro.getProperty("valor").toString());

                    listaParametros.add(param);
                }
            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return listaParametros;
        }
    }

    // insertPedComp
    public static class InsertCompsOrderAsyncTask extends AsyncTask<String, Void, ResultInsertCompsOrder> {
        private ResultInsertCompsOrder resultInsertCompsOrder = new ResultInsertCompsOrder();
        private String METHOD_NAME = "setRegistrarPedido";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public InsertCompsOrderAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected ResultInsertCompsOrder doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_casino", params[0]);
            request.addProperty("token", params[1]);
            request.addProperty("id_mesa", params[2]);
            request.addProperty("id_cliente", params[3]);
            request.addProperty("id_juego", params[4]);
            request.addProperty("nombre_cliente", params[5]);
            request.addProperty("rut_cliente", params[6]);
            request.addProperty("id_categoria", params[7]);
            request.addProperty("id_jefe_mesa", params[8]);
            request.addProperty("id_jefe_salon", params[9]);
            request.addProperty("comentario", params[10]);
            request.addProperty("monto_total", params[11]);
            request.addProperty("codigo_rvc", params[12]);
            request.addProperty("lista_productos", params[13]);
            request.addProperty("fuera_regla", params[14]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null || !resSoap.hasProperty("Resultado") || !resSoap.hasProperty("MensajeResultado"))
                    return resultInsertCompsOrder;

                resultInsertCompsOrder.resultado = resSoap.getPropertyAsString("Resultado");
                resultInsertCompsOrder.mensaje_resultado = resSoap.getPropertyAsString("MensajeResultado");

            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return resultInsertCompsOrder;
        }

        @Override
        protected void onPostExecute(ResultInsertCompsOrder result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // getHistorialCliente
    public static class GetPlayerHistoryAsyncTask extends AsyncTask<String, Void, HistorialClienteDM> {
        private HistorialClienteDM historialCliente = new HistorialClienteDM();
        private String METHOD_NAME = "getHistorialCliente";
        private String PROPERTY_NAME = "HistorialCliente";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public GetPlayerHistoryAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected HistorialClienteDM doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_casino", params[0]);
            request.addProperty("id_cliente", params[1]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null) return null;

                SoapObject cliente = (SoapObject) resSoap.getProperty("cliente");
                SoapObject detalleOperaciones = (SoapObject) resSoap.getProperty("historial");

                String horaInicioJuego = cliente.getPropertyAsString("fecha_inicio").replace("T", " ");
                String horaActualServidor = cliente.getPropertyAsString("fecha_actual").replace("T", " ");
                String tiempoJuego = Utils.getDiffFechas(horaActualServidor, horaInicioJuego);

                historialCliente.id_cliente = cliente.getPropertyAsString("id_cliente");
                historialCliente.id_cliente_mcc = cliente.getPropertyAsString("cod_cliente");
                historialCliente.tiempo_juego = tiempoJuego;
                historialCliente.consumo_cliente = Long.parseLong(cliente.getPropertyAsString("consumo"));
                historialCliente.producto_preferido = cliente.getPropertyAsString("producto_preferido");
                historialCliente.win_teorico = Long.parseLong(cliente.getPropertyAsString("win_teorico"));
                historialCliente.cantidad_comps = Integer.parseInt(cliente.getPropertyAsString("cantidad_comps"));
                historialCliente.monto_cambios_dinero = Long.parseLong(cliente.getPropertyAsString("monto_cambios"));
                historialCliente.cantidad_cambios_dinero = Integer.parseInt(cliente.getPropertyAsString("cantidad_cambios"));

                if (detalleOperaciones != null) {
                    ArrayList<ItemDetalleHistorialDM> listaOperaciones = new ArrayList<ItemDetalleHistorialDM>();

                    for (int i = 0; i < detalleOperaciones.getPropertyCount(); i++) {
                        SoapObject operacion = (SoapObject) detalleOperaciones.getProperty(i);

                        ItemDetalleHistorialDM item = new ItemDetalleHistorialDM();

                        item.numero_operacion = i + 1;
                        item.fecha_operacion = operacion.getPropertyAsString("fecha");
                        item.tipo_operacion = operacion.getPropertyAsString("tipo");
                        item.costo = Long.parseLong(operacion.getPropertyAsString("costo"));
                        item.info_adicional_operacion = operacion.getPropertyAsString("info");

                        listaOperaciones.add(item);
                    }

                    historialCliente.listaOperacionesCliente = listaOperaciones;
                }
            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                return null;
            }

            return historialCliente;
        }

        @Override
        protected void onPostExecute(HistorialClienteDM result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // getMesasCasino
    public static class GetMesasAsyncTask extends AsyncTask<String, Void, ArrayList<TipoJuegoDM>> {
        private ArrayList<TipoJuegoDM> listaMesas = new ArrayList<>();
        private String METHOD_NAME = "getMesasCasino";
        private String PROPERTY_NAME = "MesasCasino";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public GetMesasAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected ArrayList<TipoJuegoDM> doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_casino", params[0]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null) return null;

                for (int i = 0; i < resSoap.getPropertyCount(); i++) {
                    SoapObject tipoJuegoObject = (SoapObject) resSoap.getProperty(i);


                    TipoJuegoDM tipoJuego = new TipoJuegoDM();

                    tipoJuego.setNombre_tipo_juego(tipoJuegoObject.getPropertyAsString("nombre"));
                    tipoJuego.setImagen_b64(tipoJuegoObject.getPropertyAsString("imagen_mesa"));

                    SoapObject mesas = (SoapObject) tipoJuegoObject.getProperty("list_mesas");

                    ArrayList<MesaDM> mesasArrayList = new ArrayList<>();

                    for (int j = 0; j < mesas.getPropertyCount(); j++) {
                        SoapObject mesaObject = (SoapObject) mesas.getProperty(j);

                        Log.i("mesaObject", mesaObject.toString());

                        MesaDM m = new MesaDM();

                        m.setNombre_juego(tipoJuego.getNombre_tipo_juego());
                        m.setId_mesa(mesaObject.getPropertyAsString("id_mesa"));
                        m.setId_sesion(GlobalData.EMPTY);
                        m.setClase_mesa(mesaObject.getPropertyAsString("clase_mesa"));
                        m.setNombre_completo_mesa(mesaObject.getPropertyAsString("nombre_mesa"));
                        m.setBg_mesa_b64(GlobalData.EMPTY);
                        m.setImagen_mesa_b64(tipoJuego.getImagen_b64());
                        m.setCant_asientos(Integer.parseInt(mesaObject.getPropertyAsString("cant_asientos")));
                        m.setSelected(false);
                        m.setJugadores(new ArrayList<JugadorDM>());

                        int ubicacionMesa = Integer.parseInt(mesaObject.getPropertyAsString("ubicacion_mesa"));

                        switch (ubicacionMesa) {
                            case 1:
                                m.setOrientacion_mesa(MesaDM.OrientacionesMesa.ARRIBA);
                                break;

                            case 2:
                                m.setOrientacion_mesa(MesaDM.OrientacionesMesa.ABAJO);
                                break;

                            case 3:
                                m.setOrientacion_mesa(MesaDM.OrientacionesMesa.DERECHA);
                                break;

                            case 4:
                                m.setOrientacion_mesa(MesaDM.OrientacionesMesa.IZQUIERDA);
                                break;

                            case 5:
                                m.setOrientacion_mesa(MesaDM.OrientacionesMesa.CENTRO);
                                break;

                            default:
                                m.setOrientacion_mesa(MesaDM.OrientacionesMesa.ABAJO);
                                break;
                        }

                        if(mesaObject.hasProperty("list_asientos")){
                            Log.e("ERROR","NO EXISTE list_asientos");


                        SoapObject posiciones = (SoapObject) mesaObject.getProperty("list_asientos");

                        ArrayList<PosicionesJugadoresMesaDM> posicionesArrayList = new ArrayList<>();

                        Log.e("posiciones", posiciones.toString());
                        for (int k = 0; k < posiciones.getPropertyCount(); k++) {
                            SoapObject posicionObject = (SoapObject) posiciones.getProperty(k);

                            Log.i("posicionObject", posicionObject.toString());

                            PosicionesJugadoresMesaDM pos = new PosicionesJugadoresMesaDM();

                            pos.setNumero_posicion(Integer.parseInt(posicionObject.getPropertyAsString("idAsiento")));
                            pos.setPosX(Integer.parseInt(posicionObject.getPropertyAsString("x")));
                            pos.setPosY(Integer.parseInt(posicionObject.getPropertyAsString("y")));

                            posicionesArrayList.add(pos);
                        }

                        m.setPosiciones_jugadores(posicionesArrayList);

                        mesasArrayList.add(m);
                        }else{
                            Log.e("ERROR","NO EXISTE list_asientos");
                        }
                    }

                    tipoJuego.setMesas(mesasArrayList);

                    listaMesas.add(tipoJuego);
                }
            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                return null;
            }

            return listaMesas;
        }

        @Override
        protected void onPostExecute(ArrayList<TipoJuegoDM> result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // updateMinMaxMesa
    public static class UpdateMinMaxAsyncTask extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "updateMinMaxMesa";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public UpdateMinMaxAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_sesion", params[0]);
            request.addProperty("min_mesa", params[1]);
            request.addProperty("max_mesa", params[2]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // insertRelevoUsuario
    public static class InsertUserReliefAsyncTask extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "insertRelevoUsuario";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public InsertUserReliefAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_jefe_mesa", params[0]);
            request.addProperty("id_croupier", params[1]);
            request.addProperty("id_sesion", params[2]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());
            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                res = 0;
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // finishRelevoCroupier
    public static class FinishUserReliefAsyncTask extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "finishRelevoUsuario";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public FinishUserReliefAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_usuario", params[0]);
            request.addProperty("id_sesion", params[1]);
            request.addProperty("tipo_usuario", params[2]);

            Logger.d("id_usuario= "+params[0] + "\n" +
                    "id_sesion= "+params[1] + "\n" +
                    "tipo_usuario= "+params[2]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());
            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                res = 0;
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // TODO: UpdatePlayerPosition
    public static class UpdatePlayerPositionAsyncTask extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "updatePosicionCliente";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public UpdatePlayerPositionAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_juego_origen", params[0]);
            request.addProperty("posicion_origen", params[1]);
            request.addProperty("id_juego_destino", params[2]);
            request.addProperty("posicion_destino", params[3]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            }
            catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                res = 0;
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // finishAllGames
    private static class finishAllGames extends AsyncTask<String, Void, Integer> {

        private String METHOD_NAME = "finishAllGames";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_sesion", params[0]);
            request.addProperty("hora_termino", params[1]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(
                    ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                res = 0;
            }

            return res;
        }
    }

    // updateSession
    private static class updateSession extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "updateSession";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_sesion", params[0]);
            request.addProperty("id_usuario", params[1]);
            request.addProperty("tipo_usuario", params[2]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                res = 0;
            }

            return res;
        }
    }

    // TODO: insertSolicitud
    public static class InsertRequestAsyncTask extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "insertSolicitud";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private Integer res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public InsertRequestAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("juego_id_juego", params[0]);
            request.addProperty("solicitud_tipo_id_solicitud_tipo", params[1]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                res = 0;
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // TODO: insertPremio
    public static class InsertPrizeAsyncTask extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "insertPremio";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public InsertPrizeAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_juego", params[0]);
            request.addProperty("monto", params[1]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                res = 0;
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // checkAvailableCompCliente
    public static class CheckPlayerCompsAvailabilityAsyncTask extends AsyncTask<String, Void, CompAvailabilityDM> {
        private CompAvailabilityDM compAvailabilityDM = new CompAvailabilityDM();
        private String METHOD_NAME = "checkAvailableCompCliente";
        private String PROPERTY_NAME = "CompsCliente";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public CheckPlayerCompsAvailabilityAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected CompAvailabilityDM doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_casino", params[0]);
            request.addProperty("id_cliente", params[1]);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null) return null;

                compAvailabilityDM.setIs_comp_available(Boolean.parseBoolean(resSoap.getPropertyAsString("comp_disponible")));

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                return null;
            }

            return compAvailabilityDM;
        }

        @Override
        protected void onPostExecute(CompAvailabilityDM result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // insertRecuentoOperativo
    public static class InsertOperativeCountingAsyncTask extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "insertRecuentoOperativo";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public InsertOperativeCountingAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_sesion", params[0]);
            request.addProperty("monto_total", params[1]);
            request.addProperty("tipo_registro", params[2]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                res = 0;
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // TODO: cerrarMesa
    public static class CloseTableAsyncTask extends AsyncTask<String, Void, Integer> {
        private String METHOD_NAME = "cerrarMesa";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        private int res = 0;
        public AsyncResponse out;
        private ProgressDialog pd;

        public CloseTableAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            // Argumentos de WSDL
            request.addProperty("id_sesion", params[0]);
            request.addProperty("id_croupier", params[1]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();

                if (resSoap == null) return 0;

                res = Integer.parseInt(resSoap.toString());

            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                res = 0;
            }

            return res;
        }

        @Override
        protected void onPostExecute(Integer result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    // checkAvailableCompCliente
    public static class GetComentariosAsyncTask extends AsyncTask<String, Void, ArrayList<MotivosAutorizacionCompsDM>> {
        private ArrayList<MotivosAutorizacionCompsDM> listaMotivosAutorizacion = new ArrayList<MotivosAutorizacionCompsDM>();
        private String METHOD_NAME = "getMotivosAutorizacionComps";
        private String PROPERTY_NAME = "MotivosAutorizacion";
        private String SOAP_ACTION = ConnectionServer.NAMESPACE + METHOD_NAME;
        public AsyncResponse out;
        private ProgressDialog pd;

        public GetComentariosAsyncTask(ProgressDialog pd, AsyncResponse asyncResponse) {
            this.out = asyncResponse;
            this.pd = pd;
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected ArrayList<MotivosAutorizacionCompsDM> doInBackground(String... params) {
            SoapObject request = new SoapObject(ConnectionServer.NAMESPACE, METHOD_NAME);

            PropertyInfo pi = new PropertyInfo();

            pi.setName(PROPERTY_NAME);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE httpTransport = new HttpTransportSE(ConnectionServer.URL_ws1);

            try {
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resSoap = (SoapObject) envelope.getResponse();

                if (resSoap == null) return listaMotivosAutorizacion;

                for (int i = 0; i < resSoap.getPropertyCount(); i++) {
                    SoapObject motivoAutorizacion = (SoapObject) resSoap.getProperty(i);

                    MotivosAutorizacionCompsDM motivosAutorizacionComps = new MotivosAutorizacionCompsDM();

                    motivosAutorizacionComps.id_motivo_autorizacion = motivoAutorizacion.getPropertyAsString("id_motivo");
                    motivosAutorizacionComps.descripcion_motivo_autorizacion = motivoAutorizacion.getPropertyAsString("descripcion");

                    listaMotivosAutorizacion.add(motivosAutorizacionComps);
                }
            } catch (Exception exception) {
                Log.v("Error SQL " + METHOD_NAME, exception.toString());

                listaMotivosAutorizacion = null;
            }

            return listaMotivosAutorizacion;
        }

        @Override
        protected void onPostExecute(ArrayList<MotivosAutorizacionCompsDM> result) {
            out.onProcessFinish(result);

            pd.dismiss();
        }
    }

    public interface AsyncResponse {
        void onProcessFinish(Object result);
    }
}
