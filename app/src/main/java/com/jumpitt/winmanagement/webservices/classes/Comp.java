package com.jumpitt.winmanagement.webservices.classes;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class Comp implements KvmSerializable {
	public int id_comp;
	public String nom_comp;
	public String tipo_comp;
	public int precio_comp;

	public Comp()
	{
		this.id_comp = 0;
		this.nom_comp = "";
		this.tipo_comp = "";
		this.precio_comp = 0;
	}

	public Comp(int id_comp, String nom_comp, String tipo_comp, int precio_comp)
	{
		this.id_comp = id_comp;
		this.nom_comp = nom_comp;
		this.tipo_comp = tipo_comp;
		this.precio_comp = precio_comp;
	}

	@Override
	public Object getProperty(int arg0) {

		switch(arg0)
        {
        case 0:
            return id_comp;
        case 1:
            return nom_comp;
        case 2:
            return tipo_comp;
        case 3:
            return precio_comp;
        }

		return null;
	}

	@Override
	public int getPropertyCount() {
		return 4;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void getPropertyInfo(int ind, Hashtable ht, PropertyInfo info) {
		switch(ind)
        {
	        case 0:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Id comp";
	            break;
	        case 1:
	            info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Nom comp";
	            break;
	        case 2:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Tipo comp";
	            break;
	        case 3:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "Precio comp";
	            break;
	        default:break;
        }
	}

	@Override
	public void setProperty(int ind, Object val) {
		switch(ind)
        {
	        case 0:
	            id_comp = Integer.parseInt(val.toString());
	            break;
	        case 1:
	            nom_comp = val.toString();
	            break;
	        case 2:
	            tipo_comp = val.toString();
	            break;
	        case 3:
	            precio_comp = Integer.parseInt(val.toString());
	            break;
	        default:
	            break;
        }
	}
}