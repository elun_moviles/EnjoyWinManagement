package com.jumpitt.winmanagement;

import android.app.Application;


import com.crashlytics.android.Crashlytics;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.HawkBuilder;
import com.orhanobut.hawk.LogLevel;
import com.orhanobut.logger.Logger;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class WinManagement extends Application {
    protected static final String TAG_APP = "WinManagement_Mesas";

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        // Default font
        initCalligraphy();

        // Default logger
        initLogger();

        initHawk();
    }

    protected void initCalligraphy() {
        CalligraphyConfig
                .initDefault(new CalligraphyConfig.Builder().setDefaultFontPath("fonts/Lato-Regular.ttf").setFontAttrId(R.attr.fontPath).build());
    }

    protected void initLogger() {
        Logger.init(TAG_APP);
//              .setMethodCount(0)
//              .hideThreadInfo();
    }

    private void initHawk() {
        Hawk.init(this)
                .setStorage(HawkBuilder.newSharedPrefStorage(this))
                .setEncryptionMethod(HawkBuilder.EncryptionMethod.NO_ENCRYPTION)
                .setLogLevel(LogLevel.FULL)
                .build();
    }
}
