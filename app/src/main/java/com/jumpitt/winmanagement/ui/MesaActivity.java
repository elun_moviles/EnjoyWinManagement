package com.jumpitt.winmanagement.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.util.Log;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.HistorialClienteDM;
import com.jumpitt.winmanagement.dataModel.JugadorDM;
import com.jumpitt.winmanagement.dataModel.MesaDM;
import com.jumpitt.winmanagement.dataModel.PosicionesJugadoresMesaDM;
import com.jumpitt.winmanagement.dataModel.UsuarioDM;
import com.jumpitt.winmanagement.dataModel.enums.KeyboardTypes;
import com.jumpitt.winmanagement.dataModel.enums.PlayerTypes;
import com.jumpitt.winmanagement.dataModel.enums.PlayersSearchCriteria;
import com.jumpitt.winmanagement.ui.comps.CompsActivity;
import com.jumpitt.winmanagement.utils.BaseActivity;
import com.jumpitt.winmanagement.utils.GlobalData;
import com.jumpitt.winmanagement.utils.PlayerView;
import com.jumpitt.winmanagement.utils.TableOptions;
import com.jumpitt.winmanagement.utils.Utils;
import com.jumpitt.winmanagement.utils.controllers.ConfirmationDialogController;
import com.jumpitt.winmanagement.utils.controllers.KeyboardController;
import com.jumpitt.winmanagement.utils.controllers.MinMaxValuesController;
import com.jumpitt.winmanagement.utils.controllers.OptionsDialogController;
import com.jumpitt.winmanagement.utils.controllers.PlayerHistoryController;
import com.jumpitt.winmanagement.utils.controllers.ValuesKeyboardController;
import com.jumpitt.winmanagement.utils.interfaces.OnButtonPressedListener;
import com.jumpitt.winmanagement.utils.interfaces.OnMinMaxValuesEnteredListener;
import com.jumpitt.winmanagement.utils.interfaces.OnOptionSelectedListener;
import com.jumpitt.winmanagement.utils.interfaces.OnPlayerImageClickListener;
import com.jumpitt.winmanagement.utils.interfaces.OnValueEnteredListener;
import com.jumpitt.winmanagement.webservices.ServicioMesas;
import com.jumpitt.winmanagement.webservices.ServicioMesas.AsyncResponse;
import com.jumpitt.winmanagement.webservices.ServicioMesas.InsertMoneyChangeAsyncTask;
import com.jumpitt.winmanagement.webservices.ServicioMesas.InsertTrackingAsyncTask;
import com.jumpitt.winmanagement.webservices.classes.CompAvailabilityDM;
import com.jumpitt.winmanagement.webservices.classes.SignIn;
import com.jumpitt.winmanagement.webservices.classes.SignInTable;
import com.jumpitt.winmanagement.webservices.classes.UbicacionMesa;

import java.util.ArrayList;

@SuppressWarnings("deprecation")
public class MesaActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Context c;
    private Activity activity;
    private GlobalData gd;
    private int numero_jugador_seleccionado;
    private ArrayList<JugadorDM> playersArrayList = new ArrayList<>();
    private ArrayList<PlayerView> playersViewArrayList = new ArrayList<>();
    private LinearLayout tableLinearLayout;
    private LinearLayout row1;
    private LinearLayout row2;
    private LinearLayout row3;
    private LinearLayout row4;
    private RelativeLayout containerBtnDeletePlayer;
    private Button btnJefeMesa;
    private Button btnCroupier;
    private Button btnIdCliente;
    private Button btnRutCliente;
    private Button btnClienteAnonimo;
    private ImageButton btnTableOptions;
    private String digits;
    private String userType;
    private ImageView imgTable;
    private ImageView btnDeletePlayer;
    private ImageView appLogo;
    private Dialog dialogAddUser;
    private Dialog dialogUserAccepted;
    private Dialog dialogEnterNewUser;
    private Handler handlerPlayersTimer;
    private Handler handlerOperativeCounting;
    private Runnable runnableOperativeCounting;
    private Runnable runnablePlayersTimer;
    private KeyboardController addUserKeyboard;
    private ValuesKeyboardController valuesKeyboard;
    private ConfirmationDialogController confirmationDialog;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private DialogInterface.OnKeyListener enterUserOnKeyListener;
    private DialogInterface.OnKeyListener addPlayerOnKeyListener;
    private enum TrackingStates { OK, SLOW, EXPIRED }
    private Vibrator vibrator;
    private OnPlayerImageClickListener emptyPlayerImageClickListener;
    private OnPlayerImageClickListener anonymousPlayerImageClickListener;
    private OnPlayerImageClickListener registeredPlayerImageClickListener;
    private int currentApiVersion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesa);

        gd = (GlobalData) getIntent().getSerializableExtra("global_data");

        c = this;
        activity = this;
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        initViews();

        goneViewHideen();

        setInitialPlayersPosition();

        startHandlerRecuentoOperativo();
        startPlayersTimer();
    }

    private void goneViewHideen() {


        currentApiVersion = android.os.Build.VERSION.SDK_INT;

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;


        if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
        {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }

    }

    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }


    private void initViews() {
        addUserKeyboard = new KeyboardController(this);
        valuesKeyboard = new ValuesKeyboardController(this, false);
        confirmationDialog = new ConfirmationDialogController(this);

        numero_jugador_seleccionado = -1;
        digits = GlobalData.EMPTY;

        setUpOnKeyListeners();

        setPlayerImageClickListener(null);

        initDialogsViews();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.left_drawer);

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        btnDeletePlayer = (ImageView) findViewById(R.id.btnDeletePlayer);
        tableLinearLayout = (LinearLayout) findViewById(R.id.tableLinearLayout);
        row1 = (LinearLayout) findViewById(R.id.row1);
        row2 = (LinearLayout) findViewById(R.id.row2);
        row3 = (LinearLayout) findViewById(R.id.row3);
        row4 = (LinearLayout) findViewById(R.id.row4);
        containerBtnDeletePlayer = (RelativeLayout) findViewById(R.id.containerBtnDeletePlayer);
        imgTable = (ImageView) findViewById(R.id.tableImg);
        appLogo = (ImageView) findViewById(R.id.logo);
        TextView tableName = (TextView) findViewById(R.id.tableName);
        TextView btnCurrentTime = (TextView) findViewById(R.id.currentTime);
        btnJefeMesa = (Button) findViewById(R.id.jefemesaName);
        btnCroupier = (Button) findViewById(R.id.croupierName);
        btnTableOptions = (ImageButton) findViewById(R.id.options);

        setTableImageAndBg();

        Utils.setCurrentTime24Hours(btnCurrentTime);

        tableName.setText(gd.getMesa().getNombre_completo_mesa().toUpperCase(Utils.locale));
        btnJefeMesa.setText(gd.getJefeMesa().getNombre_usuario());
        btnCroupier.setText(gd.getCroupier().getNombre_usuario());

        initListeners();
    }

    private void initDialogsViews() {
        dialogAddUser = Utils.getNewDialog(this, R.layout.dialog_ingresar_cliente, true, true, 850, LayoutParams.WRAP_CONTENT);
        dialogUserAccepted = Utils.getNewDialog(this, R.layout.dialog_jugador_aceptado, false, true, 550, LayoutParams.WRAP_CONTENT);
        btnIdCliente = (Button) dialogAddUser.findViewById(R.id.ingresarIdCliente);
        btnRutCliente = (Button) dialogAddUser.findViewById(R.id.ingresarRutCliente);
        btnClienteAnonimo = (Button) dialogAddUser.findViewById(R.id.ingresarAnonimo);

        dialogAddUser.setOnKeyListener(addPlayerOnKeyListener);

        dialogAddUser.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                stopCardAnimation(dialogAddUser);
            }
        });
    }

    private void initListeners() {
        navigationView.setNavigationItemSelectedListener(this);
        btnJefeMesa.setOnClickListener(new MesaOnClickListener());
        btnCroupier.setOnClickListener(new MesaOnClickListener());
        btnTableOptions.setOnClickListener(new MesaOnClickListener());
        btnIdCliente.setOnClickListener(new MesaOnClickListener());
        btnRutCliente.setOnClickListener(new MesaOnClickListener());
        btnClienteAnonimo.setOnClickListener(new MesaOnClickListener());
        appLogo.setOnClickListener(new MesaOnClickListener());
        btnDeletePlayer.setOnDragListener(new MesaOnDragListener());
    }

    private void setTableImageAndBg() {
        String bg_mesa_b64 = gd.getMesa().getBg_mesa_b64();
        String img_mesa_b64 = gd.getMesa().getImagen_mesa_b64();

        if (!bg_mesa_b64.isEmpty()) {
            tableLinearLayout.setBackground(Utils.getDrawableFromBase64(this, bg_mesa_b64));
        }
        else {
            tableLinearLayout.setBackgroundResource(R.drawable.img_default_bg_table);
        }

        if (!img_mesa_b64.isEmpty()) {
            imgTable.setImageBitmap(Utils.getBitmapFromBase64(gd.getMesa().getImagen_mesa_b64()));
        }
        else {
            imgTable.setImageResource(R.drawable.img_unknown_table);
        }
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // TODO : orientacion mesa jugadores

        switch (gd.getMesa().getOrientacion_mesa()) {
            case ARRIBA:
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);

                imgTable.setRotation(180);
                break;

            case ABAJO:
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);

                imgTable.setRotation(0);
                break;

            case DERECHA:
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);

                imgTable.setRotation(-90);

                if (gd.getMesa().getClase_mesa().equals(GlobalData.RULETA_SIGLA)) {
                    params.rightMargin = 190;
//                    imgTable.setPadding(0, 0, 0, 195);
                }
                else {
                    imgTable.setPadding(0, 430, 0, 0);
                }

                break;

            case IZQUIERDA:
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);

                imgTable.setRotation(90);

                if (gd.getMesa().getClase_mesa().equals(GlobalData.RULETA_SIGLA)) {
                    params.leftMargin = 190;
//                    imgTable.setPadding(0, 0, 0, 195);
                }
                else {
                    imgTable.setPadding(0, 430, 0, 0);
                }

                break;

            case CENTRO:
                params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                imgTable.setRotation(0);
                break;
        }

        imgTable.setLayoutParams(params);
    }

    private void startPlayersTimer() {
        handlerPlayersTimer = new Handler();

        runnablePlayersTimer = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < playersViewArrayList.size(); i++) {
                    PlayerView playerView = getPlayerViewByPosition(i);

                    if (playerView != null) {
                        JugadorDM jugador = playerView.getJugador();

                        if (jugador != null && jugador.getPlayerType() != PlayerTypes.EMPTY) {
                            TextView timerTextView = playerView.playerTimer;
                            String timerTextStr = timerTextView.getText().toString();

                            String[] timerArray = timerTextStr.split(":");

                            checkTrackingTime(timerArray, playerView);

                            long seconds = (Integer.parseInt(timerArray[0]) * 3600) + (Integer.parseInt(timerArray[1]) * 60) + Integer.parseInt(timerArray[2]);

                            if (!jugador.isPaused()) {
                                timerTextStr = Utils.formatDate(String.valueOf((seconds + 1)), "ss", "HH:mm:ss");

                                timerTextView.setText(timerTextStr);
                            }

                            jugador.setTimer_jugador(timerTextStr);

                            setPlayerViewInPosition(playerView, i);
                        }
                    }
                }

                handlerPlayersTimer.postDelayed(this, 1000);
            }
        };

        handlerPlayersTimer.postDelayed(runnablePlayersTimer, 1000);
    }

    private void checkTrackingTime(String[] timerArray, PlayerView playerView) {
        int horas = Integer.parseInt(timerArray[0]);
        int minutos = Integer.parseInt(timerArray[1]);

        TextView playerName = (TextView) playerView.getRootView().findViewById(R.id.playerName);
        ImageView playerImage = (ImageView) playerView.getRootView().findViewById(R.id.playerImg);
        TextView playerTimer = (TextView) playerView.getRootView().findViewById(R.id.playerTimer);

        Animation alert_warning = AnimationUtils.loadAnimation(this, R.anim.alert_warning);
        Animation alert_time_exceeded = AnimationUtils.loadAnimation(this, R.anim.alert_time_exceeded);

        if (horas <= 0 && minutos >= 0 && minutos < GlobalData.LIMITE_INFERIOR_TRACKING) {
            setStateColor(playerName, playerTimer, TrackingStates.OK);
            playerImage.clearAnimation();
        } else if (horas <= 0 && minutos >= GlobalData.LIMITE_INFERIOR_TRACKING && minutos < GlobalData.LIMITE_SUPERIOR_TRACKING) {
            setStateColor(playerName, playerTimer, TrackingStates.SLOW);
            playerImage.clearAnimation();
            playerImage.startAnimation(alert_warning);
        } else if (horas >= 1 || minutos >= GlobalData.LIMITE_SUPERIOR_TRACKING) {
            setStateColor(playerName, playerTimer, TrackingStates.EXPIRED);
            playerImage.clearAnimation();
            playerImage.startAnimation(alert_time_exceeded);
        }
    }

    private void setStateColor(TextView nombre, TextView timer, TrackingStates trackingState) {
        switch (trackingState) {
            case OK:
                nombre.setBackgroundResource(R.drawable.shape_player_name);
                timer.setBackgroundColor(getResources().getColor(R.color.bg_timer_players));
                break;

            case SLOW:
                nombre.setBackgroundResource(R.drawable.shape_player_name_tracking_slow);
                timer.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                break;

            case EXPIRED:
                nombre.setBackgroundResource(R.drawable.shape_player_name_tracking_expired);
                timer.setBackgroundColor(getResources().getColor(R.color.bg_dark_red));
                break;
        }
    }

    private void setInitialPlayersPosition() {
        for (int i = 0; i < GlobalData.TABLE_SIZE_WIDTH; i++) {
            LinearLayout auxRow;

            for (int j = 0; j < GlobalData.TABLE_SIZE_HEIGHT; j++) {
                boolean isPlayerPosition = false;
                int numero_jugador = 0;

//                for (int k = 0; k < gd.getMesa().getCant_asientos(); k++) {
                for (int k = 0; k < gd.getMesa().getPosiciones_jugadores().size(); k++) {
                    PosicionesJugadoresMesaDM posicion = gd.getMesa().getPosiciones_jugadores().get(k);

                    if ((posicion.getPosY() - 1) == i && (posicion.getPosX() - 1) == j) {
                        isPlayerPosition = true;

                        numero_jugador = posicion.getNumero_posicion();

                        break;
                    }
                }

                switch (i) {
                    case 0:
                        auxRow = row1;
                        break;

                    case 1:
                        auxRow = row2;
                        break;

                    case 2:
                        auxRow = row3;
                        break;

                    case 3:
                        auxRow = row4;
                        break;

                    default:
                        auxRow = row1;
                        break;
                }

                PlayerView playerView = new PlayerView(activity);

                if (isPlayerPosition) {
                    JugadorDM newPlayer = new JugadorDM();

                    newPlayer.setNombre_jugador(getString(R.string.empty_text));
                    newPlayer.setNumero_jugador(numero_jugador);
                    newPlayer.setPosX(i);
                    newPlayer.setPosY(j);

                    playerView.playerImage.setOnDragListener(new MesaOnDragListener());

                    playerView.populateView(newPlayer, PlayerTypes.EMPTY, emptyPlayerImageClickListener);

                    gd.getMesa().getJugadores().add(newPlayer);

                    playersViewArrayList.add(playerView);
                }
                else {
                    playerView.getRootView().setVisibility(View.INVISIBLE);
                }

                Log.i("playerView",playerView.toString());
                auxRow.addView(playerView.getRootView());
            }
        }
    }

    private void setButtonDeleteStrokeColor(JugadorDM jugador) {
        int imageBackgroundColor = R.color.colorAccent;

        String categoriaJugador = jugador.getCategoria_jugador().toUpperCase(Utils.locale);

        if(categoriaJugador.equals(GlobalData.CLASSIC_CATEGORY)) {
            imageBackgroundColor = R.color.category_classic;
        }
        else if(categoriaJugador.equals(GlobalData.SILVER_CATEGORY)) {
            imageBackgroundColor = R.color.category_silver;
        }
        else if(categoriaJugador.equals(GlobalData.GOLD_CATEGORY)) {
            imageBackgroundColor = R.color.category_gold;
        }
        else if(categoriaJugador.equals(GlobalData.PLATINUM_CATEGORY)) {
            imageBackgroundColor = R.color.category_platinum;
        }
        else if(categoriaJugador.equals(GlobalData.DIAMOND_CATEGORY)) {
            imageBackgroundColor = R.color.category_diamond;
        }

        ((GradientDrawable) containerBtnDeletePlayer.getBackground()).setStroke(4, getResources().getColor(imageBackgroundColor));
    }

    private void setPlayerImageClickListener(final PlayerView playerView) {
        emptyPlayerImageClickListener = new OnPlayerImageClickListener() {
            @Override
            public void onSingleClick(View v, JugadorDM player) {
                numero_jugador_seleccionado = player.getNumero_jugador();

                startCardAnimation(dialogAddUser);

                TextView numJugador = (TextView) dialogAddUser.findViewById(R.id.numJugador);

                String ofClientText = String.format("%s %d", getString(R.string.of_client_text), numero_jugador_seleccionado);

                numJugador.setText(ofClientText);

                dialogAddUser.show();
            }

            @Override
            public void onDoubleClick(View v, JugadorDM player) {
            }

            @Override
            public void onLongClick(View v, boolean playerIsPaused) {
                Utils.toast(c, "onLongClick", 0);
            }
        };

        if (playerView != null) {
            anonymousPlayerImageClickListener = new OnPlayerImageClickListener() {
                @Override
                public void onSingleClick(View v, JugadorDM player) {
                    numero_jugador_seleccionado = player.getNumero_jugador();

                    TextView playerNameNV = (TextView) navigationView.findViewById(R.id.name);
                    ImageButton playerImageNV = (ImageButton) navigationView.findViewById(R.id.img);

                    if (!player.isPaused()) {
                        playerNameNV.setText(playerView.getJugador().getNombre_jugador());
                        playerNameNV.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        playerImageNV.setImageDrawable(getResources().getDrawable(R.drawable.img_player_unknown));

                        // Ocultar la opcion de historial para el usuario anonimo
//                        navigationView.getMenu().findItem(R.id.drawer_item_client_history).setVisible(false);

                        if(gd.getJefeMesa() == null) {
                            // Ocultar las opciones correspondientes si el jefe de mesa no esta conectado
                            navigationView.getMenu().findItem(R.id.drawer_item_tracking).setEnabled(false);
                            navigationView.getMenu().findItem(R.id.drawer_item_comps).setEnabled(false);
                        }
                        else {
                            // Mostrar las opciones correspondientes si el jefe de mesa esta conectado en la mesa
                            navigationView.getMenu().findItem(R.id.drawer_item_tracking).setEnabled(true);
                            navigationView.getMenu().findItem(R.id.drawer_item_comps).setEnabled(true);
                        }

                        // Cambiar el texto "Club Enjoy" por "Invitar al Club Enjoy" para el usuario anonimo
                        navigationView.getMenu().findItem(R.id.drawer_item_enjoy_club).setTitle(R.string.invite_enjoy_club_text);

                        drawerLayout.openDrawer(navigationView);

                        playerImageNV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                drawerLayout.closeDrawer(navigationView);

                                Utils.showInformationDialog(activity, false, c.getString(R.string.soon_text), 1200);
                            }
                        });
                    } else {
                        Utils.toast(c, GlobalData.ERROR_PLAYER_PAUSED_CAN_NOT_OPEN_MENU, 1);
                    }
                }

                @Override
                public void onDoubleClick(View v, JugadorDM player) {
                    setPausedStatePlayer(playerView);
                }

                @Override
                public void onLongClick(View v, boolean playerIsPaused) {
                    if(!playerIsPaused) {
                        numero_jugador_seleccionado = playerView.getJugador().getNumero_jugador();

                        vibrator.vibrate(200);

                        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(playerView.getRootView());

                        playerView.getRootView().startDrag(null, shadowBuilder, v, 0);

                        setButtonDeleteStrokeColor(playerView.getJugador());

                        containerBtnDeletePlayer.setVisibility(View.VISIBLE);
                        Utils.startViewAnimation(activity, containerBtnDeletePlayer, R.anim.fade_in);
                    }
                    else {
                        Utils.toast(c, GlobalData.ERROR_PLAYER_PAUSED_CAN_NOT_BE_MOVED, 1);
                    }
                }
            };

            registeredPlayerImageClickListener = new OnPlayerImageClickListener() {
                @Override
                public void onSingleClick(View v, JugadorDM player) {
                    numero_jugador_seleccionado = player.getNumero_jugador();

                    TextView playerNameNV = (TextView) navigationView.findViewById(R.id.name);
                    ImageButton playerImageNV = (ImageButton) navigationView.findViewById(R.id.img);

                    if (!player.isPaused()) {
                        playerNameNV.setText(playerView.getJugador().getNombre_jugador());

                        int categoryBackground = R.color.colorAccent;

                        String categoriaJugador = player.getCategoria_jugador().toUpperCase(Utils.locale);

                        if (categoriaJugador.equals(GlobalData.CLASSIC_CATEGORY)) {
                            categoryBackground = R.color.category_classic;
                        } else if (categoriaJugador.equals(GlobalData.SILVER_CATEGORY)) {
                            categoryBackground = R.color.category_silver;
                        } else if (categoriaJugador.equals(GlobalData.GOLD_CATEGORY)) {
                            categoryBackground = R.color.category_gold;
                        } else if (categoriaJugador.equals(GlobalData.PLATINUM_CATEGORY)) {
                            categoryBackground = R.color.category_platinum;
                        } else if (categoriaJugador.equals(GlobalData.DIAMOND_CATEGORY)) {
                            categoryBackground = R.color.category_diamond;
                        }

                        playerNameNV.setBackgroundResource(categoryBackground);

                        if (player.getFoto_jugador_b64().isEmpty()) {
                            playerImageNV.setImageDrawable(getResources().getDrawable(R.drawable.img_player_no_picture));
                        } else {
                            playerImageNV.setImageBitmap(Utils.getBitmapFromBase64(player.getFoto_jugador_b64()));
                        }

                        // Mostrar la opcion de historial para el usuario registrado
//                        navigationView.getMenu().findItem(R.id.drawer_item_client_history).setVisible(true);

                        // Cambiar el texto "Invitar al Club Enjoy" por "Club Enjoy" para el usuario registrado
                        navigationView.getMenu().findItem(R.id.drawer_item_enjoy_club).setTitle(R.string.enjoy_club_text);

                        if(gd.getJefeMesa() == null) {
                            // Ocultar las opciones correspondientes si el jefe de mesa no esta conectado
                            navigationView.getMenu().findItem(R.id.drawer_item_tracking).setEnabled(false);
                            navigationView.getMenu().findItem(R.id.drawer_item_comps).setEnabled(false);
                        }
                        else {
                            // Mostrar las opciones correspondientes si el jefe de mesa esta conectado en la mesa
                            navigationView.getMenu().findItem(R.id.drawer_item_tracking).setEnabled(true);
                            navigationView.getMenu().findItem(R.id.drawer_item_comps).setEnabled(true);
                        }

                        drawerLayout.openDrawer(navigationView);

                        playerImageNV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                drawerLayout.closeDrawer(navigationView);

                                Utils.showInformationDialog(activity, false, c.getString(R.string.soon_text), 1200);
                            }
                        });
                    } else {
                        Utils.toast(c, GlobalData.ERROR_PLAYER_PAUSED_CAN_NOT_OPEN_MENU, 1);
                    }
                }

                @Override
                public void onDoubleClick(View v, JugadorDM player) {
                    setPausedStatePlayer(playerView);
                }

                @Override
                public void onLongClick(View v, boolean playerIsPaused) {
                    if(!playerIsPaused) {
                        numero_jugador_seleccionado = playerView.getJugador().getNumero_jugador();

                        vibrator.vibrate(200);

                        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(playerView.getRootView());

                        playerView.getRootView().startDrag(null, shadowBuilder, v, 0);

                        setButtonDeleteStrokeColor(playerView.getJugador());

                        containerBtnDeletePlayer.setVisibility(View.VISIBLE);
                        Utils.startViewAnimation(activity, containerBtnDeletePlayer, R.anim.fade_in);
                    }
                    else {
                        Utils.toast(c, GlobalData.ERROR_PLAYER_PAUSED_CAN_NOT_BE_MOVED, 1);
                    }
                }
            };
        }
    }

    private void startCardAnimation(Dialog d) {
        Utils.startViewAnimation(this, d.findViewById(R.id.card), R.anim.tarjeta_login);
    }

    private PlayerView getPlayerViewByNumPlayer(int numJugador) {
        for (int i = 0; i < playersViewArrayList.size(); i++) {
            if ((playersViewArrayList.get(i).getJugador().getNumero_jugador() == numJugador))
                return playersViewArrayList.get(i);
        }

        return null;
    }

    private void stopCardAnimation(Dialog d) {
        Utils.stopViewAnimation(d.findViewById(R.id.card));
    }

    private PlayerView getPlayerViewByImage(View playerImg) {
        for (int i = 0; i < playersViewArrayList.size(); i++) {
            if ((playersViewArrayList.get(i).playerImage == playerImg))
                return playersViewArrayList.get(i);
        }

        return null;
    }

    private PlayerView getPlayerViewByPosition(int position) {
        for (int i = 0; i < playersViewArrayList.size(); i++) {
            if (/*playersViewArrayList.get(i).getJugador().getPlayerType() != PlayerTypes.EMPTY && */(playersViewArrayList.get(i).getJugador().getNumero_jugador() - 1) == position)
                return playersViewArrayList.get(i);
        }

        return null;
    }

    private void setPlayerViewInPosition(PlayerView playerView, int position) {
        for (int i = 0; i < playersViewArrayList.size(); i++) {
            if ((playersViewArrayList.get(i).getJugador().getNumero_jugador() - 1) == position)
                playersViewArrayList.set(i, playerView);
        }
    }

    private void setPausedStatePlayer(PlayerView playerView) {
        if (playerView.getJugador().isPaused()) {
            playerView.getJugador().setIsPaused(false);
            playerView.playerIsPaused.setVisibility(View.GONE);
            playerView.playerImage.setColorFilter(c.getResources().getColor(android.R.color.transparent));
            Utils.stopViewAnimation(playerView.playerTimer);
        }
        else {
            playerView.getJugador().setIsPaused(true);
            playerView.playerIsPaused.setVisibility(View.VISIBLE);
            playerView.playerImage.setColorFilter(c.getResources().getColor(R.color.grey_text_color), PorterDuff.Mode.DARKEN);
            Utils.startViewAnimation(c, playerView.playerTimer, R.anim.is_paused);
        }
    }

    private void doTracking(final PlayerView playerView, String ammount, final Button btnAccept) {
        if (playerView != null) {
            String[] timer = playerView.getJugador().getTimer_jugador().split(":");
            float hora_t = Float.parseFloat(timer[0]) * 3600;
            float min_t = Float.parseFloat(timer[1]) * 60;
            float seg_t = Float.parseFloat(timer[2]);

            String valorTrackingStr = Utils.getCleanString(ammount);
            int valorTrackingInt = Integer.parseInt(valorTrackingStr);
            String id_juego = playerView.getJugador().getId_juego();

            // Total tiempo juego
            float tiempo_t = hora_t + min_t + seg_t;
            int tiempoTrackingSegs = Math.round(tiempo_t);

            long diffTiempo = (GlobalData.LIMITE_INFERIOR_TRACKING * 60) - tiempoTrackingSegs;

            if(valorTrackingInt >= gd.getMesa().getMin_mesa() && valorTrackingInt <= gd.getMesa().getMax_mesa()) {
                AsyncResponse performTrackingResponse = new AsyncResponse() {
                    @Override
                    public void onProcessFinish(Object result) {
                        int res = (int) result;

                        if (res == 1) {
                            valuesKeyboard.closeKeyboard();

                            Utils.showSuccessOperationDialog(activity, false, GlobalData.EMPTY, 1200);

                            playerView.getJugador().setTimer_jugador(getString(R.string.null_timer_tracking));
                            playerView.playerTimer.setText(playerView.getJugador().getTimer_jugador());

                            if (playerView.getJugador().getPlayerType() != PlayerTypes.ANONYMOUS) {
                                checkCompsAvailability(playerView, false);
                            }
                        } else {
                            Utils.showInformationDialog(activity, false, GlobalData.ERROR_REGISTERING_TRACKING, 1200);

                            btnAccept.setEnabled(false);

                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    btnAccept.setEnabled(true);
                                }
                            }, 5000);
                        }
                    }
                };

                InsertTrackingAsyncTask it = new InsertTrackingAsyncTask(Utils.getNewProgressDialog(this, getString(R.string.sending_data), false), performTrackingResponse);

                it.execute(id_juego, String.valueOf(tiempoTrackingSegs), String.valueOf(diffTiempo), valorTrackingStr, Utils.getSecurityToken());
            }
            else if(valorTrackingInt < gd.getMesa().getMin_mesa()){
                Utils.showInformationDialog(activity, false, GlobalData.ERROR_VALUE_TRACKING_IS_LOWER_THAN_MIN.replace("#_MIN_VALUE_#", Utils.moneyFormat(gd.getMesa().getMin_mesa())), 1200);
            }
            else if(valorTrackingInt > gd.getMesa().getMax_mesa()){
                Utils.showInformationDialog(activity, false, GlobalData.ERROR_VALUE_TRACKING_IS_HIGHER_THAN_MAX.replace("#_MAX_VALUE_#", Utils.moneyFormat(gd.getMesa().getMax_mesa())), 1200);
            }
        }
    }

    private void changeMoney(final PlayerView playerView, String ammount, final Button btnAccept) {
        if (playerView != null) {
            String id_juego = playerView.getJugador().getId_juego();

            AsyncResponse changeMoneyResponse = new AsyncResponse() {
                @Override
                public void onProcessFinish(Object result) {
                    int res = (int) result;

                    if (res == 1) {
                        valuesKeyboard.closeKeyboard();

                        Utils.showSuccessOperationDialog(activity, false, GlobalData.EMPTY, 1200);
                    } else {
                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_REGISTERING_CAMBIO_DINERO, 1200);

                        btnAccept.setEnabled(false);

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                btnAccept.setEnabled(true);
                            }
                        }, 5000);
                    }
                }
            };

            InsertMoneyChangeAsyncTask cm = new InsertMoneyChangeAsyncTask(Utils.getNewProgressDialog(this, getString(R.string.sending_data), false), changeMoneyResponse);

//            Logger.d("id_juego= " + id_juego +
//                    "\nammount= " + ammount +
//                    "\nfecha_registro= " + Utils.getDateFormat("yyyy-MM-dd HH:mm:ss"));

            cm.execute(id_juego, ammount, Utils.getDateFormat("yyyy-MM-dd HH:mm:ss"));
        }
    }

    private void showPlayerHistory(final JugadorDM player) {
        AsyncResponse playerHistoryAsyncResponse = new AsyncResponse() {
            @Override
            public void onProcessFinish(Object result) {
                HistorialClienteDM playerHistory = (HistorialClienteDM) result;

                if (playerHistory != null) {
                    PlayerHistoryController playerHistoryController = new PlayerHistoryController(activity);

//                    Logger.d("id_jugador= "+player.getId_jugador());

                    playerHistoryController.showPlayerHistory(player, playerHistory);
                } else {
                    Utils.showInformationDialog(activity, false, GlobalData.ERROR_GETTING_CLIENT_HISTORY, 1200);
                }
            }
        };

        ServicioMesas.GetPlayerHistoryAsyncTask gph = new ServicioMesas.GetPlayerHistoryAsyncTask(Utils.getNewProgressDialog(this, getString(R.string.loading_player_history), false), playerHistoryAsyncResponse);

        gph.execute(GlobalData.ID_UNIDAD, player.getId_jugador());
    }

    private void sendRequest(PlayerView playerView, final String requestType) {
        final String tipoSolicitud = (requestType.equals(GlobalData.TIPO_SOLICITUD_CLUB) ? " Enjoy Club " : " Host ");

        AsyncResponse enjoyClubRequestAsyncResponse = new AsyncResponse() {
            @Override
            public void onProcessFinish(Object result) {
                int res = (int) result;

                if (res == 1) {
                    valuesKeyboard.closeKeyboard();

                    Utils.showSuccessOperationDialog(activity, false, getString(R.string.request_sent).replace("#_REQUEST_TYPE_#", tipoSolicitud), 1200);
                } else {
                    Utils.showInformationDialog(activity, false, GlobalData.ERROR_REGISTERING_CLUB_HOST_REQUEST, 1200);
                }
            }
        };

        ServicioMesas.InsertRequestAsyncTask ir = new ServicioMesas.InsertRequestAsyncTask(Utils.getNewProgressDialog(this, getString(R.string.sending_data), false), enjoyClubRequestAsyncResponse);

        ir.execute(playerView.getJugador().getId_juego(), requestType);
    }

    private void registerPrize(final PlayerView playerView, String ammount, final Button btnAccept) {
        if (playerView != null) {
            String id_juego = playerView.getJugador().getId_juego();

            AsyncResponse insertPrizeResponse = new AsyncResponse() {
                @Override
                public void onProcessFinish(Object result) {
                    int res = (int) result;

                    if (res == 1) {
                        valuesKeyboard.closeKeyboard();

                        Utils.showSuccessOperationDialog(activity, false, GlobalData.EMPTY, 1200);
                    } else {
                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_REGISTERING_PRICE, 1200);

                        btnAccept.setEnabled(false);

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                btnAccept.setEnabled(true);
                            }
                        }, 5000);
                    }
                }
            };

            ServicioMesas.InsertPrizeAsyncTask ip = new ServicioMesas.InsertPrizeAsyncTask(Utils.getNewProgressDialog(this, getString(R.string.sending_data), false), insertPrizeResponse);

            ip.execute(id_juego, ammount);
        }
    }

    void goToCompsActivity(Dialog dialog, boolean requiere_autorizacion, JugadorDM player) {
        dialog.dismiss();

        Intent myIntent = new Intent(c, CompsActivity.class);

//        myIntent.putExtra("id_casino", GlobalData.ID_UNIDAD);
//        myIntent.putExtra("id_jefe_mesa", jefeMesa.getId_usuario());
//        myIntent.putExtra("id_croupier", croupier.getId_usuario());
//        myIntent.putExtra("nombre_mesa", mesa.getNombre_completo_mesa());
//        myIntent.putExtra("id_mesa", mesa.getId_mesa());
//        myIntent.putExtra("id_sesion", mesa.getId_sesion());
//        myIntent.putExtra("tipo_mesa", mesa.getNombre_juego());
//        myIntent.putExtra("numJug", player.getNumero_jugador());
//        myIntent.putExtra("id_juego", player.getId_juego());
//        myIntent.putExtra("id_cliente_mcc", player.getId_mcc_jugador());
//        myIntent.putExtra("id_categoria_cliente", player.getId_categoria_jugador());
//        myIntent.putExtra("rut_cliente", player.getRut_jugador());
//        myIntent.putExtra("requiere_autorizacion", requiere_autorizacion);

        myIntent.putExtra("jefe_mesa", gd.getJefeMesa());
        myIntent.putExtra("croupier", gd.getCroupier());
        myIntent.putExtra("mesa", gd.getMesa());
        myIntent.putExtra("jugador", player);
        myIntent.putExtra("requiere_autorizacion", requiere_autorizacion);

        startActivity(myIntent);
    }

    private void setAvailableCompsDialog(CompAvailabilityDM resultCheckAvailableCompCliente, final JugadorDM player) {
        final Dialog dialogSeleccionarTipoComp = Utils.getNewDialog(this, R.layout.dialog_opciones, true, 600, LayoutParams.WRAP_CONTENT);

        Button comp = (Button) dialogSeleccionarTipoComp.findViewById(R.id.btnOption1);
        Button compFueraRegla = (Button) dialogSeleccionarTipoComp.findViewById(R.id.btnOption2);

        comp.setEnabled(resultCheckAvailableCompCliente.is_comp_available());

        dialogSeleccionarTipoComp.show();

        comp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCompsActivity(dialogSeleccionarTipoComp, false, player);
            }
        });

        compFueraRegla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCompsActivity(dialogSeleccionarTipoComp, true, player);
            }
        });
    }

    private void checkCompsAvailability(final PlayerView playerView, final boolean showCompTypeDialog) {
        AsyncResponse asyncResponseComps = new AsyncResponse() {
            @Override
            public void onProcessFinish(Object result) {
                CompAvailabilityDM resultCheckAvailableCompCliente = (CompAvailabilityDM) result;

                if (resultCheckAvailableCompCliente != null) {
                    playerView.playerHasComp.setVisibility((resultCheckAvailableCompCliente.is_comp_available()) ? View.VISIBLE : View.GONE);

                    for(int i=0; i<playersViewArrayList.size(); i++) {
                        PlayerView pv = playersViewArrayList.get(i);

                        if(pv == playerView) {
                            pv.getJugador().setHasAvailableComp((resultCheckAvailableCompCliente.is_comp_available()));

                            playersViewArrayList.set(i, pv);
                        }
                    }

                    if (showCompTypeDialog) {
                        setAvailableCompsDialog(resultCheckAvailableCompCliente, playerView.getJugador());
                    }
                }
                else {
                    Utils.showInformationDialog(activity, false, GlobalData.ERROR_CHECKING_IF_AVAILABLE_COMP, 1200);
                }
            }
        };

        ServicioMesas.CheckPlayerCompsAvailabilityAsyncTask cpca = new ServicioMesas.CheckPlayerCompsAvailabilityAsyncTask(Utils.getNewProgressDialog(c, getString(R.string.checking_player_comps_availability), false), asyncResponseComps);

        cpca.execute(GlobalData.ID_UNIDAD, playerView.getJugador().getId_mcc_jugador());
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        final PlayerView playerView = getPlayerViewByPosition(numero_jugador_seleccionado - 1);

        switch (menuItem.getItemId()) {
            case R.id.drawer_item_tracking:
                valuesKeyboard.showKeyboard(KeyboardTypes.TRACKING, new OnValueEnteredListener() {
                    @Override
                    public void onValueEntered(View v, String value) {
                        doTracking(playerView, value, (Button) v);
                    }
                });
                break;

            case R.id.drawer_item_money_change:
                valuesKeyboard.showKeyboard(KeyboardTypes.MONEY_CHANGE, new OnValueEnteredListener() {
                    @Override
                    public void onValueEntered(View v, String value) {
                        changeMoney(playerView, value, (Button) v);
                    }
                });
                break;

            case R.id.drawer_item_comps:
                if (playerView != null) {
//                    Logger.d("playerType= " + playerView.getJugador().getPlayerType().toString());

                    if (playerView.getJugador().getPlayerType() == PlayerTypes.ANONYMOUS) {
                        setAvailableCompsDialog(new CompAvailabilityDM(), playerView.getJugador());
                    } else {
                        checkCompsAvailability(playerView, true);
                    }
                }
                break;

            case R.id.drawer_item_client_history:
                if (playerView != null) {
                    showPlayerHistory(playerView.getJugador());
                }
                break;

            case R.id.drawer_item_enjoy_club:
                confirmationDialog.showConfirmationDialog(R.drawable.ic_enjoy_club, getString(R.string.send_request_question).replace("#_REQUEST_TYPE_#", getString(R.string.enjoy_club_text)), new OnButtonPressedListener() {
                    @Override
                    public void onButtonAcceptPressed() {
                        sendRequest(playerView, GlobalData.TIPO_SOLICITUD_CLUB);
                    }
                });
                break;

            case R.id.drawer_item_host:
                confirmationDialog.showConfirmationDialog(R.drawable.ic_host, getString(R.string.send_request_question).replace("#_REQUEST_TYPE_#", getString(R.string.host_text)), new OnButtonPressedListener() {
                    @Override
                    public void onButtonAcceptPressed() {
                        sendRequest(playerView, GlobalData.TIPO_SOLICITUD_HOST);
                    }
                });
                break;

            case R.id.drawer_item_prize:
                valuesKeyboard.showKeyboard(KeyboardTypes.PRIZE, new OnValueEnteredListener() {
                    @Override
                    public void onValueEntered(View v, String value) {
                        registerPrize(playerView, value, (Button) v);
                    }
                });
                break;
        }

        drawerLayout.closeDrawer(navigationView);

        return false;
    }

    private void setUpOnKeyListeners() {
        enterUserOnKeyListener = new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP) {
                    if (Utils.isAlfanumeric(String.valueOf(event.getDisplayLabel()))) {
                        digits += String.valueOf(event.getDisplayLabel());
                    }

                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        try {
                            String id_tarjeta = digits.substring(GlobalData.MIN_CARD_INDEX, GlobalData.MAX_CARD_INDEX); //00000000153

                            AsyncResponse getUsuarioAsyncResponse = new AsyncResponse() {
                                @Override
                                public void onProcessFinish(Object result) {
                                    final UsuarioDM user = (UsuarioDM) result;

                                    if (user != null) {
                                        AsyncResponse insertUserReliefAsyncResponse = new AsyncResponse() {
                                            @Override
                                            public void onProcessFinish(Object result) {
                                                int res = (int) result;

                                                if (res == 1) {
                                                    // Formatear el nombre del usuario recibido de la Base de Datos
                                                    user.setNombre_usuario(Utils.getOrderedUserName(user.getNombre_usuario()).toUpperCase(Utils.locale));

                                                    showUserAcceptedDialog(user.getNombre_usuario(), userType);

                                                    dialogEnterNewUser.dismiss();

                                                    if (userType.equals(GlobalData.USUARIO_JEFE_MESA)) {
                                                        gd.setJefeMesa(user);

                                                        ((Button) findViewById(R.id.jefemesaName)).setText(user.getNombre_usuario());

                                                        btnTableOptions.setEnabled(true);
                                                    }
                                                    else if (userType.equals(GlobalData.USUARIO_CROUPIER)) {
                                                        gd.setCroupier(user);

                                                        ((Button) findViewById(R.id.croupierName)).setText(user.getNombre_usuario());
                                                    }
                                                } else {
                                                    Utils.showInformationDialog(activity, false, GlobalData.ERROR_REGISTERING_RELIEF_OF_USER, 1200);
                                                }
                                            }
                                        };

                                        ServicioMesas.InsertUserReliefAsyncTask iur = new ServicioMesas.InsertUserReliefAsyncTask(Utils.getNewProgressDialog(activity, getString(R.string.registering_user_relief), false), insertUserReliefAsyncResponse);

                                        String idJefeMesa = "-1";
                                        String idCroupier = "-1";

                                        if (userType.equals(GlobalData.USUARIO_JEFE_MESA)) {
                                            idJefeMesa = user.getId_usuario();
                                        } else if (userType.equals(GlobalData.USUARIO_CROUPIER)) {
                                            idCroupier = user.getId_usuario();
                                        }

                                        iur.execute(idJefeMesa, idCroupier, gd.getMesa().getId_sesion());

                                    } else {
                                        Utils.showInformationDialog(activity, false, GlobalData.EMPTY, 1200);
                                    }

                                    digits = GlobalData.EMPTY;
                                }
                            };

                            ServicioMesas.GetUsuarioAsyncTask gu = new ServicioMesas.GetUsuarioAsyncTask(Utils.getNewProgressDialog(activity, getString(R.string.getting_user_info), false), getUsuarioAsyncResponse);

                            gu.execute(GlobalData.ID_UNIDAD, id_tarjeta, userType);
                        } catch (Exception e) {
                            e.printStackTrace();

                            digits = GlobalData.EMPTY;

                            Utils.showInformationDialog(activity, false, GlobalData.EMPTY, 1200);
                        }
                    }
                }

                return false;
            }
        };

        addPlayerOnKeyListener = new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP) {
                    if (Utils.isAlfanumeric(String.valueOf(event.getDisplayLabel()))) {
                        digits += String.valueOf(event.getDisplayLabel());
                    }

                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        try {
                            String id_tarjeta = digits.substring(1);

                            PlayerView playerView = getPlayerViewByPosition(numero_jugador_seleccionado - 1);

                            if (playerView != null) {
                                loginRegisteredPlayer(playerView, PlayersSearchCriteria.TAR, id_tarjeta);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            Utils.showInformationDialog(activity, false, GlobalData.EMPTY, 1200);
                        }

                        digits = GlobalData.EMPTY;
                    }
                }

                return false;
            }
        };
    }

    private void showUserAcceptedDialog(String userName, final String userType) {
        final Dialog dialog = Utils.getNewDialog(activity, R.layout.dialog_usuario_aceptado, false);

        ((TextView) dialog.findViewById(R.id.userName)).setText(userName);
        ((TextView) dialog.findViewById(R.id.userType)).setText(userType);

        dialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1200);
    }

    private void enterNewUser(String userType, boolean cancelable) {
        this.userType = userType;

        dialogEnterNewUser = Utils.getNewDialog(activity, R.layout.dialog_ingresar_nuevo_usuario, cancelable, 500, 250);

        TextView user = (TextView) dialogEnterNewUser.findViewById(R.id.userText);
        final ImageView card = (ImageView) dialogEnterNewUser.findViewById(R.id.card);

        user.setText(userType);

        Utils.startViewAnimation(this, card, R.anim.tarjeta_login);

        dialogEnterNewUser.setOnKeyListener(enterUserOnKeyListener);

        dialogEnterNewUser.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                digits = GlobalData.EMPTY;
                Utils.stopViewAnimation(card);
            }
        });

        dialogEnterNewUser.show();
    }

    private void loginAnonymousPlayer(final PlayerView playerView) {
        if (playerView != null) {
            AsyncResponse signInClientAsyncResponse = new AsyncResponse() {
                @Override
                public void onProcessFinish(Object result) {
                    SignInTable signInTable = (SignInTable) result;

                    if (signInTable != null) {
                        SignIn signIn = signInTable.signInClass;

                        playerView.getJugador().setId_juego(signIn.id_juego);
//                        playerView.getJugador().setId_mcc_jugador(signIn.id_cliente_mcc);
                        playerView.getJugador().setId_mcc_jugador("-1");
                        playerView.getJugador().setId_jugador(signIn.id_cliente);
                        playerView.getJugador().setId_categoria_jugador("-1");
                        playerView.getJugador().setRut_jugador("-1");
                        playerView.getJugador().setNombre_jugador(activity.getString(R.string.anonymous_text));
                        playerView.getJugador().setTimer_jugador(getString(R.string.null_timer_tracking));

                        playerView.getRootView().setOnDragListener(new MesaOnDragListener());

                        setPlayerImageClickListener(playerView);

                        playerView.populateView(playerView.getJugador(), PlayerTypes.ANONYMOUS, anonymousPlayerImageClickListener);

                        setPlayerViewInPosition(playerView, numero_jugador_seleccionado - 1);

                        showPlayerAcceptedDialog(dialogUserAccepted, playerView);
                    }
                    else {
                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_SIGN_IN_ANONYMOUS_CLIENT, 1200);
                    }
                }
            };

            ServicioMesas.SignInTableClientAsyncTask si = new ServicioMesas.SignInTableClientAsyncTask(Utils.getNewProgressDialog(activity, R.string.registering_new_player, false), signInClientAsyncResponse);

            si.execute(GlobalData.ID_UNIDAD, gd.getMesa().getId_sesion(), GlobalData.EMPTY, GlobalData.EMPTY, String.valueOf(numero_jugador_seleccionado), "-1", String.valueOf(0), GlobalData.EMPTY, String.valueOf(1));
        }
        else {
            Utils.showInformationDialog(activity, false, GlobalData.ERROR_SIGN_IN_ANONYMOUS_CLIENT, 1200);
        }
    }

    private void showDialogPlayerIsInAnotherTable(UbicacionMesa tableUbication) {
        final Dialog dialogClienteOcupado = Utils.getNewDialog(this, R.layout.dialog_cliente_ocupado, false, true, 1000, 660);

        TextView nombre = (TextView) dialogClienteOcupado.findViewById(R.id.txtNombreCliente);
        TextView casino = (TextView) dialogClienteOcupado.findViewById(R.id.txtCasino);
        TextView salon = (TextView) dialogClienteOcupado.findViewById(R.id.txtSalon);
        TextView mesa = (TextView) dialogClienteOcupado.findViewById(R.id.txtMesa);
        TextView posicion = (TextView) dialogClienteOcupado.findViewById(R.id.txtPosicion);
        ImageView foto = (ImageView) dialogClienteOcupado.findViewById(R.id.imgCliente);
        Button btnAccept = (Button) dialogClienteOcupado.findViewById(R.id.btnAccept);

        String casinoStr = getString(R.string.casino_text) + " " + tableUbication.nombre_casino.toUpperCase(Utils.locale);
        String salonStr = getString(R.string.room_text) + " " + tableUbication.nombre_salon.toUpperCase(Utils.locale);
        String mesaStr = getString(R.string.table_text) + " " + tableUbication.nombre_mesa.toUpperCase(Utils.locale);
        String posicionStr = getString(R.string.position_text) + " " + tableUbication.posicion;

        nombre.setText(Utils.getFormattedClientName(tableUbication.nombre_cliente));
        casino.setText(casinoStr);
        salon.setText(salonStr);
        mesa.setText(mesaStr);
        posicion.setText(posicionStr);

        if (!tableUbication.foto.isEmpty())
            foto.setImageBitmap(Utils.getBitmapFromBase64(tableUbication.foto));

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogClienteOcupado.dismiss();
            }
        });

        dialogClienteOcupado.show();
    }

    private void showPlayerAcceptedDialog(final Dialog dialog, PlayerView playerView) {
        TextView playerName = (TextView) dialog.findViewById(R.id.acceptedPlayerName);
        ImageView playerImage = (ImageView) dialog.findViewById(R.id.img);

        playerName.setText(playerView.getJugador().getNombre_jugador());

        if (playerView.getJugador().getPlayerType() == PlayerTypes.REGISTERED) {
            String playerImageStr = playerView.getJugador().getFoto_jugador_b64();

            if (!playerImageStr.isEmpty()) {
                playerImage.setScaleType(ImageView.ScaleType.MATRIX);
                playerImage.setImageBitmap(Utils.getBitmapFromBase64(playerImageStr));
            } else playerImage.setImageResource(R.drawable.img_player_no_picture_dialog);
        } else if (playerView.getJugador().getPlayerType() == PlayerTypes.ANONYMOUS) {
            playerImage.setImageResource(R.drawable.img_anonymous_dialog);
        }

        dialogAddUser.dismiss();

        dialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1200);
    }

    // TODO: DESHACER
    @SuppressWarnings("unused")
//    private void loginDummyClient(PlayerView playerView, String id) {
//        String fotoMatias = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCACVAIwDASIAAhEBAxEB/8QAGwAAAgMBAQEAAAAAAAAAAAAABAUCAwYAAQf/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQMCBAX/2gAMAwEAAhADEAAAAdpSWFSM7KvJll9JE9sIUD40Rn8+hqt1b8+Fa+0zyzeDsHI9efGydvsuVt66aQDN1/RClpmdfCteV0PzybB0aYS1dVlXcTQv0L5H9ThAiNUEq3UDtMY/LMJ0cAMOvnF7DyhLM5HYZZagq1gdaeZ/TrMNZvMzuZwCdVMkUm97szgbIfz+it9nxL423JmPRi/519EyG2rQwErt0yQN5VabZY1zze+B59Z0mZztmXrq1BvLUgUiwbNS8RWmkVsM5XI9yivoH+nwn1mTTrxe535YUXlillSxqYZcNqAqjHdMvp2UzsLYtNSFbDQy5ZrPeYciZpapI+PbwVUJvJ9YPlF9Qzbf5zqYYtlPQT9pkHR4kLmCjtBgtF2RuagJwmitnTGqch81MrMsSN0Kse6pqE65D9hKLVds6wtvFvRO0fwGW/8AmurwtZbTXyv5lTOrsIjECD9nC/R5GzhR88mHlldwQptHAoteUL6joPnW/wDPv8Vq7u2MBe7Tlb3Ml73Crs7g9l3BUP3DtJ7hF/Uu6b//xAAqEAACAgEDAwMEAgMAAAAAAAABAgADBAUREhATISIxQQYUIzIkMyAlQv/aAAgBAQABBQLMUieG6CFAZQx5lRCktIrV9UAlmqiY2dXY4SHgktt3gr5TYCVbdtlb7u1O3aCLFgMxfVdlb7i5lmr6gb35MRYlkrY8tKy3fH4s0ChYT0x/6mG8YGuWVcQbByZvGnj0ZH9mt5PaxsXHNxrx1EemZlBRtEvK5W/QxrglmGjrVHUOtiMr2IjENsMMbY95/N9QH+UGeqjv2q7ZXCtLVylxuVeYhJVmCj12SupMdt5j3izrdSeN6uoQbI1z93OTvapm1uGoreyzXERJjbCzHP8AsXssmMv5lflVw7pjpxmNlb9bK1sBrYR63VqV/kOrWrW9eLZqDrlJi4ZY6UvHKyD3Lq8duVdaoOntO3u1WQ9MpvS3qRvMuoUZHdVUyyrNgUExvxP9P1dykKF623JUtuqtyhEMZJpz2MLLkrIII19gDZvZD4NNz12MTc2mY/22JLcmqqX6pvHue6/tw7joRCPOENqNV2Z1NlZ1HJa5ksHJuDtZ6JoyjtNq1zCzLutnkwLErnbgZqnW5LDsZZt26PFGZ6rr7EqNh3vsGxDmIS5xCj6PxigkrXFWKIWVY68najle1tlKPn1hLNaqUZOpXWmy9mtLeLPMC+QONWkMBpZHhB+R760jZ20fKsYoOQ2uV8T8lmo51bBrC0Z/BPg+9T+PgCftMbKNWRc4VKcpu9eysWVuFg4S5qi3tNYyeEc+o9G6b7Gt5sBLrov7VW+jEUNl1swx7bH2rPqr/U5iKmRabrHMPv8AJnzVe1M7ic2vZuiypoG2ieus4WRaadIuZl0rHA1TKFznwW6fPQ9BBFg/beJYVOFkG+n0mbRjD7wz56fPETjB0/6B8KZo+SK2+4UE5Sw9DD7iCAeXEXoOnys3lR2mlLTk0fb19Pk9BB0MXoOhixfJBmiZf29nebqfb4EH+I6GDosrOx0y+y/G/8QAIBEAAgMAAwACAwAAAAAAAAAAAAECEBESITEgIjBAQf/aAAgBAwEBPwF9EZbW3tIa0cceka8OmOkjBrRLCEf6cexx+xL28HGk8N0TJPusExs9Y/abOJlLGNmukSfZqNNH8tNRyQ3v6MFyX4Ys/8QAIBEAAwEAAQUBAQEAAAAAAAAAAAECEQMQEiAhMUFRMP/aAAgBAgEBPwFLSpwwa9kx/RwjMMJE8JaouO1i+iKKPp8I5P6JlU2Sfh+DFrEsNI5RVouldNHRUYTOlPJOFN+2aWzuHRrPaJ4t+i45EVI59CligUnFPd78N6OEzGvppMqVi8X05Fq8F5c09tD/AMOb4f/EADYQAAEDAgQDBQQKAwAAAAAAAAEAAhESIQMQMVEiMkETIGFxkSMwgaEEM0BCUmKxwfDxctHh/9oACAEBAAY/AgcPnmB+6MCCNW7Zy3VBpGZc6wC4WqOqh1nHK6gBcWmy0QTahwtbYqrDPSm/XwVTMydghC4vmqGcg+aEInItLpLSr2Vsxk6SKj80Dh8Lunj4KD/eTnblfBUDmxFJ0zqHRBvR/caw/WO9AvacxM5Q5A6tTrXCuam9N02E5Yf+CaxnMblCpSeqI+8mAahyE6qXGAuDhG519E2lt3mKjrov+KDzZ+y12KaHG4sR/PJADosSBVfTRAdNFKaNVgho6IOwzdSZgOJsgAKZ319EXOmRGqb43TS6QAZGUsVOJrvnDxK4Ch2irPNfK93Jr29LKoaBfS8Qjla6D4ysKlvnCJNgYQjuEg8Sg3Csb7Z3RA/gVyqmBF502XDykLF7US2f7/ZWEZziOheyY2n82d1wlO7QmBomh7g2dFIMhYA63UArnMps+qLBtqmtPMbnLjeJ2UYLfiUQ8lzgB3vNMB6LgcfgUG4t3MEznAWP9Ifhh1HLO/8AIRpa1q4sQ5XVhk1uADRp2Lt/NQJDhq1wgheCZumeSKgniPRE75hDs9OzWmV8+JwCodygcY/f9U6ppfSGw+YdqdChxsxmROxTO0ljgOQhezY4+aJmnyQc8zG6nxzcfgsdnUf6yC1uuBvqub0U9o1vmqvrJ6jWNk4zS48VJ6HxQbgMpdcu8Cpdr3x+EJzQeBzYKkK4kAKqoSUX0vo/FTZMJc3j6AyQvZVx+Yrqz9EMOBURNXgp75kyobkKi60ixVsJlJNqrwoEAHWyayo07K+Rc7onPOp7xjR2o3XaRT+VW0zIypaCXeC4cJ1t7KcRzWD1UYmK6rzAVOHyDrvmPdy0wUHOfDtDZfferYQ98cjhu0K6LX5d+/uqiwVixXIPsL2u0fC+qxPT7DKqe8yDC//EACcQAQACAgIBBAIDAQEBAAAAAAEAESExQVFhEHGBkaGxwdHw4SDx/9oACAEBAAE/IXngrHXkdYfkhkaY23/UK6IjomsD95j0vjiX9GYHVfErz2mLBccLAlA/GYM4l8MF4H4hDU8E0sDwbZsEIaAJdhNQCZ8jy+fz9xtuBRYJblw7r6hV8cjseoMpI/8AUXHr8bgvA/zcudXS7QjVJRh3lNVG6UBW6/1zkP2Z5x3K625i5mXguCaZ4EJdPj54mKHgcDbvH6mkBclNnkMpwn+4Yi/17ysv1FdG5x+9cyvFMcTA4G4+GqWk4gyJ53foWZEMfpL+ReviJr8JweD0r1ZNQ68eeH/dy1SYUTDyPjF5iuKXSqfz93DvrHN9y2iLQuC35ZixCzrxC6bIg+wlmrpqHo0xQNciLTdjBuDua8t69xtqA4NlkW38Stu32tBSk/b0SynUa7HTRKzzFTe8wWqAI56Am2hfDp0+YjvMoo62RRdB1UtLZc+05VqJLs61ZeJdvGUtxcwPeFZH+EyY8HdLc+0DSGn2bisLEaVz/cCijBFV2Nw3eA1/b1ohPmZn42UorAPlzCdrJnzKK17wzjwENDs945YKL8wb6DdCp+lgI7FDxH0HzGIKC0xb62wkMvlIt8azDVd2/UBQH3lbxlQ92I2VEBocbm4iQVMy1ZEvIvjfK/8AEFUQ8HrVofPM1I+z9xLnVM9bnJ8Us0UFonM5uYaEmkdx2bH8ZiUJWusdwwd3w5mBi6flKHV9m/8AKly7qDhl+pQaH+dRVEFr2uPxBpmiFZYvn0M7ub7ksy2ELfv/API+T3V4Jn+s6VdYlariUOswFEHkSUjoHxLWuvKUBTpq0mOaVVGD6IQt2fYjXgIY5W5u8TTYvIvZxrEb/wA8I4b4AVL4vvmYTu33A2c1QQrpFRrjBPzGaoHuBVsUCF5g5rL95lgxFJgHaEaCZNcRKkulhPy+qnsPzlMQGkC3+wK0QIHyM0nvp07rMCjWLV8U5h0VPAgGBZRi6ZVy48yCC/Sdt6fMHCZrPDQ/TA+k/IRFFI2BOI+8NlZZVYQbnCq/9RQsFYoBa/BIgZOwaq1TdK3CSeQW1dPxCrFLuMHuLk7ZkvaKodahmD3DDsW+WYTiPju/eFUMmJdtmeK1tggeFbgSsHRc+/kmmgDUJ58ziN5sXLWvFQDudmYxF8d8nB9eiPZ5i38RYI7qCuOdwxBcMFogdczxnFEbZviZlAQsKs3Elu2UjO/n5m1J6b+9y5KPfEyOhOy4/qVukzEKyXXUyR2PMuHbUYZxpUxpNNcmmh8TG4wOc7nOUK6Y96dS5ZawLfqVL2z/AClwJMhkfBj8wWryT8DMowH2ET4Z3It/KE7Y8zS4Z3FibRZiqTFhZAZEhkCaAZe4Ft+1kl+j5ZZM4cbmHoHM5YQoXUp2w1yw38Th6Jmm0TsemXhf2/qFcGHHfoWXj0BZkMaGZEdzacsdIsMKHliIeZl8O/fmAmPpjHb0Ll9/SsTmZE2lTaczaKhm46JcwTllpsS/7l+nlzF3Hk/+AbfQ3D0cvtFNIOamBEoMKh6Gs0Hif//aAAwDAQACAAMAAAAQcto6JLyS5zLZdkS3IUmhHwHfuk7mkGRoMXWkJcT5vT2l85FhpaQFlURVWz9g7iDQxKf1PEQkHKE8/wDHe44wfgA3gPPP/8QAGhEBAQEBAQEBAAAAAAAAAAAAAQARIRAxQf/aAAgBAwEBPxDS2A8h8OvyxHZj2eNmlwcnvb6xnGQcIY55h2UQfUerVU3Wn5aDI6rAtkPi8t0Lm/L9ItZdY1b2KR1kALUZUgQYwsP2XeeHzsxgwH5cJdqCONllm/bpyYRGG+CrWJ83zJ5Y/hZZNtnr7B4+kzDf/8QAHBEBAQEBAAMBAQAAAAAAAAAAAQARITFBURBh/9oACAECAQE/ECM7nbZYkDYwydsj6l2d6QePbh3UP28JGkCuQEB5EHSb1juvy8RfNs8h/KAXEhxiOy9WcuDPtw7JOTFs5MH+WWRz4IAY3wlz+pSd+RDeNgetgeJvUq/sqN6weolrwWReCykxPONpyJIH4rYiyHIut7LfSy/Pzwufj+ZKgHG4C9/hHifxgJ2//8QAJhABAAICAgEDBQEBAQAAAAAAAREhADFBYVFxgZGhscHR8OHxEP/aAAgBAQABPxCHcowkiRdcSiSS5QT1sEHxX0QjpLyH9WN+RrGIoXivVhOasiYBcORtLoIE+mESBSHh84phYwxGv8w5TyoGYY0fvOQB3S9cz9byRp4VUC8++OWCdHh3gFnRqFfbBCJEAH1Y/wCY/ToH3echAgAFaMk/gaOGMWQrTaJMOWqBnet+ehNV2JWC6DjtJ0KBUodP9ZeObn7ZYz36ZMpUZ6o/A4gXIKGpmCnjTh8SBwr/AB74AE6OLkXiOD5wILAN+7zlQg28+uGSwEJSEXfngxO2d6cEF3EoNAEFYLmDtuTNGl8m19cgQEYVx29ZYOYGY7cQFbQ+uCaIRE2YMpCabGJx4AvZ8ZJsWtC5CdgNIQvbolpAAkwNkM8IWhhQKRJrGneInwP9wwQ3EfWcE6KLOIgZJdzHhMQrxvJfgxclDZnTpyiAFCPi8V1BgJSpvTrW/PcryczwEeqn5c4Hxk+3LQvW4/564Rw+bBJiO1q9BQkUMaUACKK9bZvDj0xYN0bNI09Mm8jIggtcg72enLGpVGyYIEtUQTksrJzITomWUQDKSi2pMgsIkCWZmEn5BxKJCDcWERiGEQE6W/6RjKE1vLUvuZJSKWWN/wC4SKfUiv3gDACySjP63kFcQmTKP2wobhdz1/fGExVqR/NUbeMCIIkLBCeAgJRftikCOUE1W7ddTZ4EjWzkhWsbodxo9P6/8BAFVCZBpAuwZHfBs9HqMFRWxJii+IgmrHEkEonMEYRAhOaIKYKLgMKuXVucQCLQpMj5+Zw0lKDREV/TiGdhjUt3vnFYEWwWc9sOQEkG0tL58+2ucISdqgSRB6B+sAsKyA4FoYUzS8cbRArJRiVIrZ1oq7VDVJEvS46rCgPEaAXyEKt3hEIHBkwlqFQ+a0/RxrFQRiW5Bx66fuak/wDLL7JCYfI8YKqFIkj/ADCtplksDadO8cugK7S99u/nDlYYEfT85JVbKuwfOm4xZJOHkhJ+Y9MkP88rLr5jxguVEdEYjnsVPWSAiZBZTuLecgTCAngdeveIACBJY68Zzk4qqYjClKRUEsV449bIweEPph07PtgoWu+NOPPLX/nnvIEV2AvAb5ALKxea587w0IUYWGYMZv8AnIQeVMHKPsY57AIlACbjXni+ejh1PAIZHJwaPnCPhSDOMd4y4/NrRQW74MCwSU3b5oBxV4YqOsvnbGCDBzGsadLy/H8ZI6mxZuWdvGNGFNJDd6Ik3G8B9cyQOo3kVEyXFddvrkNwgzrrzjM0VUg9jI4OrYHV94NYd4Ww0jzR6zkSiGNb0b0wEAWe+I6MFFOSWR4SnvjqbfGpJ4VM6ZTyZeKCyCuNBbq6iMIKTw5CmTp/fPpTihNKvjCiojCQJDb6P/HHBsyrclH4cKELQcQIwYtFlKJ2c7eOcj0UoEbQFWA3PirwCaH2yokkEveLkZCEz2OFcbaFhjLlcITbGGeidB92F9skk2R5juAfW82Ge5W8nEjBLsQFi0l9+Wv3k4ndDEmjtUgZeJYpEGl0lKMCCGdHr1PGMEzILZ8ayf0NPBL4QFakHeCwsPufnEKg9ChgjtwRhvTLKk9E8vtOGk7H5YMdUY5NL3ZihLTsrJ4jq3Xfpg0CsAksQOWS5ZlyIBoLjAEYe/BlQJT3h0C9s4YiF+d5yyGAMZERBkGWYGhQYQy3iFBYEIACCZ2QCQVZSchJMkZZARLIwLax2/I0dqBUSz3zhooAEMBHc/THOxQGYOVb8HjE20hCgXl7HrAH1vGuuU3/AMjKmb/oxUh5jBEUYJu1H+/rLeBjJAkvh+hgBX8MCLzT4xK7JbT71mpEnas+xr5wjfISKXVbLjmsbrR5XRdIi/pjuEYyYpDFDciTAZcBrkIkiJCRhWXzj6NbASXe0kgd8zhPdQlWe+dZGK9sY5vk1ibboe5PxxiFNJVMJ/h84Q4hPWZ/nnJVIi8aAkmhBAavfnnG6ymXSnh4c0sRM5IDAodkbPORcIIgC5QVS4EQsxVWGwuO4vjIKGnSgMuiw1tj1RKppzzcYBDMMNTWD4MkeSBQC9enzGISC+3nzweqGLAdoxhhJsVC3EFTwpJ9sGSiBB6YdIpV3kJReqwSkkoJWm/plTo0XTV/TeSFKz/O9VkiYzaEXXVJ64gE+0CBvriD65OSmC+sp64yI2ZSgCPCilSEdZKh4FVIExqCBkphVHT8xL3VYEcSDveUA6ROCyyGpneJqJJFuvHzrK2JX8R+MF8yGC6ONvXpjX1C4WaOY1B4xpNxre8GBw+oxlxfOQKL+MbNqILvv+/jGIkAaQJD0yia88YjNyS1rbyDkOJgqIoYKgKI9d4wly99ehkonOoPr8ZMji9MuyKq/tjdUlPaRVhcSnGATUWx4iKoPpgyJtDCnsPcPxhe/MEss7CPnGDTgBCHLyHjq61gZmVW9XkE6NeMgdIfJOQ2xfQyEyWYK422Y5EkhwFkPxkEDUVuvxmievBkEF4Pz+sQQnzky3f+by0Lboid8YzfU0FDTemdbnALBhkA8aYI6wAOviH6DjSlcYg6j941wmH+4wkvEz98EU+dZYnPJMRrwy/5jC/LyZFgHpbwFoHvjJ0vBzvJx0EPXGNXGaLfHzlC9bvIh7Wgh4VqI+vvlCVDNjfDInISgjhflMTcEzm6XiX65GGUjfyZUMzhS/THGBgyOQiBDu/8yyDeKCVk/WEmVgoPPW8BqOCMuxxe8ZGHRiIMnR+cBKwZmYvznHwwrAJCdM+kzFVg4h+j9Yltj4xvJCEdXv64+I5l7xlxLl9/1kpucufXBZxGjCQvJPvihW4yCI6wUPLXP9sx2nxrvEHY6zgMfe8UjQhFb/qxYCj5xOfGq1DbEQvfAk+djV8bzU6oxJ6B989ET+cSB7YIH2xURjkqCMbF9Mo484GQlwyzyf5goU8MSa7+2Cx5cSAoymlzk1EgR85BxljVYZSt7z//2Q==";
//        String fotoFelipe = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCACVAIwDASIAAhEBAxEB/8QAGwAAAQUBAQAAAAAAAAAAAAAABQECAwQGAAf/xAAZAQADAQEBAAAAAAAAAAAAAAABAgMABAX/2gAMAwEAAhADEAAAAfT8prkTY7WDCCEY+w1SD842GFpoWFnUM/ovkO5ltDUe8Bt22YbRvWJ1Cn/MTHPXbxx2LyGvvop8sUrkzQ2OGx1dN1jN7JdJeXjHkqC1Y6AJ3VOAozQcXaz0rzn0C/Ml7z+pRNpgiUFKULCgq2MGwRSI19Rb8JVSKz0nFzYyuOqjrPJ3O3WE2lIDIY603IDLDX2cH2qXozm9K8q2UNuWZyOONqMs1mww2Yr5PL1CVZvRvPt6Bmcyaz9FmtTaRtkA2lpWYaRfbU7G3WlpzkIqkm1pKzuZ2hjKxYAeap2azXqMdlzFwqI65gXut64+MpSwl0VAhSLkj4adtQjF8/f8l9m4bxTdBMlRszfU5og5SaqimEKxNZCcIM9nuGZVkhOYUEGQPE/RfOH8lPWLPnfo8LNRO9PlsxSRAska0hrpkGtsSAGKvPVbUj+VPsPH+VvLrHr/AJX7CxFsbV6AcRIlKwQsIJrVXa1CxuyQLUxoaADWYYhvdyHbbrupho/usLTu7ZtLuI0I3uDSTd2FBe4kTk+6Q//EACcQAAICAgEEAQQDAQAAAAAAAAECAwQAERIFEyEiEAYUIzEkMzQy/9oACAEBAAEFAmRWW5X7UgVe/JoRoVbNe3UrP20c8xmlOsLZ0y0YZI2EmGQafbqCIzFHJJMqBc18EbFqu2ue0eOOQ9tlH1E54ojykU8kraX/AIPTvevrykTvkFJVPwzoppXxJ8sgbJIW5F2VvqUDv1IQtc8Rk86LknGVfp+FZqCVkGDx8hgwvVWkmIKmnfaIxSpKvz9VRfiaWTs1CWktxkTVVJP0z61/iSwiY0kk2Vk7a4XDBk8q7o0LcoRbhLg7HV4hN037USQVa/be5GrSIojzo0qR3XtjC8suJXGBfGtfBTWeQR5Nf/PfX+WjSIWtTmGs5EFhmGc2MgO86egewkYGKuKPh5FXN4cbF/db/NZQGfsjBENyRdt7TnmS+4SUbpo3V1hdVx7qLitYmwPD3sE0iNBIJgPDUzurftCGwvUI8HUIeU12GSIn8rjUvP8AJ0/qBUmW1LnaXaSJCIlM+JCqtJ/1JmjMYfMdH/L17YsueD+xyAFntrvGZsiXZ4H7sKkjRQohV1KxBu0LHMPK27MbR5DJFquPFH/L9RsfvO/KX6ej2pZ4BDVqjnBJD5jTWQgfcxydxB6Zy/KrZy5Hee2paFOVz09wlRSkPX67ySmrPx6VUkqtdcca3qr5rJIz2+n2DDCJUkYjRLDlDLuR5pNhAM980MrnayfvmO3r897lziTjjx+VQKJH+6nSNWVVGAtyOgkXhkX1S77Lc9hdXKR2rE5W840nJnjaTHewg8jJ15LTg4uo3hcYqncr7RdFxphExR6Xbs1rkEfYpeEnPpV/sR2QyxjB7YI9xKujCCGj9QI/xgkq4YGu/wCRUdsGfTtrcJ6hHOa/jLDccrephAcedMPHHWRgEuOMZUZ55yaOMScpaFnSaJyM8TTsmGxC/PLX7r+JIPQnZi48WXXGAFl/6Zj+Ofxk2hjAkI/89BLr4Q5UUx07muUDakT+4HVeY+7ecI4rsfcJkTYw2pblhk1ZRjx+Omxd+6raW/8A0wP7s4+7LezncUre0QPFfMv7xj5kOh+1nG5EsLxw59MxqXJ5La126H9hP8re5ohsN7mP+0HJW4mQCGF/aQeMfxPN6yf/xAAiEQACAgICAgIDAAAAAAAAAAAAAQIREBIDMRMhIHEwQVH/2gAIAQMBAT8BPo2OzU6xeJcf8xZBl+rJ9XhQbE9GNHKjxnFDuxNJako3E1USUseRM5RSVGxZfo+xtfoskPoVmw0OlEbZsUVi6Lti9ol1lOiFTJQpDKItok7fwhPV2Pk2j8FlfhWXmOV2f//EACIRAAICAQQDAAMAAAAAAAAAAAABAhEDEBIhMRMgUSIyQf/aAAgBAgEBPwFqyPHZtIIoktFE6MeT6cMcPhFUN2f2iqJZEh/muRGOTPMQluJE5Vyb3IjAfB42iA4uzFuiztGRXpGP0lGkR60SNtuihJ2bEx4ycmuC60xpyIqiiuSlpkxpmSPj5IysxRqOjSZHhaIkZMe+O0jh2zSEuPaSOiKF1okUVox9ERejGZP1P//EAD4QAAEDAgIFCQYEBQUBAAAAAAEAAhESIQMxECJBUbETYWJxgZHB0fAyQlJyoeEEICOyM4KDksIkU6LS8fL/2gAIAQEABj8CgiyaWiWLDO7JOMbFqrZCBaBJ3ovfpvkVqhGgVxnGXetftAyTXCSQmvFmjPnWSy0QVVg3ImyM2KbWNbYctis625wlYTYF9xVslfS0kmBs2K91DG9qDsU1O+mnWcAgzFs7fv03THMdkZQtZYVO5N5xoujTnmnZ1VqTc/kkXVTNoV7KnEl2HwU4ZkfkwsYRY0lNAdS0NUVEohCoLGwvgfp3lQMkRM6IcpahQS129MJzIRaX0uG9WWO07G1d10w9FSdP4sOObWFaoXMta6t+bD+ULEhS0uHUn4ZeSHCLpg5kaIlDW69GPv1VYfkufyFYfyp2jNEAGneqQLcyiwRBTTvnjouVbNWbQ3e5PA/XeAJM6oV/og0xjAiZbmvaFdMkZJyw+pQWzaVdpWTkW607LaJLlZN/DwMtUrVYQOey/wBR+Iv8LVODhhgz5TE8keV5TEO5wpH9vrJXpO619GFGdCLnNGJ+k0kzcZptD3h20HJYaa6l1FGYHOqZWbf7ghcT1rlGZHThkcwUYxfULWNvV1eAzoiO9F76C+8RuR5bENRGs8bEx2FhbLVWgL+NT1QsOtpGrCIroNLYtmdqHV4piY3ZyY4lMxKiXtyJPasaXtiBVqzOYU4QZSRRYX7/AO5On4raQcqdY9l01zdZpgzvmyp9647cwifcdwhfzeAUzq7lqsUOYHDmVVPJO5rIDCe10ZTZBrswsJ2GwuMQYCA5DEn5SnYmKQxzwAG7k3k/ljhxRGnGgH2R4oYeLVSQaTulPfhm0g+f0TgM+EZcUaRMjeovaFqMLh0Vqkhe65fwyPlToJN9qGtCfS6/s1eutDrv67Qm4hJpdIj4YQvOipx1QuSwG04dpO03TJHstYqQIBlvfdSfas49tkQdhieCN5HjtQluET08wqZYXbkQQRbrVynmfeKEb7og7fIeSaI1i1rvr9k97nExWQnCsQOiE4ucDeEGjeJ71WPWfmnDeC0dlvFF2yzu/wCwRa6JfV64pxp3OPrsT2hoNhzzn4IEs5Xc6Ag5uYusPFoGsNycQ0Aoq7ZBsgPg1eK1drGgf8kQOdnfdEnI60b9yjcOCd/T/csY7NX95QO5zh3yfJNBzLXM8FU7mf2J43yO+/mqdjmgjcpY6neKZg6H4J9y47VyTWulEKRm0euCDG/F4LC7P2lCdoa6ec2UNOsZ+hsrZVAz1281q+zqfuT4+Ge4uX9QcAEL5PI758wo92pzPHyVXvOAjx4rEaZpI+socuAX76CVbQ3EzIVdyOYJmRvfZKA21D9iwew/Qqd2G08VY3Dons+6o2azT67FXa8Ej+Yot2cmR2kpzuhXxPksWkQRDh6/lTzss4euxFozkt6punWlhw+2FPtztqjSFhTI1e9NidXf65lU7Ome4X4rCadjW8HIt28l4fdOduLT9fstb4v8funU/wC3PHzWJ8Nfh9lhNNpBaeyPIrBJ2sp4fdYbNtJZ67ip3gHxKkNiAB9Sv0nAN3EZacJhym/VtRds20+vUprRlMA82XFN53Ud6dzAf5KDtBbwCd0mA8SsQ9IH13IA5uDuIVZ2swz3lTuxI7D/AOqM/wBSI65/7Lp18f8A6VI6TETGbfXitY369OLiEawho+vknEi4PfB+yNrWPeVhc4B4rGHV+4rD+Z3FNByOHHBX24crD7QsTo4WH4rGp90g8PJYxHtMLYPd5LFJ3tKnpTw81PRH+SIX/8QAJxABAAIBAwMEAwEBAQAAAAAAAQARITFBUWFxgZGhsdEQwfDh8SD/2gAIAQEAAT8hZGV0l5OAXht+5jHN7O21dprMpNBlhdoa16S1ODjMRSts0Jm0WKy7xitWaIOBGCjdZekemEtaHw36wL2FaAG/VcENTTOA7cTXQxbZ+9oVgd5Th+AYLGYSAoe9yoNJEywOJV5G0F7thh/JmGaA28l14isNQmLd4C0OI2G0ytYlOq2seZjnw0OKlzYzqjtD22eJp+KQnwsQo7e3/X5LQaznyw74qGKr3bWkLlOd+q/uMBhsZc2uBI48zBhe8YnBg7aEfp70AKADp+LolwQck0HUX3lWKpj+grmKBf8AxOmUTkzk+GEqPHfEvwDzH4Z5e0CCpnL3V7n+fm4z0CI6HDaIrZd/ihFkMeXHmdMgGqjp7Cr4hzEomOjzpABQjvN4QD/HEo8248RRoK0ljcsHwSkPU44sfkgcXdWM4/Qly2rgldABN78NoY2LuqnGn8TiOVMWPtL0+tYZw1GV2TQFVINohvtF+oNxoymF3HnYWTXefsGNX4CFYy2jMcpXpDl/Cfpoqur9Q2JeFtI2+KIx2laxpbyjlTpRSB3n9OZfjpoTCq+Gs/ni6TH6/Jyr6eLgtG6t2PEcWo9IBbfaUUrhY+i9Zinj8KI2WFr/AAzYFOOD7hnC9w19Y+V1j0gwABMJ+uuas5wzcCVKigXOdfGr6RyWluPoM6doUni4w22PZnW4eYOzAdDpW36xA3rMxVgNZjNQcgdSBtC4W8C1l9NveeyfmC7AzwDI+U9SOlCjWifMM1USMlW0FZeM+SKdcxLWGifA9YgbsQpk3WM7DtLoUJgejuvP9Uw57hBbM+CIUWQaKfG/zAyeB0bU46pWmPiI4WwHyO0qiF1ZqhzKrdALOt4f7Kl5Hyi9z5gwPcfxoRRVF3KN/syjQlCUKsOHMU2aDYHdtnT3SiHReEpxCKjEY1obq2/wYWSEByPwZiLa7up+C/WUbIvQcMfL8w8lNlva/cpmTTQbvrvBbAdWOZT16O0yAGn/AB0nH5Bou5vZY1LF+2bVf9g9UMD0pb3jGhXXrgrXeyWe5sr58HVFgnXPXaC3aHCJa9ifKc+PeY2jCfwYY7F2lK21dHvLvDWw5IT7ZjYQs4PPgjOzFY6N5pzfPrvPKqyPiAenuPSULGjriUtquVDokMuNie81jUbajALVJ6JhWjy7wyKXNsJ17wnfWz1uVbIspajFyaLBoL4gkSIey/VwzkJNv5KhFUWd1s4CagamnX4CRjqAFbb6dOfEs5p2ycTka0OfQmPOBMnLtNyBylQRAig1AXwoK4Lz7QJcWhPFTFJa3839hKm5DXBnYmzR6uxQvzNooey8TiJ6qn3EsqGwOMn5HpKRfOYCr5XpKmFgcA9LVPaNiVA1t39BRJW8Zfru3owLM7KY4zUYBHA65i5yhy0fe4ImapDrMV1hdNR1q77fUG4qiTTEgEi2jofuQYzFJjcWfQlG1oKNyp8CeAGDd0HnMPlX8kyjskmij98pBU3sTon2mkzPkAPsvqRrDTA4QQLVynytx/niDKcsI5mv9vvNfSItu5a7bPX5iI1tW1t/yFsa3MfGqs3d/wColdFakbwOdWEPqfth1R7+PSGp2EZ7b+4mVcqDoGfylBtRw6XiCWd3qPmoQTFAeG54Ui4hG28h4iMtqwrhuBBkUTXN/wCMVNah7D7GeamwR1XDnHxKNEWwUYIAZvvZMiyQp1JRHILTgNfeZun2R83CVdyqeA4LPsL5Iumjsy/v75SFhtri2n0UYbQx0RKmd9aDYfZHem9jfzEKo8QNbe2aOxp8Vr7e+WKYLoFveiEO1Z03P8iLOa/6te82/GWM2KjT0XVc6+kCDYZ43/fMwBtq2UgU7oYWNweggRTjUOkZpYGdfmvzFl3L+O4gTaIFPRL7iMLjyxyH8aywpZHZxkPpLPVZvTQhUpg7eD+wIFYpPan9TuWrNp0/Ajhdzbpz1aDAbN66gVb3dGHuY6vXV6I/pD1M5Mb7gMDYAXrLftfw3WGo290DOpUZOBGAVDIXyg/uLWhh7HVz7ZThcnZ1PZGLuYdAQjkb+V+iEQHIdsfuGGqmMVh+F3rsi7X/AHeBmCW9hnvWFUDAzjWPUJmLuPe/pL8y638nPeHrl9TUBaen3YrT7On7h9XxZ+pYPlni/wCpioAogK9oJ/eEW7vmH1NcGQ+SD9obBuNcH8Ji0//aAAwDAQACAAMAAAAQeTXhvzNscRKU3VECVNGqXLYhyPecKefahkkiSGzoSHlkJ2JjQqQ8r3hlk27vIuLyWaYJDaaZ37xJlNlSkCjhDB9dcf8A/8QAHREBAQEBAAMBAQEAAAAAAAAAAQARIRAxQVEgYf/aAAgBAwEBPxAk+zj2WfMbv92L7kHq633nRj9xAqQuUXLlW7jwu+difcIE86SAJtkWRHQS7GvWPb93eZYBZOFkXfE4e0J0QlmbscxLEINbTGGCFyF6gZshbqdlmF0kxxhkWy+zt1CQaE9fBW6VngdL9JErPv8Ag+f235Pvwx4GXXwXxMwHd/L7491//8QAHBEBAQEBAQADAQAAAAAAAAAAAQARITEQQVFh/9oACAECAQE/EMXPbSS9+Nif2UGtt1jnkj7YC+m6MP8AE3N5CQYB4uAdYc+J54wNjrEheke6W2eNbYs8jeu5W0uT6/IehLwQP0SPqDhF4nN2BCCQhBh8SM3IM0kNQ/EspmcITz2wOQGAGnsE72QAdLDZ/wCwg+QHCA37bA5DGCmAeQfDUnsd7HLZPqOLbrHqRfgcMTLOHweL735E+3IfDuf/xAAmEAEBAAICAgICAwEBAQEAAAABEQAhMUFRcWGBkaGxwfDREOHx/9oACAEBAAE/EAFxEcC6ARLsI/GmVRqrZkkHnRYLDrowqOPEwFgecUULSR27HY3qXGmE1RuqRvElpv63kMVroA519dz3jZCaan6+sEyU9P8A3FogHvHbIAlhRp84/wAmAchuwPvElpIGuO1prSKutYVIFQgSIflO9VdcVzop0RWdDYAAB/OOc2Qcwn7VucZGMe425/8AmYPEw4Rojw4OzUctDBddfHvHdMd+GYjmMYdmUui64vS7xogBVE/Kdg2vfPOOxrTCs2ITd7T3jtj/ANV84V3UaPOsYNxFmGUu33joWlOx8bYt7euJklAAWAErQ4NnB1rKumug3Hf2GsEJxExNt8nfeACGjg1nd5wk8lBinn+fxiQzTiT58L8P4w38nWPz+8AlpKc4sR6rOoeOHxgd/wAiCQX8mGNAbut/m14IwAktpcTqroGQaXo3cnerFwOrjTUj6XrzzcFo2R4b3xgsQ6x+cRRQDzloypTs5ytEcryKX8BiAx6T9nxzvHpkUytbG16Iw8PWbfZGOz2cme5n5/8AKiShyR9hPucSR7io3/WHeKJTXziAYKbKdPzrOByEOvjnxiEqqs6YGv8Aef8AxebJhIERy6PbkvqcgQe/PB8YkZKvi+Px8YROP3kiC88nyPORQsD1BeZyc/7lCKcUB7N3AicTlWv5ucT2P7Dl25N94eVqBonk+MWWaQsdE8XT05txUxOqkV+YYoSdWoj195WwEGbian8ZcAK+DKh4MUY4Hd0G/fT6t+nfwG0xEUPo/g/7hwdbOC/7vWAXjMJguajzOsp2J94kLQ8mNhKAEIbd+bxdzxN1xXYPfePwT+riWytBm0P9rkTnDQU9nWBnlywgldzHvUb9BN+FN4tmqmoVr/veNB7NDUV3ueD83NEd/P8AzCAdtK8bwaDXm/GIQa9m/rIIz1MEhGZoadesk2gsAv8A394PS/nAt7YNC7JDcmDUCkHxlFQ4fPBMDtYed6F16xuw/nOQAGh8s34PnIIKQFKsPbyTkA+K1vIVv4eu8Og3xvxxzx3cYMaEdHrCVAUidhD+vrA8z85TpeLv8Y6mtAVX0b/MM0Rt8LLyHd5d6fOFafp80PhpSHyHDmrBYWDw7Mp4ZjQxzTQAaYG8Ol42jB1KgxWeDT5ZtmJVjI5E74eH3lr2Rz4UybdgSzZx9ecg0/Uf7zUGA3k26nDp+eMLfwpIB4eQaj2DjOQYqx5cjhRqvM8f4zdiJpXg1qe8rwORFq2MqwNm9enUgyWCLzOUnv7cYXAbxOxAIQXwN3vCoHUrwahY7gpVDuZC1iGCpGk7H49yjiiUIBw3zfDbIgKp+XGnx/GCzSEVQOdN9Y2pmssCbirVdq/FM1SVLI7RdiBm1VcAY3cGgtvCMGIjyoULJob6O8YCQkkRSIBG04PnCyLI8C8+W9YA4I9q6hHneMtRqQlbzOt9Tm5Uo6C/64dQVSXVPWJdSN+CrY8iaj6wDIFlSWwWubI6lyofwWoUHiCLwlOcLPZxpKi2xRZpdHeOdQjiA97QMMJKmsBCp4g4IFp0CIi0qH30NEVijbQO4ywyV/YgRUODvwpv0rLr1cUCmhTl1I6mI+KRfIH/AG4MY2fTfHK0icN6k9/cHjLh5U2Ei90flXnGIEDc8ohGkW0rrGdAgtsYKLDXUB1ETkPZw4h6q4SSF8PeK6Yc9YE6ykx2IZJ4e4Jo50C/jXOatVGeUIr4sPBp5wN6RQ2Up8MDlfbJSl2nakd/M9F/ILsYOWhUEAR7mo+MCegkP4BwxKp5ESKtpcuVoDJCkqnm6KvnEK+tPLpS1vwD8YTeykpytLuRHjH7q4JVLKHP7O945QT5Auk19d7xsAwC1UsR9oBCOhV1QXAKCb0AInJritloNrwa19F95xCCY+ofjeabZqQOA8psbdDdwhtpctSpzGtFdzGhsS6mavg9Q0+csF2XwK8ch4hMhEe4D2rcdvjz5AQ6qruWxQrHKQVA0dVG55AtXUR7EYcABMDRUXwtYcEsk13vVHU8ZJ7Zsn2Iz6ycAZGw13v4y12kSAlJvyPX3jnGoDB/iNmItWrkLv5b4aim64d3cm0UhPAbtIvEiALdsVCBvjaN2Q+wYDXX3gXi2boPE3fAVusciiygqrxyWXz6G3Jm9AG/EV1MIJH80mlJ4H9ZICvpSA9EXxqYjDXEDbbkqV0ct2Yk0Uk4Hc52QAjU8A4z5ilaNtRpVR0RISKNAZUZrztPD0/OCNzDYXgjQaI7v04EjDmQfQr+sNstIEW97/3WEz7kgAGnxD5vHyL+Jpqv1D6cecWBgzRZCLwNJ5MtWiPShOCNB89+MqEfXQKFN6B5vxgEJiaBVGg0kbNQvPOQ8NYN2Q9EMVwLIONN+QzXb1VdeoRE32v1zkZDDrljJ6vm3zmkbLGwZvkbPOVwboEK0o8wpTWLJA1rKCcCADpoMN40FGxG9KoT18sVCP1JGhvfO/rB8SawIVAu5EeTeanESOg/y4glNfnoyqOlZwBDyNGu5DvZixobYNAutJo+chkTJpVPccRPooNC8SHB5fzIMQ0A4V3x4jzrEByvdK8F73eYQ4XG1VSPHIHXrImSOmku/wA7xvCaTdaH2GIjlALsMPjS+1w9MKbwE++Qa+KdmcQjMtAZzNP18ZwAS3qauc7o1Z15bM5JFyC7F0+G4AAig+8IHAAtlQdAF9+uO6MBhpeleF1hNlBjq3X+3nGDtBsYc8OgiF+sph0MEoG+9rv04pgqLfDX+ckbSFGoX6lPszSwhRtaB1V28ReLnQGqdsyuJVfPPeESMFVY+2633cE1SZWor+Rw9BNEHga8KQ8kzlEoFNavK4OkmMSTxca78DMsutthJR8AvLWIUNhQ2AKweDRK1HPlSNgkAslOVsb1MYAhfGR9EbYk3R64yuaEXCDXzFe9+eMIVXXAO/ZpE4fMwaM1QgRN1bt43TNOh0HYIT6aeTJF08tbT30aHIKMGFdXMKgwSF0T3Ja84StQXQAnVDnij7Ho+bFEjmbQe3XnFWpsfA0A8cQN4HAQGjVgp9D0GQUJr8G8jpF+wylFvxNLNuxOPRPeOSqGoBtrnanEneBiRDsI4NoK+QqbyD70JaQC8FD8pyZodtmpvDlkicus1xANa33iiA32pNORKDuU6hkFJyHKhF8efiuMBXkr1qF8bjEW0aDtN36DOwfdDlcfnbAMjy6Xmvpc2IsEvCSfxfbhSgChuANDsa/iYcd4UIPA8VfIYsiGoBZrdSB9ssBOp1HsOyh5zXmQpY1r43/r2wpbm3tTxPKxOoTvbIPxS3paV1hxdNUXaa0eZ5VNOPgOvvHu8Q5+8IapwUUhOXEN3RHYtRCGpErwVLCm95CHXiIwCUdi+m/e1BBgXkL+gyuEW9I7DvxjE75bF4fsc0wSL4ee+ge+8Mlrno++tYtN8kNgeQK5pHLRFAnHCX4cBUbjOoZ/+XBhioXjvhPjAIUhDVtj5o/nxlXoEqm1Q9bfjWEZDWJbRvxTPfzmt3V4fk79+vGCpFLj+7MQihozmR3dDXSN1MDWKi8HivAn/GEg7CWHQfavw+cgyP511+5x+hBeCcb5Cl9fYqQ/nF/k5SzCHoA/x5ziiAp4L/d9mcRDc1yhfpOWs8PNUBff7ceWD0s5mh+n5cr+xG+NMH28CDU6T41xBKkfZ/5veWRQtbgQD4/pkkdaf1n/2Q==";
//
//        JugadorDM player;
//        JugadorDM matias = new JugadorDM(PlayerTypes.REGISTERED, "", "", "130832", "", "", Utils.getFormattedClientName("MATIAS ARANCIBIA YUDICHE"), "", "SILVER", "00:00:00", fotoMatias, "OK", "ACTIVO", 0, 1, 2, 1, false, false);
//        JugadorDM sole = new JugadorDM(PlayerTypes.REGISTERED, "", "", "130831", "", "", Utils.getFormattedClientName("SOLANGE SANTOS LLANOS"), "", "GOLD", "00:00:00", GlobalData.EMPTY, "OK", "ACTIVO", 0, 1, 2, 2, false, false);
//        JugadorDM felipe = new JugadorDM(PlayerTypes.REGISTERED, "", "", "169387", "", "", Utils.getFormattedClientName("FELIPE OJEDA LATORRE"), "", "PLATINUM", "00:00:00", fotoFelipe, "OK", "ACTIVO", 0, 1, 2, 3, false, false);
//        JugadorDM alejandro = new JugadorDM(PlayerTypes.REGISTERED, "", "", "10111", "", "", Utils.getFormattedClientName("ALEJANDRO MARTINEZ VERDUGO"), "", "CLASSIC", "00:00:00", GlobalData.EMPTY, "OK", "ACTIVO", 0, 1, 2, 4, false, false);
//        JugadorDM fernando = new JugadorDM(PlayerTypes.REGISTERED, "", "", "11111", "", "", Utils.getFormattedClientName("FERNANDO VICENCIO REYES"), "", "DIAMOND", "00:00:00", GlobalData.EMPTY, "OK", "ACTIVO", 0, 1, 2, 5, false, false);
//
//        player = matias;
//
//        switch (id) {
//            case "130831":
//                player = sole;
//                break;
//            case "169387":
//                player = felipe;
//                break;
//            case "10111":
//                player = alejandro;
//                break;
//            case "11111":
//                player = fernando;
//                break;
//        }
//
//        if (player.getMensaje_validacion().equals("OK")) {
//            SignIn mSignIn = new SignIn("", "", id);
//            SignInTable signInTable = new SignInTable(mSignIn, null);
//
//            if (signInTable.tableUbicationClass != null) {
//                showDialogPlayerIsInAnotherTable(signInTable.tableUbicationClass);
//            } else {
//                SignIn signIn = signInTable.signInClass;
//
//                playerView.getJugador().setId_juego(signIn.id_juego);
//                playerView.getJugador().setId_mcc_jugador(signIn.id_cliente_mcc);
//                playerView.getJugador().setId_jugador(signIn.id_cliente);
//                playerView.getJugador().setId_categoria_jugador(player.getId_categoria_jugador());
//                playerView.getJugador().setRut_jugador(player.getRut_jugador());
//                playerView.getJugador().setNombre_jugador(player.getNombre_jugador());
//                playerView.getJugador().setPlayerType(PlayerTypes.REGISTERED);
//                playerView.getJugador().setCategoria_jugador(player.getCategoria_jugador().toUpperCase(Utils.locale));
//                playerView.getJugador().setFoto_jugador_b64(player.getFoto_jugador_b64());
//                playerView.getJugador().setIsPaused(false);
//                playerView.getJugador().setPuntos_jornada(player.getPuntos_jornada());
//                playerView.getJugador().setTimer_jugador(getString(R.string.null_timer_tracking));
//
//                playerView.getRootView().setOnDragListener(new MesaOnDragListener());
//
//                setPlayerImageClickListener(playerView);
//
//                playerView.populateView(playerView.getJugador(), PlayerTypes.REGISTERED, registeredPlayerImageClickListener);
//
//                checkCompsAvailability(playerView, false);
//
//                setPlayerViewInPosition(playerView, numero_jugador_seleccionado - 1);
//
//                showPlayerAcceptedDialog(dialogUserAccepted, playerView);
//
//                if(player.getId_mcc_jugador().equals("130832")) {
//                    signInTable.tableUbicationClass = new UbicacionMesa("ACTIVO", "Viña del Mar", "Main", "Corral 1", "Blackjack 007", "1", GlobalData.EMPTY, player.getNombre_jugador());
//                }
//            }
//        }
//        else {
//            Utils.showInformationDialog(activity, false, player.getMensaje_validacion(), 1200);
//        }
//    }

    private void loginRegisteredPlayer(final PlayerView playerView, final PlayersSearchCriteria searchCriteria, final String value) {
//        loginDummyClient(playerView, value);

        AsyncResponse getClientAsyncResponse = new AsyncResponse() {
            @Override
            public void onProcessFinish(Object result) {
                final JugadorDM player = (JugadorDM) result;

                if (player.getMensaje_validacion().equals("OK")) {
                    AsyncResponse signInTableClientAsyncResponse = new AsyncResponse() {
                        @Override
                        public void onProcessFinish(Object result) {
                            SignInTable signInTable = (SignInTable) result;

                            if (signInTable != null) {
                                if (signInTable.tableUbicationClass != null) {
                                    showDialogPlayerIsInAnotherTable(signInTable.tableUbicationClass);
                                } else {
                                    SignIn signIn = signInTable.signInClass;

                                    playerView.getJugador().setId_juego(signIn.id_juego);
                                    playerView.getJugador().setId_mcc_jugador(signIn.id_cliente_mcc);
                                    playerView.getJugador().setId_jugador(signIn.id_cliente);
                                    playerView.getJugador().setId_categoria_jugador(player.getId_categoria_jugador());
                                    playerView.getJugador().setRut_jugador(player.getRut_jugador());
                                    playerView.getJugador().setNombre_jugador(player.getNombre_jugador());
                                    playerView.getJugador().setPlayerType(PlayerTypes.REGISTERED);
                                    playerView.getJugador().setCategoria_jugador(player.getCategoria_jugador().toUpperCase(Utils.locale));
                                    playerView.getJugador().setFoto_jugador_b64(player.getFoto_jugador_b64());
                                    playerView.getJugador().setIsPaused(false);
                                    playerView.getJugador().setPuntos_jornada(player.getPuntos_jornada());
                                    playerView.getJugador().setTimer_jugador(getString(R.string.null_timer_tracking));

                                    playerView.getRootView().setOnDragListener(new MesaOnDragListener());

                                    setPlayerImageClickListener(playerView);

                                    playerView.populateView(playerView.getJugador(), PlayerTypes.REGISTERED, registeredPlayerImageClickListener);

                                    checkCompsAvailability(playerView, false);

                                    setPlayerViewInPosition(playerView, numero_jugador_seleccionado - 1);

                                    showPlayerAcceptedDialog(dialogUserAccepted, playerView);
                                }
                            } else {
                                Utils.showInformationDialog(activity, false, GlobalData.ERROR_SIGN_IN_REGISTERED_CLIENT, 1200);
                            }
                        }
                    };

                    ServicioMesas.SignInTableClientAsyncTask si = new ServicioMesas.SignInTableClientAsyncTask(Utils.getNewProgressDialog(activity, R.string.registering_new_player, false), signInTableClientAsyncResponse);

                    si.execute(GlobalData.ID_UNIDAD, gd.getMesa().getId_sesion(), player.getRut_jugador(), player.getNombre_jugador(), String.valueOf(numero_jugador_seleccionado), player.getId_categoria_jugador(), String.valueOf(player.getPuntos_jornada()), player.getId_mcc_jugador(), String.valueOf(0));
                } else {
                    Utils.showInformationDialog(activity, false, player.getMensaje_validacion(), 1200);
                }
            }
        };

        ServicioMesas.GetEnjoyClientAsyncTask si = new ServicioMesas.GetEnjoyClientAsyncTask(Utils.getNewProgressDialog(activity, R.string.getting_player_info, false), getClientAsyncResponse);

        si.execute(GlobalData.ID_UNIDAD, value, searchCriteria.toString(), Utils.getSecurityToken());
    }

    private void startHandlerRecuentoOperativo() {
        handlerOperativeCounting = new Handler();

        runnableOperativeCounting = new Runnable() {
            @Override
            public void run() {
                String horaActual = Utils.getCurrentTime();
                String mins = Utils.formatDate(horaActual, "HH:mm:ss", "mm");

                if (mins.equals(getString(R.string.number_double_zero_text)) && GlobalData.OPERATING_COUNTING == 0) {
                    GlobalData.OPERATING_COUNTING++;

                    valuesKeyboard.showKeyboard(KeyboardTypes.OPERATIVE_COUNTING, new OnValueEnteredListener() {
                        @Override
                        public void onValueEntered(View v, String value) {
                            if (!value.isEmpty()) {
                                AsyncResponse insertOperativeCountingAsyncResponse = new AsyncResponse() {
                                    @Override
                                    public void onProcessFinish(Object result) {
                                        int res = (int) result;

                                        if (res == 1) {
                                            valuesKeyboard.closeKeyboard();

                                            Utils.showSuccessOperationDialog(activity, false, GlobalData.SUCCESS_REGISTERING_OPERATIVE_COUNTING, 1200);
                                        } else {
                                            Utils.showInformationDialog(activity, false, GlobalData.ERROR_DOING_OPERATIVE_COUNTING, 1200);
                                        }
                                    }
                                };

                                ServicioMesas.InsertOperativeCountingAsyncTask ioc = new ServicioMesas.InsertOperativeCountingAsyncTask(Utils.getNewProgressDialog(activity, R.string.registering_operative_counting, false), insertOperativeCountingAsyncResponse);

                                ioc.execute(gd.getMesa().getId_sesion(), value, GlobalData.RECUENTO_OPERATIVO);
                            } else {
                                Utils.showInformationDialog(activity, false, GlobalData.ERROR_VALUE_IS_ZERO_OR_EMPTY, 1200);
                            }
                        }
                    });
                }
                else if (!mins.equals(getString(R.string.number_double_zero_text))){
                    GlobalData.OPERATING_COUNTING = 0;
                }

                handlerOperativeCounting.postDelayed(this, GlobalData.INTERVALO_RECUENTO_OPERATIVO);
            }
        };

        handlerOperativeCounting.postDelayed(runnableOperativeCounting, GlobalData.INTERVALO_RECUENTO_OPERATIVO);
    }

    private void swapPlayerNumber(JugadorDM origen, JugadorDM destino) {
        int numJug = origen.getNumero_jugador();
        origen.setNumero_jugador(destino.getNumero_jugador());
        destino.setNumero_jugador(numJug);
    }

//    private void swapImagesAttr(PlayerView origin, PlayerView destination) {
//        ImageView pauseOrigin = origin.playerIsPaused;
//        ImageView pauseDestination = destination.playerIsPaused;
//        ImageView playerHasCompOrigin = origin.playerHasComp;
//        ImageView playerHasCompDestination = destination.playerHasComp;
//
//        int visibilityPauseOrigin = pauseOrigin.getVisibility();
//        int visibilityPauseDestination = pauseDestination.getVisibility();
//        int visibilityPlayerHasCompOrigin = playerHasCompOrigin.getVisibility();
//        int visibilityPlayerHasCompDestination = playerHasCompDestination.getVisibility();
//        int visibilityAux;
//
//        visibilityAux = visibilityPauseOrigin;
//        visibilityPauseOrigin = visibilityPauseDestination;
//        visibilityPauseDestination = visibilityAux;
//
//        pauseOrigin.setVisibility(visibilityPauseOrigin);
//        pauseDestination.setVisibility(visibilityPauseDestination);
//
//        visibilityAux = visibilityPlayerHasCompOrigin;
//        visibilityPlayerHasCompOrigin = visibilityPlayerHasCompDestination;
//        visibilityPlayerHasCompDestination = visibilityAux;
//
//        playerHasCompOrigin.setVisibility(visibilityPlayerHasCompOrigin);
//        playerHasCompDestination.setVisibility(visibilityPlayerHasCompDestination);
//
//        if(destination.getJugador().getPlayerType() == PlayerTypes.EMPTY) {
//            resetViewState(destination);
//        }
//    }

    private int getPlayerViewPosition(PlayerView playerView) {
        for(int i=0; i<playersViewArrayList.size(); i++) {
            if(playerView == playersViewArrayList.get(i)) {
                return i;
            }
        }

        return -1;
    }

    private void swapPlayerViews(PlayerView origin, PlayerView destination) {
        JugadorDM origen = origin.getJugador();
        JugadorDM destino = destination.getJugador();

        swapPlayerNumber(origen, destino);

//        swapImagesAttr(origin, destination);

        origen.setTimer_jugador(origin.playerTimer.getText().toString());
        destino.setTimer_jugador(destination.playerTimer.getText().toString());

        OnPlayerImageClickListener listenerOrigen = emptyPlayerImageClickListener;
        OnPlayerImageClickListener listenerDestino = emptyPlayerImageClickListener;

        setPlayerImageClickListener(origin);

        if(destino.getPlayerType() == PlayerTypes.REGISTERED) {
            listenerDestino = registeredPlayerImageClickListener;
        }
        else if(destino.getPlayerType() == PlayerTypes.ANONYMOUS) {
            listenerDestino = anonymousPlayerImageClickListener;
        }

        setPlayerImageClickListener(destination);

        origin.populateView(destino, destino.getPlayerType(), listenerDestino);

        if(origen.getPlayerType() == PlayerTypes.REGISTERED) {
            listenerOrigen = registeredPlayerImageClickListener;
        }
        else if(origen.getPlayerType() == PlayerTypes.ANONYMOUS) {
            listenerOrigen = anonymousPlayerImageClickListener;
        }

        destination.populateView(origen, origen.getPlayerType(), listenerOrigen);
    }

    private void resetViewState(PlayerView playerView) {
        TextView playerName = (TextView) playerView.getRootView().findViewById(R.id.playerName);
        ImageView playerImage = (ImageView) playerView.getRootView().findViewById(R.id.playerImg);
        TextView playerTimer = (TextView) playerView.getRootView().findViewById(R.id.playerTimer);

        setStateColor(playerName, playerTimer, TrackingStates.OK);
        playerImage.clearAnimation();
    }

    // TODO: MesaOnDragListener
    protected class MesaOnDragListener implements View.OnDragListener {
        Animation fade_in = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
        private PlayerView playerViewDestination;
        private PlayerView playerViewOrigin;

        public MesaOnDragListener() {}

        @Override
        public boolean onDrag(View view, DragEvent dragEvent) {
            switch (dragEvent.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:

                    break;

                case DragEvent.ACTION_DRAG_ENTERED:
                    if (view == btnDeletePlayer) {
                        btnDeletePlayer.setVisibility(View.VISIBLE);
                    }
                    view.startAnimation(fade_in);
                    break;

                case DragEvent.ACTION_DRAG_EXITED:

                    break;

                case DragEvent.ACTION_DROP:
                    playerViewOrigin = getPlayerViewByPosition(numero_jugador_seleccionado - 1);

                    if (view == btnDeletePlayer) {
                        if (playerViewOrigin != null) {
                            AsyncResponse finishGameAsyncResponse = new AsyncResponse() {
                                @Override
                                public void onProcessFinish(Object result) {
                                    int res = (int) result;

                                    if (res == 1) {
                                        int posX = playerViewOrigin.getJugador().getPosX();
                                        int posY = playerViewOrigin.getJugador().getPosY();
                                        boolean isPaused = playerViewOrigin.getJugador().isPaused();

                                        JugadorDM emptyPlayer = new JugadorDM();

                                        emptyPlayer.setNombre_jugador(getString(R.string.empty_text));
                                        emptyPlayer.setTimer_jugador(getString(R.string.null_timer_tracking));
                                        emptyPlayer.setNumero_jugador(numero_jugador_seleccionado);
                                        emptyPlayer.setPosX(posX);
                                        emptyPlayer.setPosY(posY);

                                        playerViewOrigin.populateView(emptyPlayer, PlayerTypes.EMPTY, emptyPlayerImageClickListener);

                                        if (isPaused) {
                                            setPausedStatePlayer(playerViewOrigin);
                                        }

                                        resetViewState(playerViewOrigin);

                                        setPlayerViewInPosition(playerViewOrigin, numero_jugador_seleccionado - 1);
                                    } else {
                                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_DELETING_PLAYER_FROM_TABLE, 1200);
                                    }
                                }
                            };

                            String tiempoJuego = Utils.formatDate(playerViewOrigin.getJugador().getTimer_jugador(), "HH:mm:ss", "ss");

                            ServicioMesas.FinishGameAsyncTask fg = new ServicioMesas.FinishGameAsyncTask(Utils.getNewProgressDialog(activity, R.string.closing_player_session, false), finishGameAsyncResponse);

                            fg.execute(gd.getMesa().getId_sesion(), playerViewOrigin.getJugador().getId_jugador(), Utils.getCurrentTime(), tiempoJuego);
                        }
                    }
                    else {
                        playerViewDestination = getPlayerViewByImage(view);

                        if (playerViewOrigin != null && playerViewDestination != null && playerViewOrigin != playerViewDestination) {
                            if(!playerViewDestination.getJugador().isPaused()) {
                                AsyncResponse updatePlayerPositionAsyncResponse = new AsyncResponse() {
                                    @Override
                                    public void onProcessFinish(Object result) {
                                        int res = (int) result;

                                        if(res == 1) {
                                            int posPlayerOrigin = getPlayerViewPosition(playerViewOrigin);
                                            int posPlayerDestination = getPlayerViewPosition(playerViewDestination);

                                            swapPlayerViews(playerViewOrigin, playerViewDestination);

                                            if(posPlayerOrigin != -1 && posPlayerDestination != -1) {
                                                playersViewArrayList.set(posPlayerOrigin, playerViewDestination);
                                                playersViewArrayList.set(posPlayerDestination, playerViewOrigin);
                                            }

                                            if(playerViewDestination.getJugador().getPlayerType() == PlayerTypes.REGISTERED) {
                                                checkCompsAvailability(playerViewDestination, false);
                                            }
                                            else {
                                                playerViewDestination.getJugador().setHasAvailableComp(false);
                                                playerViewDestination.playerHasComp.setVisibility(View.GONE);
                                            }

                                            if(playerViewOrigin.getJugador().getPlayerType() == PlayerTypes.REGISTERED) {
                                                checkCompsAvailability(playerViewOrigin, false);
                                            }
                                            else {
                                                playerViewOrigin.getJugador().setHasAvailableComp(false);
                                                playerViewOrigin.playerHasComp.setVisibility(View.GONE);
                                            }
                                        }
                                        else {
                                            Utils.showInformationDialog(activity, false, GlobalData.ERROR_UPDATING_PLAYER_POSITION, 1200);
                                        }
                                    }
                                };

                                String idJuegoOrigen = playerViewOrigin.getJugador().getId_juego();
                                String idJuegoDestino = playerViewDestination.getJugador().getId_juego();
                                String posicionOrigen = String.valueOf(playerViewOrigin.getJugador().getNumero_jugador());
                                String posicionDestino = String.valueOf(playerViewDestination.getJugador().getNumero_jugador());

                                if(idJuegoDestino.isEmpty()) idJuegoDestino = String.valueOf(-1);

                                ServicioMesas.UpdatePlayerPositionAsyncTask upp = new ServicioMesas.UpdatePlayerPositionAsyncTask(Utils.getNewProgressDialog(activity, R.string.registering_player_position_update, false), updatePlayerPositionAsyncResponse);

                                upp.execute(idJuegoOrigen, posicionOrigen, idJuegoDestino, posicionDestino);

                            }
                            else {
                                Utils.toast(c, GlobalData.ERROR_PLAYER_PAUSED_CAN_NOT_BE_MOVED, 1);
                            }
                        }
                    }
                    break;

                case DragEvent.ACTION_DRAG_ENDED:
                    if (playerViewOrigin != null) {
                        vibrator.vibrate(200);
                    }

                    containerBtnDeletePlayer.clearAnimation();

                    Animation fade_out = AnimationUtils.loadAnimation(activity, R.anim.fade_out);

                    containerBtnDeletePlayer.startAnimation(fade_out);

                    fade_out.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            containerBtnDeletePlayer.clearAnimation();
                            containerBtnDeletePlayer.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });

                    break;
            }

            return true;
        }
    }

    // TODO: MesaOnClickListener
    protected class MesaOnClickListener implements View.OnClickListener {

        public MesaOnClickListener() {
        }

        @Override
        public void onClick(View v) {
            if (v == btnIdCliente) {
                final PlayerView playerView = getPlayerViewByPosition(numero_jugador_seleccionado - 1);

                addUserKeyboard.showKeyboard(KeyboardTypes.ID, new OnValueEnteredListener() {
                    @Override
                    public void onValueEntered(View v, String value) {
                        if(playerView != null) {
                            loginRegisteredPlayer(playerView, PlayersSearchCriteria.MCC, value);
                        }
                    }
                });
            } else if (v == btnRutCliente) {
                final PlayerView playerView = getPlayerViewByPosition(numero_jugador_seleccionado - 1);

                addUserKeyboard.showKeyboard(KeyboardTypes.RUT, new OnValueEnteredListener() {
                    @Override
                    public void onValueEntered(View v, String value) {
                        if(playerView != null) {
                            loginRegisteredPlayer(playerView, PlayersSearchCriteria.RUT, value);
                        }
                    }
                });
            } else if (v == btnClienteAnonimo) {
                PlayerView playerView = getPlayerViewByPosition(numero_jugador_seleccionado - 1);

                loginAnonymousPlayer(playerView);
            } else if (v == appLogo) {
                Utils.getAbout(activity, 400, 600, true).show();
            } else if (v == btnJefeMesa) {
                userType = GlobalData.USUARIO_JEFE_MESA;

                if (gd.getJefeMesa() != null) {
                    String nombreJefeMesa = getString(R.string.logout_jefe_mesa_request_question).replace("#_NOMBRE_USUARIO_#", "<br/><b>(" + gd.getJefeMesa().getNombre_usuario() + ")</b>");

                    confirmationDialog.showConfirmationDialog(-1, Html.fromHtml(nombreJefeMesa), new OnButtonPressedListener() {
                        @Override
                        public void onButtonAcceptPressed() {
                            AsyncResponse finishUserReliefAsyncResponse = new AsyncResponse() {
                                @Override
                                public void onProcessFinish(Object result) {
                                    int res = (int) result;

                                    if (res == 1) {
                                        btnJefeMesa.setText(getString(R.string.log_in_text));

                                        gd.setJefeMesa(null);

                                        btnTableOptions.setEnabled(false);
                                    } else {
                                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_CLOSING_SESSION_JEFE_MESA, 1200);
                                    }
                                }
                            };

                            ServicioMesas.FinishUserReliefAsyncTask fur = new ServicioMesas.FinishUserReliefAsyncTask(Utils.getNewProgressDialog(activity, R.string.closing_jefemesa_session, false), finishUserReliefAsyncResponse);

                            fur.execute(gd.getJefeMesa().getId_usuario(), gd.getMesa().getId_sesion(), String.valueOf(0));
                        }
                    });
                } else {
                    enterNewUser(GlobalData.USUARIO_JEFE_MESA, true);
                }
            } else if (v == btnCroupier) {
                userType = GlobalData.USUARIO_CROUPIER;

                if (gd.getCroupier() != null) {
                    String nombreCroupier = getString(R.string.logout_croupier_request_question).replace("#_NOMBRE_USUARIO_#", "<br/><b>(" + gd.getCroupier().getNombre_usuario() + ")</b>");

                    confirmationDialog.showConfirmationDialog(-1, Html.fromHtml(nombreCroupier), new OnButtonPressedListener() {
                        @Override
                        public void onButtonAcceptPressed() {
                            AsyncResponse finishUserReliefAsyncResponse = new AsyncResponse() {
                                @Override
                                public void onProcessFinish(Object result) {
                                    int res = (int) result;

                                    if (res == 1) {
                                        btnCroupier.setText(getString(R.string.log_in_text));

                                        gd.setCroupier(null);

                                        enterNewUser(GlobalData.USUARIO_CROUPIER, false);
                                    } else {
                                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_REGISTERING_RELIEF_OF_USER, 1200);
                                    }
                                }
                            };

                            ServicioMesas.FinishUserReliefAsyncTask fur = new ServicioMesas.FinishUserReliefAsyncTask(Utils.getNewProgressDialog(activity, R.string.closing_cruopier_session, false), finishUserReliefAsyncResponse);

                            fur.execute(gd.getCroupier().getId_usuario(), gd.getMesa().getId_sesion(), String.valueOf(1));
                        }
                    });
                }
            } else if (v == btnTableOptions) {
                final OptionsDialogController optionsController = new OptionsDialogController(activity, new OnOptionSelectedListener() {
                    @Override
                    public void onOptionSelected(final Dialog optionsDialog, TableOptions optionSelected) {
                        switch (optionSelected) {
                            case REFRESH_TABLE_IMG:
                                AsyncResponse getTableImagesAsynResponse = new AsyncResponse() {
                                    @Override
                                    public void onProcessFinish(Object result) {
                                        MesaDM mesa = (MesaDM) result;

                                        if(mesa != null) {
                                            gd.getMesa().setImagen_mesa_b64(mesa.getImagen_mesa_b64());
                                            gd.getMesa().setBg_mesa_b64(mesa.getBg_mesa_b64());

                                            setTableImageAndBg();

                                            optionsDialog.dismiss();

                                            Utils.showSuccessOperationDialog(activity, false, GlobalData.SUCCESS_REFRESHING_TABLE_IMAGES, 1200);
                                        }
                                        else {
                                            Utils.showInformationDialog(activity, false, GlobalData.ERROR_GETTING_TABLE_IMAGES, 1200);
                                        }
                                    }
                                };

                                ServicioMesas.GetTableImageAndBgAsyncTask gtib = new ServicioMesas.GetTableImageAndBgAsyncTask(Utils.getNewProgressDialog(activity, R.string.getting_table_images, false), getTableImagesAsynResponse);

                                gtib.execute(gd.getMesa().getId_mesa());

                                break;

                            case GET_PARAMS:
                                boolean succesGetParams = Utils.getParamsFromDB(GlobalData.ID_UNIDAD);

                                if (succesGetParams) {
                                    optionsDialog.dismiss();

                                    Utils.showSuccessOperationDialog(activity, false, GlobalData.SUCCESS_GETTING_PARAMS, 1200);
                                } else {
                                    Utils.showInformationDialog(activity, false, GlobalData.ERROR_GETTING_PARAMS, 1200);
                                }
                                break;

                            case UPDATE_MIN_MAX:
                                final MinMaxValuesController minMaxValuesController = new MinMaxValuesController(activity, R.array.min_max_values, R.array.min_max_values);

                                minMaxValuesController.showMinMaxDialog(new OnMinMaxValuesEnteredListener() {
                                    @Override
                                    public void onValuesEntered(final int minValue, final int maxValue) {
                                        minMaxValuesController.setEnabledAcceptButton(false);

                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                minMaxValuesController.setEnabledAcceptButton(true);
                                            }
                                        }, 5000);

                                        if (minValue > 0 && maxValue > 0) {
                                            AsyncResponse updateMinMaxAsyncResponse = new AsyncResponse() {
                                                @Override
                                                public void onProcessFinish(Object result) {
                                                    minMaxValuesController.setEnabledAcceptButton(true);

                                                    int res = (int) result;

                                                    if (res == 1) {
                                                        minMaxValuesController.closeMinMaxDialog();

                                                        optionsDialog.dismiss();

                                                        gd.getMesa().setMin_mesa(minValue);
                                                        gd.getMesa().setMax_mesa(maxValue);

                                                        Utils.showSuccessOperationDialog(activity, false, GlobalData.SUCCESS_UPDATE_MIN_MAX, 1200);
                                                    } else {
                                                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_UPDATING_MIN_MAX, 1200);
                                                    }
                                                }
                                            };

                                            ServicioMesas.UpdateMinMaxAsyncTask umm = new ServicioMesas.UpdateMinMaxAsyncTask(Utils.getNewProgressDialog(activity, R.string.updating_min_max_values, false), updateMinMaxAsyncResponse);

                                            umm.execute(gd.getMesa().getId_sesion(), String.valueOf(minValue), String.valueOf(maxValue));
                                        } else {
                                            Utils.showInformationDialog(activity, false, GlobalData.ERROR_VALUE_MIN_MAX_IS_ZERO, 1200);
                                        }
                                    }
                                });
                                break;

                            case OPERATIVE_COUNTING:
                                valuesKeyboard.showKeyboard(KeyboardTypes.OPERATIVE_COUNTING_ON_DEMAND, new OnValueEnteredListener() {
                                    @Override
                                    public void onValueEntered(View v, String value) {
                                        if (!value.isEmpty()) {
                                            AsyncResponse insertOperativeCountingAsyncResponse = new AsyncResponse() {
                                                @Override
                                                public void onProcessFinish(Object result) {
                                                    int res = (int) result;

                                                    if (res == 1) {
                                                        valuesKeyboard.closeKeyboard();

                                                        optionsDialog.dismiss();

                                                        Utils.showSuccessOperationDialog(activity, false, GlobalData.SUCCESS_REGISTERING_OPERATIVE_COUNTING, 1200);
                                                    } else {
                                                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_DOING_OPERATIVE_COUNTING, 1200);
                                                    }
                                                }
                                            };

                                            ServicioMesas.InsertOperativeCountingAsyncTask ioc = new ServicioMesas.InsertOperativeCountingAsyncTask(Utils.getNewProgressDialog(activity, R.string.registering_operative_counting, false), insertOperativeCountingAsyncResponse);

                                            ioc.execute(gd.getMesa().getId_sesion(), value, GlobalData.RECUENTO_OPERATIVO);
                                        } else {
                                            Utils.showInformationDialog(activity, false, GlobalData.ERROR_VALUE_IS_ZERO_OR_EMPTY, 1200);
                                        }
                                    }
                                });
                                break;

                            case CLOSE_TABLE:
                                ConfirmationDialogController confirmationDialogController = new ConfirmationDialogController(activity);

                                String confirmationText = getString(R.string.close_table_request_question).replace("#_NOMBRE_MESA_#", "<b>" + gd.getMesa().getNombre_completo_mesa() + "</b>");

                                confirmationDialogController.showConfirmationDialog(-1, Html.fromHtml(confirmationText), new OnButtonPressedListener() {
                                    @Override
                                    public void onButtonAcceptPressed() {
                                        AsyncResponse closeTableAsyncResponse = new AsyncResponse() {
                                            @Override
                                            public void onProcessFinish(Object result) {
                                                int res = (int) result;

                                                if (res == 1) {
                                                    gd = null;

                                                    optionsDialog.dismiss();

                                                    if (handlerPlayersTimer != null) {
                                                        handlerPlayersTimer.removeCallbacks(runnablePlayersTimer);
                                                    }

                                                    if (handlerOperativeCounting != null) {
                                                        handlerOperativeCounting.removeCallbacks(runnableOperativeCounting);
                                                    }

                                                    LoginActivity.sesion = null;

                                                    finish();

                                                    Utils.toast(c, getString(R.string.success_session_closed));
                                                } else {
                                                    Utils.showInformationDialog(activity, false, GlobalData.ERROR_CLOSING_TABLE, 1200);
                                                }
                                            }
                                        };

                                        ServicioMesas.CloseTableAsyncTask ct = new ServicioMesas.CloseTableAsyncTask(Utils.getNewProgressDialog(activity, getString(R.string.closing_table_session), false), closeTableAsyncResponse);

                                        ct.execute(gd.getMesa().getId_sesion(), gd.getCroupier().getId_usuario());
                                    }
                                });
                            break;
                        }
                    }
                });

                optionsController.showOptionsDialog();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationView)) {
            drawerLayout.closeDrawer(navigationView);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(GlobalData.MUST_CHECK_IF_AVAILABLE_COMPS) {
            GlobalData.MUST_CHECK_IF_AVAILABLE_COMPS = false;

            PlayerView player = getPlayerViewByPosition(numero_jugador_seleccionado-1);

//            Logger.d("numero_jugador_seleccionado= "+numero_jugador_seleccionado);

            if(player != null) {
                checkCompsAvailability(player, false);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        for(int i=0; i<playersViewArrayList.size(); i++) {
            playersArrayList.add(playersViewArrayList.get(i).getJugador());
        }

//        playersViewArrayList.clear();

        if (gd != null) {
            gd.getMesa().setJugadores(playersArrayList);

            outState.putSerializable("global_data", gd);
        }

        outState.putSerializable("players_list", playersArrayList);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        gd = (GlobalData) savedInstanceState.getSerializable("global_data");

        playersArrayList = (ArrayList<JugadorDM>) savedInstanceState.getSerializable("players_list");

        if(playersArrayList != null && playersArrayList.size() > 0) {
//            Logger.d("playersViewArrayList.size()= " + playersViewArrayList.size());

            for(int i=0; i<playersViewArrayList.size(); i++) {
                PlayerView playerView = playersViewArrayList.get(i);
                boolean isPlayerPosition = false;
                JugadorDM player = null;

                for(int j=0; j<playersArrayList.size(); j++) {
                    if(playersArrayList.get(j).getNumero_jugador() == playerView.getJugador().getNumero_jugador()) {
                        isPlayerPosition = true;

                        player = playersArrayList.get(j);

                        break;
                    }
                }

                if(isPlayerPosition && player != null) {
                    PlayerTypes pType = player.getPlayerType();

//                    Logger.d(player.getNombre_jugador() +
//                            "\npos= " + player.getNumero_jugador() +
//                            "\nplayer type= " + pType.name());

                    OnPlayerImageClickListener listener = emptyPlayerImageClickListener;

                    setPlayerImageClickListener(playerView);

                    switch (pType) {
                        case EMPTY:
                            listener = emptyPlayerImageClickListener;
                            break;

                        case ANONYMOUS:
                            listener = anonymousPlayerImageClickListener;
                            break;

                        case REGISTERED:
                            listener = registeredPlayerImageClickListener;
                            break;
                    }

                    playerView.populateView(player, pType, listener);
                }
            }

            playersArrayList.clear();
        }
    }
}
