package com.jumpitt.winmanagement.ui.comps;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.CategoriaCompsDM;
import com.jumpitt.winmanagement.dataModel.CompDM;
import com.jumpitt.winmanagement.dataModel.JugadorDM;
import com.jumpitt.winmanagement.dataModel.MesaDM;
import com.jumpitt.winmanagement.dataModel.ModificadorCompDM;
import com.jumpitt.winmanagement.dataModel.MotivosAutorizacionCompsDM;
import com.jumpitt.winmanagement.dataModel.ResultCompsDM;
import com.jumpitt.winmanagement.dataModel.ResultListCompsDM;
import com.jumpitt.winmanagement.dataModel.UsuarioDM;
import com.jumpitt.winmanagement.lists.adapters.ListaInfoExtraAdapter;
import com.jumpitt.winmanagement.lists.adapters.ListaPedidosCompsAdapter;
import com.jumpitt.winmanagement.lists.adapters.ListaProductosCompsAdapter;
import com.jumpitt.winmanagement.lists.classes.ItemListaComp;
import com.jumpitt.winmanagement.lists.classes.ItemPedidoComp;
import com.jumpitt.winmanagement.lists.classes.SelectedItemListaComp;
import com.jumpitt.winmanagement.lists.swipeToDismiss.SwipeDismissListViewTouchListener;
import com.jumpitt.winmanagement.utils.BaseActivity;
import com.jumpitt.winmanagement.utils.GlobalData;
import com.jumpitt.winmanagement.utils.ItemClickSupport;
import com.jumpitt.winmanagement.utils.Utils;
import com.jumpitt.winmanagement.utils.controllers.ConfirmationDialogController;
import com.jumpitt.winmanagement.utils.interfaces.OnAdditionalInfoButtonPressedListener;
import com.jumpitt.winmanagement.utils.interfaces.OnButtonPressedListener;
import com.jumpitt.winmanagement.utils.interfaces.OnProductSelectedListener;
import com.jumpitt.winmanagement.webservices.ServicioMesas;
import com.jumpitt.winmanagement.webservices.classes.ResultInsertCompsOrder;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.Collections;

@SuppressWarnings("deprecation")
public class CompsActivity extends BaseActivity implements View.OnClickListener {
    private Activity activity;
    private MesaDM mesa;
    private UsuarioDM jefeSalon;
    private UsuarioDM jefeMesa;
    private UsuarioDM croupier;
    private JugadorDM jugador;
    private int contComps = 0;
    private long presupuesto_comps = -1;
    private long presupuesto_inicial_comps;
    private boolean requiere_autorizacion;
    private boolean requiere_autorizacion_inicial;
    private boolean USER_CAN_DELETE_ITEMS_FROM_LIST = true;
    private String codigo_rvc;
    private String digits = "";
    private String comentarioAutorizacion;
    private TextView presupuestoTextView;
    private ImageButton btnOpciones;
    private Button volver;
    private Button enviarComp;
    private Button bebestibles;
    private Button cafeteria;
    private Button limpiarLista;
    private Button conAlcohol;
    private Button sinAlcohol;
    private TextView currentTime;
    private TextView emptyCompsCategory;
    private ArrayList<String> listaComentarios;
    private ArrayList<CompDM> listaBS;
    private ArrayList<CompDM> listaBC;
    private ArrayList<CompDM> listaC;
    private ArrayList<ItemListaComp> listaBebidaSinAlcohol;
    private ArrayList<ItemListaComp> listaBebidaConAlcohol;
    private ArrayList<ItemListaComp> listaCafeteria;
    private ArrayList<ItemPedidoComp> listaPedidosComps = new ArrayList<>();
    private ListaPedidosCompsAdapter adapterListaPedidosComps;
    private ListView listViewProductos;
    private ListView listViewPedidos;
    private RelativeLayout tipoBebidaRelativeLayout;
    private Dialog dialogOpcionesJefeMesa;
    private Dialog dialogEnterNewUser;
    private Dialog dialogMostrarValoresParametros;
    private Dialog dialogIngresarMotivosCompFueraRegla;
    private OnProductSelectedListener onProductSelectedListener;
    private Handler getCurrentTimeHandler;
    private Runnable getCurrentTimeRunnable;
    private int currentApiVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comps);

        activity = this;

        jefeMesa = (UsuarioDM) getIntent().getSerializableExtra("jefe_mesa");
        croupier = (UsuarioDM) getIntent().getSerializableExtra("croupier");
        mesa = (MesaDM) getIntent().getSerializableExtra("mesa");
        jugador = (JugadorDM) getIntent().getSerializableExtra("jugador");
        requiere_autorizacion = getIntent().getExtras().getBoolean("requiere_autorizacion");
        requiere_autorizacion_inicial = requiere_autorizacion;

        goneViewHideen();

        boolean succesGetParams = Utils.getParamsFromDB(GlobalData.ID_UNIDAD);

        if (!succesGetParams) {
            Utils.showInformationDialog(activity, false, GlobalData.ERROR_GETTING_PARAMS, 1200);

            finish();

            return;
        }

        initViews();

        getProducts();
    }

    private void goneViewHideen() {


        currentApiVersion = android.os.Build.VERSION.SDK_INT;

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;


        if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
        {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }

    }

    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }



    private void initViews() {
        setHeaderInfo();

        initDialogsViews();

        listViewProductos = (ListView) findViewById(R.id.list);
        listViewPedidos = (ListView) findViewById(R.id.listaPedidos);

        TextView nombreJugador = (TextView) findViewById(R.id.nombreJugador);
        presupuestoTextView = (TextView) findViewById(R.id.presupuesto);
        emptyCompsCategory = (TextView) findViewById(R.id.emptyList);
        bebestibles = (Button) findViewById(R.id.bebestibles);
        conAlcohol = (Button) findViewById(R.id.conAlcohol);
        sinAlcohol = (Button) findViewById(R.id.sinAlcohol);
        cafeteria = (Button) findViewById(R.id.cafeteria);
        volver = (Button) findViewById(R.id.volver);
        limpiarLista = (Button) findViewById(R.id.limpiarLista);
        enviarComp = (Button) findViewById(R.id.enviarComp);

        tipoBebidaRelativeLayout = (RelativeLayout) findViewById(R.id.tipoBebida);

        comentarioAutorizacion = GlobalData.EMPTY;
        listaComentarios = new ArrayList<>();
        listaBS = new ArrayList<>();
        listaBC = new ArrayList<>();
        listaC = new ArrayList<>();

        listaBebidaConAlcohol = new ArrayList<>();
        listaBebidaSinAlcohol = new ArrayList<>();
        listaCafeteria = new ArrayList<>();

        nombreJugador.setText(jugador.getNombre_jugador());

        initListeners();

        changeToFunction(2);

        onProductSelectedListener = new OnProductSelectedListener() {
            @Override
            public void onProductSelected(View view, SelectedItemListaComp selectedItem) {
                if (contComps < GlobalData.LIMITE_PEDIDOS_COMPS && !selectedItem.getNombre_producto().isEmpty()) {
                    enviarComp.setEnabled(true);
                    limpiarLista.setEnabled(true);

                    contComps++;

                    final ItemPedidoComp itemPedido = new ItemPedidoComp();

                    itemPedido.generarIdPedido(); /* Genera un id_pedido a traves de una funcion de hash y
                                                     lo asigna a la variable id_pedido */

                    itemPedido.id_producto = selectedItem.getId_producto();
                    itemPedido.nombre_producto = selectedItem.getNombre_producto();
                    itemPedido.precio_producto = selectedItem.getPrecio_producto();
                    itemPedido.cantidad_producto = 1;
                    itemPedido.permite_ingredientes = selectedItem.isPermite_ingredientes();
                    itemPedido.permite_info_extra = selectedItem.isPermite_info_extra();
                    itemPedido.listaIngredientes = Hawk.get(GlobalData.LISTA_ORIGINAL_INGREDIENTES);
                    itemPedido.listaInfoExtra = Hawk.get(GlobalData.LISTA_ORIGINAL_INFO_EXTRA);
                    itemPedido.precio_total_productos = itemPedido.precio_producto;

                    listaPedidosComps.add(itemPedido);

                    // Actualizar el presupuesto disponible
                    updatePresupuestoComps(-itemPedido.precio_producto);

                    OnAdditionalInfoButtonPressedListener onAdditionalInfoButtonPressedListener = new OnAdditionalInfoButtonPressedListener() {
                        @Override
                        public void onAddButtonPressed(final String idPedido) {
                            final Dialog dialogAgregarInfo = Utils.getNewDialog(activity, R.layout.dialog_info_adicional_comps, false, false, 1000, ViewGroup.LayoutParams.WRAP_CONTENT);
                            final RecyclerView ingredientsRecyclerView = (RecyclerView) dialogAgregarInfo.findViewById(R.id.ingredientsRecyclerView);
                            final RecyclerView additionalInfoRecyclerView = (RecyclerView) dialogAgregarInfo.findViewById(R.id.additionalInfoRecyclerView);
                            Button btnAgregar = (Button) dialogAgregarInfo.findViewById(R.id.btnAgregar);
                            Button btnCancelar = (Button) dialogAgregarInfo.findViewById(R.id.btnCancelar);
                            TextView emptyIngredientsList = (TextView) dialogAgregarInfo.findViewById(R.id.emptyIngredientsList);
                            TextView emptyAdditionalInfoList = (TextView) dialogAgregarInfo.findViewById(R.id.emptyAdditionalInfoList);
                            final ListaPedidosCompsAdapter listaPedidosCompsAdapter = ((ListaPedidosCompsAdapter) listViewPedidos.getAdapter());

                            final ItemPedidoComp itemPedidoComp = listaPedidosCompsAdapter.getItemById(idPedido);
                            final int positionItemPedidoComp = listaPedidosCompsAdapter.getItemPosition(itemPedidoComp);

                            setUpRecyclerView(itemPedidoComp, ingredientsRecyclerView, itemPedidoComp.listaIngredientes, TipoModificadorComps.INGREDIENTE);
                            setUpRecyclerView(itemPedidoComp, additionalInfoRecyclerView, itemPedidoComp.listaInfoExtra, TipoModificadorComps.INFO_EXTRA);

                            setUpEmptyAdditionalInfoText(itemPedidoComp, emptyIngredientsList, TipoModificadorComps.INGREDIENTE);
                            setUpEmptyAdditionalInfoText(itemPedidoComp, emptyAdditionalInfoList, TipoModificadorComps.INFO_EXTRA);

                            final boolean[] ingredientesSeleccionados = new boolean[itemPedidoComp.listaIngredientes.size()];
                            final boolean[] infoExtraSeleccionada = new boolean[itemPedidoComp.listaInfoExtra.size()];
                            final int contIngredientes = itemPedido.contIngredientes;
                            final int contInfoExtra = itemPedido.contInfoExtra;

                            for (int i = 0; i < itemPedidoComp.listaIngredientes.size(); i++) {
                                ingredientesSeleccionados[i] = itemPedidoComp.listaIngredientes.get(i).isSelected();
                            }

                            for (int i = 0; i < itemPedidoComp.listaInfoExtra.size(); i++) {
                                infoExtraSeleccionada[i] = itemPedidoComp.listaInfoExtra.get(i).isSelected();
                            }

                            ItemClickSupport.addTo(ingredientsRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                                @Override
                                public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                                    if (itemPedidoComp.listaIngredientes.get(position).isSelected()) {
                                        itemPedidoComp.listaIngredientes.get(position).setSelected(false);
                                        listaPedidosCompsAdapter.updateContIngredientes(positionItemPedidoComp, -1);

                                        ingredientsRecyclerView.getAdapter().notifyDataSetChanged();
                                    } else if (itemPedidoComp.contIngredientes < GlobalData.LIMITE_INGREDIENTES_COMPS) {

                                        itemPedidoComp.listaIngredientes.get(position).setSelected(true);

                                        listaPedidosCompsAdapter.updateContIngredientes(positionItemPedidoComp, 1);

                                        ingredientsRecyclerView.getAdapter().notifyDataSetChanged();

                                    }
                                }
                            });

                            ItemClickSupport.addTo(additionalInfoRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                                @Override
                                public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                                    if (itemPedidoComp.listaInfoExtra.get(position).isSelected()) {
                                        itemPedidoComp.listaInfoExtra.get(position).setSelected(false);
                                        listaPedidosCompsAdapter.updateContInfoExtra(positionItemPedidoComp, -1);

                                        additionalInfoRecyclerView.getAdapter().notifyDataSetChanged();
                                    } else if (itemPedidoComp.contInfoExtra < GlobalData.LIMITE_INFO_EXTRA_COMPS) {
                                        {
                                            itemPedidoComp.listaInfoExtra.get(position).setSelected(true);

                                            listaPedidosCompsAdapter.updateContInfoExtra(positionItemPedidoComp, 1);

                                            additionalInfoRecyclerView.getAdapter().notifyDataSetChanged();
                                        }
                                    }
                                }
                            });

                            btnAgregar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    listaPedidosComps.get(positionItemPedidoComp).listaIngredientes = itemPedidoComp.listaIngredientes;
                                    listaPedidosComps.get(positionItemPedidoComp).listaInfoExtra = itemPedidoComp.listaInfoExtra;

                                    String additionalInfoStr = GlobalData.EMPTY;

                                    if (itemPedidoComp.listaIngredientes != null && itemPedidoComp.listaIngredientes.size() > 0) {
                                        for (int i = 0; i < itemPedidoComp.listaIngredientes.size(); i++) {
                                            if (itemPedidoComp.listaIngredientes.get(i).isSelected()) {
                                                if (additionalInfoStr.isEmpty()) {
                                                    additionalInfoStr = capitalizeWord(itemPedidoComp.listaIngredientes.get(i).getNombreModificador(), TipoModificadorComps.INGREDIENTE);
                                                }
                                                else {
                                                    if (itemPedidoComp.listaIngredientes.get(i).isSelected()) {
                                                        additionalInfoStr = additionalInfoStr + " - " + capitalizeWord(itemPedidoComp.listaIngredientes.get(i).getNombreModificador(), TipoModificadorComps.INGREDIENTE);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (itemPedidoComp.listaInfoExtra != null && itemPedidoComp.listaInfoExtra.size() > 0) {
                                        for (int i = 0; i < itemPedidoComp.listaInfoExtra.size(); i++) {
                                            if (itemPedidoComp.listaInfoExtra.get(i).isSelected()) {
                                                if (additionalInfoStr.isEmpty()) {
                                                    additionalInfoStr = capitalizeWord(itemPedidoComp.listaInfoExtra.get(i).getNombreModificador(), TipoModificadorComps.INFO_EXTRA);
                                                }
                                                else {
                                                    additionalInfoStr = additionalInfoStr + " - " + capitalizeWord(itemPedidoComp.listaInfoExtra.get(i).getNombreModificador(), TipoModificadorComps.INFO_EXTRA);
                                                }
                                            }
                                        }
                                    }

                                    listaPedidosComps.get(positionItemPedidoComp).setTextEmptyInfoList(additionalInfoStr.isEmpty() ? getString(R.string.empty_additional_info_list) : additionalInfoStr);

                                    dialogAgregarInfo.dismiss();
                                }
                            });

                                btnCancelar.setOnClickListener(new View.OnClickListener()

                                {
                                    @Override
                                    public void onClick(View view) {

                                        itemPedidoComp.contIngredientes = contIngredientes;
                                        itemPedidoComp.contInfoExtra = contInfoExtra;

                                        listaPedidosCompsAdapter.setContIngredientes(positionItemPedidoComp, contIngredientes);
                                        listaPedidosCompsAdapter.setContInfoExtra(positionItemPedidoComp, contInfoExtra);

                                        for (int i = 0; i < itemPedidoComp.listaIngredientes.size(); i++) {
                                            itemPedidoComp.listaIngredientes.get(i).setSelected(ingredientesSeleccionados[i]);
                                        }

                                        for (int i = 0; i < itemPedidoComp.listaInfoExtra.size(); i++) {
                                            itemPedidoComp.listaInfoExtra.get(i).setSelected(infoExtraSeleccionada[i]);
                                        }

                                        dialogAgregarInfo.dismiss();
                                    }
                                });

                                dialogAgregarInfo.setOnDismissListener(new DialogInterface.OnDismissListener()

                                {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        listaPedidosCompsAdapter.notifyDataSetChanged();
                                    }
                                });

                            dialogAgregarInfo.show();
                        }
                    };

                    adapterListaPedidosComps = new ListaPedidosCompsAdapter(listaPedidosComps, activity, onAdditionalInfoButtonPressedListener);

                    listViewPedidos.setAdapter(adapterListaPedidosComps);
                }
                else if (contComps >= GlobalData.LIMITE_PEDIDOS_COMPS) {
                    Utils.showInformationDialog(activity, false, GlobalData.ERROR_COMPS_LIMIT_REACHED.replace("#_COMPS_LIMIT_VALUE_#", String.valueOf(GlobalData.LIMITE_PEDIDOS_COMPS)), 1200);
                }
            }
        };
    }

    public static String capitalizeWord(String word, TipoModificadorComps tipoModificadorComps) {
        word = word.trim();

        if(word.isEmpty()) {
            return word;
        }
        else if(word.length() == 1) {
            return word.toUpperCase();
        }
        else {
            String firstLetter = String.valueOf(word.charAt(0)).toUpperCase();
            String restOfWord = word.substring(1);//.toLowerCase();

            word = firstLetter + restOfWord;
        }

        if(tipoModificadorComps == TipoModificadorComps.INGREDIENTE) {
            word = "<font color=\"#8f13fd\">" + word + "</font>";
        }
        else {
            word = "<font color=\"#f5a623\">" + word + "</font>";
        }

        return word;
    }

    private void setUpRecyclerView(ItemPedidoComp itemPedidoComp, RecyclerView recyclerView, ArrayList<ModificadorCompDM> listaModificadores, TipoModificadorComps tipoModificadorComps) {
        LinearLayoutManager llm = new LinearLayoutManager(activity);

        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(llm);

        if(tipoModificadorComps == TipoModificadorComps.INGREDIENTE) {
            if(itemPedidoComp.permite_ingredientes) {
                recyclerView.setAdapter(new ListaInfoExtraAdapter(activity, listaModificadores));
            }
            else {
                recyclerView.setAdapter(null);
            }
        }
        else {
            if(itemPedidoComp.permite_info_extra) {
                recyclerView.setAdapter(new ListaInfoExtraAdapter(activity, listaModificadores));
            }
            else {
                recyclerView.setAdapter(null);
            }
        }
    }

    private void setUpEmptyAdditionalInfoText(ItemPedidoComp itemPedidoComp, TextView emptyList, TipoModificadorComps tipoModificador) {
        String emptyListStr = GlobalData.EMPTY;
        ArrayList<ModificadorCompDM> listaAux;

        if(tipoModificador == TipoModificadorComps.INGREDIENTE) {
            if(!itemPedidoComp.permite_ingredientes) {
                emptyListStr = getString(R.string.ingredients_not_allowed);
            }

            listaAux = itemPedidoComp.listaIngredientes;
        }
        else {
            if(!itemPedidoComp.permite_info_extra) {
                emptyListStr = getString(R.string.additional_info_not_allowed);
            }

            listaAux = itemPedidoComp.listaInfoExtra;
        }

        if(listaAux.isEmpty()) {
            emptyListStr = getString(R.string.empty_list);
        }

        emptyList.setText(emptyListStr);
        emptyList.setVisibility(emptyListStr.isEmpty() ? View.GONE : View.VISIBLE);
    }

    private void initDialogsViews() {
        dialogEnterNewUser = Utils.getNewDialog(this, R.layout.dialog_ingresar_nuevo_usuario, true, 500, 250);
        dialogOpcionesJefeMesa = Utils.getNewDialog(this, R.layout.dialog_opciones, true, 600, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogMostrarValoresParametros = Utils.getNewDialog(this, R.layout.dialog_opciones, true, 600, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogIngresarMotivosCompFueraRegla = Utils.getNewDialog(this, R.layout.dialog_ingresar_motivo_autorizacion_comp, false, ViewGroup.LayoutParams.MATCH_PARENT, 550);
    }

    private void initListeners() {
        bebestibles.setOnClickListener(this);
        conAlcohol.setOnClickListener(this);
        sinAlcohol.setOnClickListener(this);
        cafeteria.setOnClickListener(this);
        volver.setOnClickListener(this);
        limpiarLista.setOnClickListener(this);
        enviarComp.setOnClickListener(this);
        btnOpciones.setOnClickListener(this);
    }

    private void setTableName(TextView btnTableName, String tableName) {
        btnTableName.setText(tableName);
        btnTableName.setBackgroundColor(getResources().getColor(R.color.mesaBlackjack));
        btnTableName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cards, 0, 0, 0);
    }

    private void setHeaderInfo() {
        ImageView appLogo = (ImageView) findViewById(R.id.logo);
        TextView tableName = (TextView) findViewById(R.id.tableName);
        currentTime = (TextView) findViewById(R.id.currentTime);
        Button jefemesaName = (Button) findViewById(R.id.jefemesaName);
        Button croupierName = (Button) findViewById(R.id.croupierName);
        btnOpciones = (ImageButton) findViewById(R.id.options);

        setTableName(tableName, mesa.getNombre_completo_mesa());
        jefemesaName.setText(jefeMesa.getNombre_usuario());
        croupierName.setText(croupier.getNombre_usuario());

        setCurrentTime24Hours();

        appLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.getAbout(activity, 400, 600, true).show();
            }
        });
    }

    private void updatePresupuestoComps(long valor) {
        presupuesto_comps += valor;

        presupuestoTextView.setText(activity.getString(R.string.available_estimation_default_text).replace("$---", Utils.moneyFormat(presupuesto_comps)));

        requiere_autorizacion = presupuesto_comps < 0 || requiere_autorizacion_inicial;
    }

    private void getProducts() {
        ServicioMesas.AsyncResponse getCompsAsyncResponse = new ServicioMesas.AsyncResponse() {
            @Override
            public void onProcessFinish(Object result) {
                ResultCompsDM resultComps = (ResultCompsDM) result;

                if (resultComps != null) {
                    ResultListCompsDM resultListComps = resultComps.resultListComps;
                    final ArrayList<ModificadorCompDM> ingredientsComps = resultComps.ingredientsComps;
                    final ArrayList<ModificadorCompDM> additionalInfoComps = resultComps.additionalInfoComps;

                    if (resultListComps != null) {
                        codigo_rvc = resultListComps.codigo_rvc;
                        presupuesto_inicial_comps = presupuesto_comps = resultListComps.presupuesto_cliente;

                        // Actualizar datos de presupuesto
                        presupuestoTextView.setText(getString(R.string.available_estimation_default_text).replace("$---", Utils.moneyFormat(presupuesto_comps)));

                        for (CategoriaCompsDM categoriaComp : resultListComps.getListaCategoriaComps()) {
                            for (CompDM comp : categoriaComp.getListaComps()) {
                                String nombreCategoriaComp = categoriaComp.nombre_categoria.toUpperCase(Utils.locale).replace("BEBIDAS ", GlobalData.EMPTY);

                                if (categoriaComp.codigo_categoria.equals(GlobalData.ID_CATEGORIA_COMP_BEBIDA_CON_ALCOHOL)) {
                                    conAlcohol.setText(nombreCategoriaComp);

                                    listaBC.add(comp);
                                } else if (categoriaComp.codigo_categoria.equals(GlobalData.ID_CATEGORIA_COMP_BEBIDA_SIN_ALCOHOL)) {
                                    sinAlcohol.setText(nombreCategoriaComp);

                                    listaBS.add(comp);
                                } else if (categoriaComp.codigo_categoria.equals(GlobalData.ID_CATEGORIA_COMP_CAFETERIA)) {
                                    cafeteria.setText(nombreCategoriaComp);

                                    listaC.add(comp);
                                }
                            }
                        }

                        listaBebidaConAlcohol = getItemsArrayListCompDM(listaBC);
                        listaBebidaSinAlcohol = getItemsArrayListCompDM(listaBS);
                        listaCafeteria = getItemsArrayListCompDM(listaC);

                        changeToFunction(2);

                        ListaProductosCompsAdapter productosCompsAdapter = new ListaProductosCompsAdapter(listaBebidaConAlcohol, activity, onProductSelectedListener);

                        listViewProductos.setAdapter(productosCompsAdapter);

                        SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(listViewPedidos, new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return USER_CAN_DELETE_ITEMS_FROM_LIST;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                try {
                                    adapterListaPedidosComps = (ListaPedidosCompsAdapter) listViewPedidos.getAdapter();

                                    for (int position : reverseSortedPositions) {
                                        long precioTotalProducto = listaPedidosComps.get(position).precio_total_productos;

                                        listaPedidosComps.remove(position);

                                        contComps--;

                                        // Actualizar el presupuesto disponible
                                        updatePresupuestoComps(precioTotalProducto);
                                    }

                                    if (adapterListaPedidosComps.getCount() == 0) {
                                        clearData();
                                    }

                                    adapterListaPedidosComps.notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();

                                    Utils.toast(activity, GlobalData.ERROR_DELETING_PRODUCT_OF_LIST);
                                }
                            }
                        });

                        listViewPedidos.setOnTouchListener(touchListener);
                        listViewPedidos.setOnScrollListener(touchListener.makeScrollListener());
                    } else {
                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_GETTING_COMPS_LIST, 1200);

                        listViewProductos.setAdapter(null);
                    }

                    if (ingredientsComps != null) {
                        Hawk.put(GlobalData.LISTA_ORIGINAL_INGREDIENTES, ingredientsComps);
                    } else {
                        Hawk.put(GlobalData.LISTA_ORIGINAL_INGREDIENTES, new ArrayList<>());

                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_GETTING_INGREDIENTS_COMPS_LIST, 1200);
                    }

                    if (additionalInfoComps != null) {
                        Hawk.put(GlobalData.LISTA_ORIGINAL_INFO_EXTRA, additionalInfoComps);
                    } else {
                        Hawk.put(GlobalData.LISTA_ORIGINAL_INFO_EXTRA, new ArrayList<>());

                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_GETTING_ADDITIONAL_INFO_COMPS_LIST, 1200);
                    }


                } else {
                    Utils.showInformationDialog(activity, false, GlobalData.ERROR_GETTING_DATA, 1200);

                    listViewProductos.setAdapter(null);
                }
            }
        };

        String idMcc = "-1";

        if (!jugador.getId_mcc_jugador().isEmpty()) idMcc = jugador.getId_mcc_jugador();

        ServicioMesas.GetCompsAsyncTask gcm = new ServicioMesas.GetCompsAsyncTask(Utils.getNewProgressDialog(activity, getString(R.string.getting_products_list), false), getCompsAsyncResponse);

        gcm.execute(GlobalData.ID_UNIDAD, mesa.getId_mesa(), idMcc, jugador.getId_categoria_jugador(), GlobalData.TOKEN_PRODUCTOS_COMPS);
    }

    private ArrayList<ItemListaComp> getItemsArrayListCompDM(ArrayList<CompDM> lista) {
        int i, j;
        ArrayList<ItemListaComp> listaItems = new ArrayList<>();

        for (i = 0; i < lista.size(); i++) {
            ItemListaComp item = new ItemListaComp();
            CompDM comp = lista.get(i);

            for (j = 0; j < 2; j++) {
                if (Utils.isEvenNumber(j)) {
                    item.setIdProd1(comp.getIdComp());
                    item.setNombreProd1(comp.getNombreComp());
                    item.setPrecioProd1(comp.getPrecioTotalComp());
                    item.setPermiteIngredientesProd1(comp.isPermiteIngredientes());
                    item.setPermiteInfoExtraProd1(comp.isPermiteInfoExtra());
                } else {
                    if ((i + 1) < lista.size()) {
                        i++;
                        comp = lista.get(i);
                        item.setIdProd2(comp.getIdComp());
                        item.setNombreProd2(comp.getNombreComp());
                        item.setPrecioProd2(comp.getPrecioTotalComp());
                        item.setPermiteIngredientesProd2(comp.isPermiteIngredientes());
                        item.setPermiteInfoExtraProd2(comp.isPermiteInfoExtra());
                    } else {
                        item.setIdProd2(GlobalData.EMPTY);
                        item.setNombreProd2(GlobalData.EMPTY);
                        item.setPrecioProd2(0);
                        item.setPermiteIngredientesProd2(false);
                        item.setPermiteInfoExtraProd2(false);
                    }
                }
            }

            listaItems.add(item);
        }

        return listaItems;
    }

    public void setButtonState(Button button, boolean pressed) {
        if (pressed) {
            button.setTextColor(getResources().getColor(R.color.blanco));
            button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        } else {
            button.setTextColor(getResources().getColor(R.color.dark_grey_text_color));
            button.setBackgroundColor(getResources().getColor(R.color.blanco));
        }
    }

    public void setDrinkButtonState(Button button, boolean pressed) {
        if (pressed) {
            button.setTextColor(getResources().getColor(R.color.blanco));
            button.setBackgroundColor(getResources().getColor(R.color.dark_grey_text_color));
        } else {
            button.setTextColor(getResources().getColor(R.color.dark_grey_text_color));
            button.setBackgroundColor(getResources().getColor(R.color.blanco));
        }

    }

    private void stopCurrentTimeHandler() {
        if (getCurrentTimeHandler != null)
            getCurrentTimeHandler.removeCallbacks(getCurrentTimeRunnable);
    }

    private void setCurrentTime24Hours() {
        getCurrentTimeHandler = new Handler();

        getCurrentTimeRunnable = new Runnable() {
            public void run() {
                currentTime.setText(Utils.getCurrentTime());

                getCurrentTimeHandler.postDelayed(this, 1000);
            }
        };

        getCurrentTimeHandler.postDelayed(getCurrentTimeRunnable, 1000);
    }

    private void changeToFunction(int function) {
        switch (function) {
            case 1:
                setButtonState(bebestibles, false);
                setButtonState(cafeteria, false);
                break;

            case 2:
                setButtonState(bebestibles, true);
                setButtonState(cafeteria, false);

                setDrinkButtonState(conAlcohol, true);
                setDrinkButtonState(sinAlcohol, false);

                emptyCompsCategory.setVisibility((listaBebidaConAlcohol.isEmpty()) ? View.VISIBLE : View.GONE);
                break;

            case 3:
                setButtonState(bebestibles, true);
                setButtonState(cafeteria, false);

                setDrinkButtonState(conAlcohol, false);
                setDrinkButtonState(sinAlcohol, true);

                emptyCompsCategory.setVisibility((listaBebidaSinAlcohol.isEmpty()) ? View.VISIBLE : View.GONE);
                break;

            case 4:
                setButtonState(bebestibles, false);
                setButtonState(cafeteria, true);

                emptyCompsCategory.setVisibility((listaCafeteria.isEmpty()) ? View.VISIBLE : View.GONE);
                break;
        }
    }

    public void clearData() {
        requiere_autorizacion = requiere_autorizacion_inicial;
        presupuesto_comps = presupuesto_inicial_comps;
        presupuestoTextView.setText(getString(R.string.available_estimation_default_text).replace("$---", Utils.moneyFormat(presupuesto_comps)));
        listaPedidosComps.clear();
        listViewPedidos.setAdapter(null);
        contComps = 0;
        enviarComp.setEnabled(false);
        limpiarLista.setEnabled(false);
    }

    void sendComps(String id_mesa, String idJefeSalon, String idJefeMesa, String codigo_rvc, ArrayList<ItemPedidoComp> listaPedidosComps,
                   int tipoComp, JugadorDM jugador, String comentario) {

        try {
            long monto_total = 0;
            String listaPedidosStr = "[";

            for (ItemPedidoComp item : listaPedidosComps) {
                String idProducto = "\"Id\" : ";
                String nombreProducto = "\"Nombre\" : ";
                String cantidadProducto = "\"Cantidad\" : ";
                String precioProducto = "\"Precio\" : ";
                String ingredientesArray = "\"Ingredientes\" : [";
                String infoExtraArray = "\"InfoExtra\" : [";
                ArrayList<ModificadorCompDM> listaIngredientesSeleccionados = new ArrayList<>();
                ArrayList<ModificadorCompDM> listaInfoExtraSeleccionada = new ArrayList<>();

                for (ModificadorCompDM ingredienteSeleccionado : item.listaIngredientes) {
                    if (ingredienteSeleccionado.isSelected()) {
                        listaIngredientesSeleccionados.add(ingredienteSeleccionado);
                    }
                }

                for (ModificadorCompDM infoExtraSeleccionada : item.listaInfoExtra) {
                    if (infoExtraSeleccionada.isSelected()) {
                        listaInfoExtraSeleccionada.add(infoExtraSeleccionada);
                    }
                }

                listaPedidosStr += "{";

                idProducto += item.id_producto + ", ";
                nombreProducto += "\"" + item.nombre_producto + "\", ";
                cantidadProducto += item.cantidad_producto + ", ";
                precioProducto += item.precio_producto + ", ";

                for (ModificadorCompDM ingrediente : listaIngredientesSeleccionados) {
                    if (ingrediente.isSelected()) {
                        String idIngrediente = "\"Id\" : ";
                        String nombreIngrediente = "\"Nombre\" : ";
                        String cantidadIngrediente = "\"Cantidad\" : ";
                        String precioIngrediente = "\"Precio\" : ";

                        ingredientesArray += "{";

                        idIngrediente += ingrediente.getIdModificador() + ", ";
                        nombreIngrediente += "\"" + ingrediente.getNombreModificador() + "\", ";
                        cantidadIngrediente += 1 + ", ";
                        precioIngrediente += ingrediente.getPrecioAdicionalModificador();

                        ingredientesArray += idIngrediente + nombreIngrediente + cantidadIngrediente + precioIngrediente;

                        ingredientesArray += ((listaIngredientesSeleccionados.indexOf(ingrediente) == (listaIngredientesSeleccionados.size() - 1)) ? "}" : "}, ");
                    }
                }

                ingredientesArray += "],";

                for (ModificadorCompDM infoExtra : listaInfoExtraSeleccionada) {
                    if (infoExtra.isSelected()) {
                        String idInfoExtra = "\"Id\" : ";
                        String nombreInfoExtra = "\"Nombre\" : ";
                        String cantidadInfoExtra = "\"Cantidad\" : ";
                        String precioInfoExtra = "\"Precio\" : ";

                        infoExtraArray += "{";

                        idInfoExtra += infoExtra.getIdModificador() + ", ";
                        nombreInfoExtra += "\"" + infoExtra.getNombreModificador() + "\", ";
                        cantidadInfoExtra += 1 + ", ";
                        precioInfoExtra += infoExtra.getPrecioAdicionalModificador();

                        infoExtraArray += idInfoExtra + nombreInfoExtra + cantidadInfoExtra + precioInfoExtra;

                        infoExtraArray += ((listaInfoExtraSeleccionada.indexOf(infoExtra) == (listaInfoExtraSeleccionada.size() - 1)) ? "}" : "}, ");
                    }
                }

                infoExtraArray += "]";

                monto_total += item.precio_total_productos;

                listaPedidosStr += idProducto + nombreProducto + cantidadProducto + precioProducto + ingredientesArray + infoExtraArray;

                listaPedidosStr += ((listaPedidosComps.indexOf(item) == (listaPedidosComps.size() - 1)) ? "}]" : "}, ");
            }

            Logger.d(listaPedidosStr);

            ServicioMesas.AsyncResponse insertCompsOrderAsynResponse = new ServicioMesas.AsyncResponse() {
                @Override
                public void onProcessFinish(Object result) {
                    ResultInsertCompsOrder resultInsertCompsOrder = (ResultInsertCompsOrder) result;

                    if (resultInsertCompsOrder != null) {
                        if (resultInsertCompsOrder.resultado.equals("OK")) {
                            final Dialog dialog = Utils.getSuccessOperationDialog(activity, false, GlobalData.SUCCESS_COMP_SENT);

                            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    stopCurrentTimeHandler();

                                    GlobalData.MUST_CHECK_IF_AVAILABLE_COMPS = true;

                                    finish();
                                }
                            });

                            dialog.show();

                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    dialog.dismiss();
                                }
                            }, 1200);
                        } else {
                            Utils.showInformationDialog(activity, true, resultInsertCompsOrder.mensaje_resultado.toUpperCase(Utils.locale), 1200);
                        }
                    }
                }
            };

            ServicioMesas.InsertCompsOrderAsyncTask ico = new ServicioMesas.InsertCompsOrderAsyncTask(Utils.getNewProgressDialog(activity, getString(R.string.registering_comps_order), false), insertCompsOrderAsynResponse);

            ico.execute(GlobalData.ID_UNIDAD, GlobalData.TOKEN_PRODUCTOS_COMPS, id_mesa, jugador.getId_mcc_jugador(),
                        jugador.getId_juego(), jugador.getNombre_jugador(), jugador.getRut_jugador(), jugador.getId_categoria_jugador(),
                        idJefeMesa, idJefeSalon, comentario, String.valueOf(monto_total), codigo_rvc, listaPedidosStr, String.valueOf(tipoComp));
        }
        catch(Exception e) {
            e.printStackTrace();

            Toast.makeText(getApplicationContext(), getString(R.string.error_sending_comps_order), Toast.LENGTH_SHORT).show();

            Utils.createErrorFile(e);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == volver) {
            stopCurrentTimeHandler();

            finish();
        } else if (view == limpiarLista) {
            clearData();
        } else if (view == enviarComp) {
            ConfirmationDialogController confirmationDialogController = new ConfirmationDialogController(activity);

            confirmationDialogController.showConfirmationDialog(-1, getString(R.string.send_comp_question), new OnButtonPressedListener() {
                @Override
                public void onButtonAcceptPressed() {
                    try {
                        if (requiere_autorizacion) {
                            ImageView card = (ImageView) dialogEnterNewUser.findViewById(R.id.card);

                            TextView textJefeSalon = (TextView) dialogEnterNewUser.findViewById(R.id.userText);

                            Utils.startViewAnimation(activity, card, R.anim.tarjeta_login);

                            textJefeSalon.setText(getString(R.string.jefe_salon_text));

                            dialogEnterNewUser.show();

                            DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
                                @Override
                                public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                                    if (event.getAction() == KeyEvent.ACTION_UP) {
                                        if (Utils.isAlfanumeric(String.valueOf(event.getDisplayLabel()))) {
                                            digits += String.valueOf(event.getDisplayLabel());
                                        }

                                        if (keyCode == KeyEvent.KEYCODE_ENTER) {
                                            try {
                                                String id_tarjeta = digits.substring(GlobalData.MIN_CARD_INDEX, GlobalData.MAX_CARD_INDEX); //00000000153

                                                ServicioMesas.AsyncResponse getUsuarioAsyncResponse = new ServicioMesas.AsyncResponse() {
                                                    @Override
                                                    public void onProcessFinish(Object result) {
                                                        final UsuarioDM user = (UsuarioDM) result;

                                                        if (user != null) {
                                                            jefeSalon = user;

                                                            dialogEnterNewUser.dismiss();

                                                            ServicioMesas.AsyncResponse asyncResponseListaComentarios = new ServicioMesas.AsyncResponse() {
                                                                @SuppressWarnings("unchecked")
                                                                @Override
                                                                public void onProcessFinish(Object result) {
                                                                    ArrayList<MotivosAutorizacionCompsDM> resultGetComentarios = (ArrayList<MotivosAutorizacionCompsDM>) result;

                                                                    listaComentarios.clear();

                                                                    if (resultGetComentarios != null) {
                                                                        for (int i = 0; i < resultGetComentarios.size(); i++) {
                                                                            MotivosAutorizacionCompsDM motivosAutorizacionComps = resultGetComentarios.get(i);

                                                                            listaComentarios.add(motivosAutorizacionComps.descripcion_motivo_autorizacion);
                                                                        }

                                                                        Collections.sort(listaComentarios);

                                                                        listaComentarios.add(0, getString(R.string.no_comments_text));

                                                                        Button cancelar = (Button) dialogIngresarMotivosCompFueraRegla.findViewById(R.id.cancelar);
                                                                        Button enviar = (Button) dialogIngresarMotivosCompFueraRegla.findViewById(R.id.enviar);

                                                                        dialogIngresarMotivosCompFueraRegla.show();

                                                                        final ListView presetCommentsList = (ListView) dialogIngresarMotivosCompFueraRegla.findViewById(R.id.presetCommentsList);

                                                                        presetCommentsList.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_single_choice, listaComentarios) {
                                                                            @Override
                                                                            public View getView(int position, View convertView, ViewGroup parent) {
                                                                                TextView textView = (TextView) super.getView(position, convertView, parent);

                                                                                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
                                                                                textView.setTextColor(getResources().getColor(R.color.dark_grey_text_color));

                                                                                return textView;
                                                                            }
                                                                        });

                                                                        presetCommentsList.setItemChecked(0, true);

                                                                        comentarioAutorizacion = presetCommentsList.getItemAtPosition(0).toString();

                                                                        presetCommentsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                                            @Override
                                                                            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                                                                                comentarioAutorizacion = parent.getItemAtPosition(position).toString();
                                                                            }
                                                                        });

                                                                        cancelar.setOnClickListener(new View.OnClickListener() {
                                                                            @Override
                                                                            public void onClick(View arg0) {
                                                                                dialogIngresarMotivosCompFueraRegla.dismiss();
                                                                            }
                                                                        });

                                                                        enviar.setOnClickListener(new View.OnClickListener() {
                                                                            @Override
                                                                            public void onClick(View arg0) {
                                                                                dialogIngresarMotivosCompFueraRegla.dismiss();

                                                                                digits = GlobalData.EMPTY;

                                                                                Logger.d("comentarioAutorizacion= ", comentarioAutorizacion);

                                                                                int tipoComp = 1;

                                                                                if (requiere_autorizacion && !requiere_autorizacion_inicial) {
                                                                                    tipoComp = 0;
                                                                                }

                                                                                sendComps(mesa.getId_mesa(), jefeSalon.getId_usuario(), jefeMesa.getId_usuario(), codigo_rvc, listaPedidosComps, tipoComp, jugador, comentarioAutorizacion);
                                                                            }
                                                                        });
                                                                    } else {
                                                                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_AUTHORIZATION_REASONS_LIST, 1200);
                                                                    }
                                                                }
                                                            };

                                                            ServicioMesas.GetComentariosAsyncTask gca = new ServicioMesas.GetComentariosAsyncTask(Utils.getNewProgressDialog(activity, getString(R.string.loading_authorization_reasons), false), asyncResponseListaComentarios);

                                                            gca.execute();
                                                        } else {
                                                            Utils.showInformationDialog(activity, false, GlobalData.EMPTY, 1200);
                                                        }

                                                        digits = GlobalData.EMPTY;
                                                    }
                                                };

                                                ServicioMesas.GetUsuarioAsyncTask gu = new ServicioMesas.GetUsuarioAsyncTask(Utils.getNewProgressDialog(activity, getString(R.string.getting_user_info), false), getUsuarioAsyncResponse);

                                                gu.execute(GlobalData.ID_UNIDAD, id_tarjeta, GlobalData.USUARIO_JEFE_SALON);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                    return false;
                                }
                            };

                            dialogEnterNewUser.setOnKeyListener(keyListener);
                        } else {
                            sendComps(mesa.getId_mesa(), "-1", jefeMesa.getId_usuario(), codigo_rvc, listaPedidosComps, 0, jugador, GlobalData.EMPTY);
                        }
                    }
                    catch(Exception e) {
                        e.printStackTrace();

                        Toast.makeText(getApplicationContext(), getString(R.string.error_sending_comps_order), Toast.LENGTH_SHORT).show();

                        Utils.createErrorFile(e);
                    }
                }
            });
        } else if (view == btnOpciones) {
            dialogOpcionesJefeMesa.show();

            Button refreshParams = (Button) dialogOpcionesJefeMesa.findViewById(R.id.btnOption1);
            Button showParamsValues = (Button) dialogOpcionesJefeMesa.findViewById(R.id.btnOption2);
            Button refreshProductsLists = (Button) dialogOpcionesJefeMesa.findViewById(R.id.btnOption3);

            refreshParams.setText(getString(R.string.get_params_from_database));

            showParamsValues.setVisibility(View.VISIBLE);
            showParamsValues.setText(getString(R.string.show_params_values));

            refreshProductsLists.setVisibility(View.VISIBLE);
            refreshProductsLists.setText(getString(R.string.get_products_lists));

            refreshParams.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean successGetParams = Utils.getParamsFromDB(GlobalData.ID_UNIDAD);

                    if (successGetParams) {
                        Utils.showSuccessOperationDialog(activity, false, GlobalData.EMPTY, 1200);

                        if (contComps > GlobalData.LIMITE_PEDIDOS_COMPS) {
                            clearData();
                        }
                    } else {
                        Utils.showInformationDialog(activity, false, GlobalData.ERROR_GETTING_PARAMS, 1200);
                    }

                    dialogOpcionesJefeMesa.dismiss();
                }
            });

            showParamsValues.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button param1 = (Button) dialogMostrarValoresParametros.findViewById(R.id.btnOption1);
                    Button param2 = (Button) dialogMostrarValoresParametros.findViewById(R.id.btnOption2);
                    Button param3 = (Button) dialogMostrarValoresParametros.findViewById(R.id.btnOption3);

                    param3.setVisibility(View.VISIBLE);

                    param1.setEnabled(false);
                    param2.setEnabled(false);
                    param3.setEnabled(false);

                    param1.setText(String.format("%s %d", getString(R.string.comps_limit_text), GlobalData.LIMITE_PEDIDOS_COMPS));
                    param2.setText(String.format("%s %d", getString(R.string.ingredients_limit_text), GlobalData.LIMITE_INGREDIENTES_COMPS));
                    param3.setText(String.format("%s %d", getString(R.string.additional_info_limit_text), GlobalData.LIMITE_INFO_EXTRA_COMPS));

                    dialogMostrarValoresParametros.show();
                }
            });

            refreshProductsLists.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogOpcionesJefeMesa.dismiss();

                    clearData();

                    listaBC.clear();
                    listaBS.clear();
                    listaC.clear();

                    listaBebidaConAlcohol.clear();
                    listaBebidaSinAlcohol.clear();
                    listaCafeteria.clear();

                    getProducts();
                }
            });
        } else if (view == bebestibles) {
            changeToFunction(2);

            tipoBebidaRelativeLayout.setVisibility(RelativeLayout.VISIBLE);
            listViewProductos.setAdapter(new ListaProductosCompsAdapter(listaBebidaConAlcohol, activity, onProductSelectedListener));
            setDrinkButtonState(conAlcohol, true);
            setDrinkButtonState(sinAlcohol, false);
        } else if (view == conAlcohol) {
            changeToFunction(2);

            listViewProductos.setAdapter(new ListaProductosCompsAdapter(listaBebidaConAlcohol, activity, onProductSelectedListener));
        } else if (view == sinAlcohol) {
            changeToFunction(3);

            listViewProductos.setAdapter(new ListaProductosCompsAdapter(listaBebidaSinAlcohol, activity, onProductSelectedListener));
        } else if (view == cafeteria) {
            changeToFunction(4);

            tipoBebidaRelativeLayout.setVisibility(RelativeLayout.GONE);
            listViewProductos.setAdapter(new ListaProductosCompsAdapter(listaCafeteria, activity, onProductSelectedListener));
        }
    }
}