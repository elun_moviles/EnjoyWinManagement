package com.jumpitt.winmanagement.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.JugadorDM;
import com.jumpitt.winmanagement.dataModel.MesaDM;
import com.jumpitt.winmanagement.dataModel.PosicionesJugadoresMesaDM;
import com.jumpitt.winmanagement.dataModel.SesionDM;
import com.jumpitt.winmanagement.dataModel.TipoJuegoDM;
import com.jumpitt.winmanagement.dataModel.UsuarioDM;
import com.jumpitt.winmanagement.dataModel.enums.PlayerTypes;
import com.jumpitt.winmanagement.databaseConnection.ConnectionServer;
import com.jumpitt.winmanagement.listeners.JuegosOnItemClickListener;
import com.jumpitt.winmanagement.listeners.MesasOnItemClickListener;
import com.jumpitt.winmanagement.listeners.interfaces.OnGameSelectedListener;
import com.jumpitt.winmanagement.listeners.interfaces.OnTableSelectedListener;
import com.jumpitt.winmanagement.lists.adapters.ListaMesasAdapter;
import com.jumpitt.winmanagement.lists.adapters.TipoJuegoAdapter;
import com.jumpitt.winmanagement.ui.comps.CompsActivity;
import com.jumpitt.winmanagement.utils.BaseActivity;
import com.jumpitt.winmanagement.utils.GlobalData;
import com.jumpitt.winmanagement.utils.Utils;
import com.jumpitt.winmanagement.utils.controllers.MinMaxValuesController;
import com.jumpitt.winmanagement.utils.interfaces.OnMinMaxValuesEnteredListener;
import com.jumpitt.winmanagement.webservices.ServicioMesas;
import com.jumpitt.winmanagement.webservices.ServicioMesas.AsyncResponse;
import com.jumpitt.winmanagement.webservices.ServicioMesas.GetMesasAsyncTask;
import com.jumpitt.winmanagement.webservices.ServicioMesas.GetUsuarioAsyncTask;
import com.orhanobut.logger.Logger;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class LoginActivity extends BaseActivity implements View.OnClickListener, OnGameSelectedListener, OnTableSelectedListener {
    private GlobalData gd;
    private String digits;
    private String userType;
    private Button btnOpenTable;
    private Button btnAccept;
    private Button btnCancel;
    private TextView titleDialogSelectTable;
    private TextView emptyGamesListTextView;
    private TextView emptyTablesListTextView;
    private Dialog dialogSeleccionarMesa;
    private ListView juegosListView;
    private GridView mesasGridView;
    private OnGameSelectedListener listenerJuegos;
    private OnTableSelectedListener listenerMesas;
    private ListaMesasAdapter adapterMesas;
    private MinMaxValuesController minMaxValuesController;
    public static SesionDM sesion;
    private ImageView logo;
    private int cont = 0;
    private String androidId;
    private int currentApiVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_login);
        androidId = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
        initViews();
        goneViewHideen();


    }


    // Inicializar la app
    private void initViews() {
        // Establecer servidor que se va a utilizar
        ConnectionServer.setAppEnvironment(ConnectionServer.Environment.PRODUCTIVO);
//        ConnectionServer.setAppEnvironment(ConnectionServer.Environment.DESARROLLO);
        // Establecer la version de la app
        setAppVersion();

        // Iniciar animacion de la ic_tarjeta
        startCardAnimation();

        gd = new GlobalData(getApplicationContext());

        clearVars();

        dialogSeleccionarMesa = Utils.getNewDialog(this, R.layout.dialog_elegir_mesa, true, true, 1200, 650);

        btnOpenTable = (Button) findViewById(R.id.btnOpenTable);
        titleDialogSelectTable = (TextView) dialogSeleccionarMesa.findViewById(R.id.title);
        emptyGamesListTextView = (TextView) dialogSeleccionarMesa.findViewById(R.id.emptyGamesList);
        emptyTablesListTextView = (TextView) dialogSeleccionarMesa.findViewById(R.id.emptyTablesList);
        btnAccept = (Button) dialogSeleccionarMesa.findViewById(R.id.btnAccept);
        btnCancel = (Button) dialogSeleccionarMesa.findViewById(R.id.btnCancel);
        juegosListView = (ListView) dialogSeleccionarMesa.findViewById(R.id.gamesListView);
        mesasGridView = (GridView) dialogSeleccionarMesa.findViewById(R.id.tablesGridView);
        logo = (ImageView)findViewById(R.id.logo);

        listenerJuegos = this;
        listenerMesas = this;

        initListeners();




        dialogSeleccionarMesa.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                titleDialogSelectTable.setText(getResources().getString(R.string.title_select_game_table));

                setListsAdapter(juegosListView, null);
                setListsAdapter(mesasGridView, null);

                btnAccept.setEnabled(false);
            }
        });

        userType = GlobalData.USUARIO_JEFE_MESA;
    }

    private void goneViewHideen() {


        currentApiVersion = android.os.Build.VERSION.SDK_INT;

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;


        if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
        {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }

    }

    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }


    private void initListeners() {
        btnOpenTable.setOnClickListener(this);
        btnAccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        logo.setOnClickListener(this);
    }

    private void setAppVersion() {
        ((TextView) findViewById(R.id.versionApp)).setText(String.format("v%s%s", ConnectionServer.getAppEnvironment().substring(0, 1), Utils.getAppVersion(this)));
    }

    private void startCardAnimation() {
        Utils.startViewAnimation(this, findViewById(R.id.card), R.anim.tarjeta_login);
    }

    private void stopCardAnimation() {
        Utils.stopViewAnimation(findViewById(R.id.card));
    }

    @Override
    public boolean onKeyUp(int keyCode, @NonNull KeyEvent event) {
        String dig_tarjeta = String.valueOf(event.getDisplayLabel());

        if(Utils.isAlfanumeric(dig_tarjeta)) {
            digits += dig_tarjeta;
        }

        if(keyCode == KeyEvent.KEYCODE_ENTER) {
            try {

                // TODO: Descomentar al momento de mandar apk
                String id_tarjeta = digits.substring(GlobalData.MIN_CARD_INDEX, GlobalData.MAX_CARD_INDEX); //00000000153
//                String id_tarjeta = "00153";

                if(Utils.isAlfanumeric(id_tarjeta)) {
                    digits += id_tarjeta;
                }
                AsyncResponse asyncResponseGetUsuario = new AsyncResponse() {
                    @Override
                    public void onProcessFinish(Object result) {
                        UsuarioDM user = (UsuarioDM) result;

                        if (user != null) {
                            // Formatear el nombre del usuario recibido de la Base de Datos
                            user.setNombre_usuario(Utils.getOrderedUserName(user.getNombre_usuario()).toUpperCase(Utils.locale));

                            showUserAcceptedDialog(user.getNombre_usuario(), userType);

                            if (userType.equals(GlobalData.USUARIO_JEFE_MESA)) {
                                gd.setJefeMesa(user);

                                ((TextView) findViewById(R.id.jefeMesaName)).setText(String.format("%s: %s", getString(R.string.jefe_mesa_text), user.getNombre_usuario()));
                            } else if (userType.equals(GlobalData.USUARIO_CROUPIER)) {
                                gd.setCroupier(user);

                                ((TextView) findViewById(R.id.croupierName)).setText(String.format("%s: %s", getString(R.string.croupier_text), user.getNombre_usuario()));
                            }
                        } else {
                            Utils.showInformationDialog(LoginActivity.this, false, GlobalData.EMPTY, 1200);
                        }

                        clearVars();
                    }
                };

                GetUsuarioAsyncTask gu = new GetUsuarioAsyncTask(Utils.getNewProgressDialog(this, getString(R.string.getting_user_info), false), asyncResponseGetUsuario);

                gu.execute(GlobalData.ID_UNIDAD, id_tarjeta, userType);
            }
            catch(Exception e) {
                e.printStackTrace();

                clearVars();

                Utils.showInformationDialog(LoginActivity.this, false, GlobalData.EMPTY, 1200);
            }

            return false;
        }

        return super.onKeyUp(keyCode, event);

    }

    private void showUserAcceptedDialog(String userName, final String userType) {
        final Dialog dialog = Utils.getNewDialog(LoginActivity.this, R.layout.dialog_usuario_aceptado, false);

        ((TextView) dialog.findViewById(R.id.userName)).setText(userName);
        ((TextView) dialog.findViewById(R.id.userType)).setText(userType);

        dialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();

                if (userType.equals(GlobalData.USUARIO_JEFE_MESA)) {
                    changeUser();
                } else if (userType.equals(GlobalData.USUARIO_CROUPIER)) {
                    if (btnOpenTable.getVisibility() == View.GONE) {
                        LinearLayout ll = (LinearLayout) findViewById(R.id.slideCardLinearLayout);

                        stopCardAnimation();

                        Utils.startViewAnimation(getApplicationContext(), ll, R.anim.fade_out);

                        ll.setVisibility(View.GONE);

                        btnOpenTable.setVisibility(View.VISIBLE);

                        Utils.startViewAnimation(getApplicationContext(), btnOpenTable, R.anim.fade_in);
                    }
                }
            }
        }, 1200);
    }

    private void changeUser() {
        userType = GlobalData.USUARIO_CROUPIER;

        // Cambiar el texto "JEFE DE MESA" por "CROUPIER" cuando corresponda deslizar la tarjeta del croupier
        final TextView userTypeTv = (TextView) findViewById(R.id.userText);

        Animation left_out = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_out);

        userTypeTv.startAnimation(left_out);

        left_out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                userTypeTv.setText(getString(R.string.croupier_text));
                Utils.startViewAnimation(getApplicationContext(), userTypeTv, R.anim.right_in);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    public void onBackPressed() {
        // Comentar esta linea para que la app no se pueda cerrar con el boton "atras"
        finish();
    }

    public void clearVars() {
        digits = GlobalData.EMPTY;
    }

    private void clearData() {
        clearVars();

        LinearLayout ll = (LinearLayout) findViewById(R.id.slideCardLinearLayout);

        btnOpenTable.setVisibility(View.GONE);
        ll.setVisibility(View.VISIBLE);
        startCardAnimation();

        userType = GlobalData.USUARIO_JEFE_MESA;

        ((TextView) findViewById(R.id.userText)).setText(getString(R.string.jefe_mesa_text));

        ((TextView) findViewById(R.id.jefeMesaName)).setText(getString(R.string.jefe_mesa_text));
        ((TextView) findViewById(R.id.croupierName)).setText(getString(R.string.croupier_text));
    }

    @Override
    protected void onStop() {
        super.onStop();

        clearData();
    }

    @Override
    public void onClick(View v) {
        if(v == btnOpenTable) {
            AsyncResponse getMesasAsyncResponse = new AsyncResponse() {
                @Override
                public void onProcessFinish(Object result) {
                    ArrayList<TipoJuegoDM> tiposJuegos = (ArrayList<TipoJuegoDM>) result;

                    if(tiposJuegos != null) {
                        populateGamesList(tiposJuegos);

                        dialogSeleccionarMesa.show();
                    }
                    else {
                        Utils.toast(getApplicationContext(), GlobalData.ERROR_GETTING_TABLES);
                    }
                }
            };

            GetMesasAsyncTask gm = new GetMesasAsyncTask(Utils.getNewProgressDialog(this, getString(R.string.getting_tables_list), false), getMesasAsyncResponse);

            gm.execute(GlobalData.ID_UNIDAD);
        }
        else if(v == btnAccept) {
            minMaxValuesController = new MinMaxValuesController(LoginActivity.this, R.array.min_max_values, R.array.min_max_values);

            minMaxValuesController.showMinMaxDialog(new OnMinMaxValuesEnteredListener() {
                @Override
                public void onValuesEntered(final int minValue, final int maxValue) {
                    minMaxValuesController.setEnabledAcceptButton(false);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            minMaxValuesController.setEnabledAcceptButton(true);
                        }
                    }, 5000);

                    boolean success = Utils.getParamsFromDB(GlobalData.ID_UNIDAD);

                    if (success) {
                        if (minValue > 0 && maxValue > 0) {
//                            String macAddress = Utils.getMacAddress(getApplicationContext());

                            if(androidId != null && !androidId.isEmpty()) {
                                Logger.d("sesion= "+sesion);

                                if(sesion == null) {
                                    AsyncResponse createSessionAsyncResponse = new AsyncResponse() {
                                        @Override
                                        public void onProcessFinish(Object result) {
                                            sesion = (SesionDM) result;

                                            proceedWithCreatedSession(sesion, minValue, maxValue);
                                        }
                                    };

                                    ServicioMesas.CreateSessionAsyncTask cs = new ServicioMesas.CreateSessionAsyncTask(Utils.getNewProgressDialog(LoginActivity.this, R.string.creating_new_session, false), createSessionAsyncResponse);

                                    Logger.d("cs.execute(" + gd.getJefeMesa().getId_usuario() + ", " + gd.getCroupier().getId_usuario() + ", " + gd.getMesa().getId_mesa() + ", " + androidId + ")");

                                    cs.execute(gd.getJefeMesa().getId_usuario(), gd.getCroupier().getId_usuario(), gd.getMesa().getId_mesa(), androidId);
                                }
                                else {
                                    proceedWithCreatedSession(sesion, minValue, maxValue);
                                }
                            }
                            else {
                                minMaxValuesController.setEnabledAcceptButton(true);

                                Utils.showInformationDialog(LoginActivity.this, false, GlobalData.ERROR_GETTING_DEVICE_MAC_ADDRESS, 1200);
                            }
                        }
                        else {
                            minMaxValuesController.setEnabledAcceptButton(true);

                            Utils.showInformationDialog(LoginActivity.this, false, GlobalData.ERROR_VALUE_MIN_MAX_IS_ZERO, 1200);
                        }
                    }
                    else {
                        minMaxValuesController.setEnabledAcceptButton(true);

                        Utils.showInformationDialog(LoginActivity.this, false, GlobalData.ERROR_GETTING_PARAMS, 1200);
                    }
                }
            });
        }
        else if(v == btnCancel) {
            dialogSeleccionarMesa.dismiss();
        }else if(v == logo){

            cont++;
            if(cont>=5){

                Log.e("androidId",androidId);
                AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Android ID")
                        .setMessage(androidId)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

                TextView textView = (TextView) dialog.findViewById(android.R.id.message);
                textView.setTextSize(25f);

                cont=0;
            }

        }
    }

    private void proceedWithCreatedSession(SesionDM sesion, final int minValue, final int maxValue) {
        if (sesion != null) {
            if (Integer.parseInt(sesion.id_sesion) == -1) {
                LoginActivity.sesion = null;

                Utils.showInformationDialog(LoginActivity.this, false, GlobalData.ERROR_TABLET_NOT_REGISTERED_OR_DISABLED, 1200);
            }
            else {
                gd.getMesa().setId_sesion(sesion.id_sesion);
                gd.getMesa().setFecha_actual(sesion.fecha_actual);

                AsyncResponse updateMinMaxAsyncResponse = new AsyncResponse() {
                    @Override
                    public void onProcessFinish(Object result) {
                        int res = (int) result;

                        if (res == 1) {
                            AsyncResponse insertUserReliefAsyncResponse = new AsyncResponse() {
                                @Override
                                public void onProcessFinish(Object result) {
                                    int res = (int) result;

                                    if (res == 1) {
                                        minMaxValuesController.closeMinMaxDialog();

                                        dialogSeleccionarMesa.dismiss();

                                        gd.getMesa().setMin_mesa(minValue);
                                        gd.getMesa().setMax_mesa(maxValue);

                                        Intent mIntent = new Intent(getApplicationContext(), MesaActivity.class);

                                        mIntent.putExtra("global_data", gd);

                                        startActivity(mIntent);
                                    }
                                    else {
                                        minMaxValuesController.setEnabledAcceptButton(true);

                                        Utils.showInformationDialog(LoginActivity.this, false, GlobalData.ERROR_REGISTERING_RELIEF_OF_USER, 1200);
                                    }
                                }
                            };

                            ServicioMesas.InsertUserReliefAsyncTask iur = new ServicioMesas.InsertUserReliefAsyncTask(Utils.getNewProgressDialog(LoginActivity.this, R.string.registering_user_relief, false), insertUserReliefAsyncResponse);

                            iur.execute(gd.getJefeMesa().getId_usuario(), gd.getCroupier().getId_usuario(), gd.getMesa().getId_sesion());
                        }
                        else {
                            minMaxValuesController.setEnabledAcceptButton(true);

                            Utils.showInformationDialog(LoginActivity.this, false, GlobalData.ERROR_UPDATING_MIN_MAX, 1200);
                        }
                    }
                };

                Logger.d("id_jefe_mesa= " + gd.getJefeMesa().getId_usuario() +
                        "\nid_croupier= " + gd.getCroupier().getId_usuario() +
                        "\nid_sesion= " + gd.getMesa().getId_sesion() +
                        "\nid_mesa= " + gd.getMesa().getId_mesa() +
                        "\nmacAddress= " + Utils.getMacAddress(getApplicationContext()));

                ServicioMesas.UpdateMinMaxAsyncTask umm = new ServicioMesas.UpdateMinMaxAsyncTask(Utils.getNewProgressDialog(LoginActivity.this, R.string.updating_min_max_values, false), updateMinMaxAsyncResponse);

                umm.execute(gd.getMesa().getId_sesion(), String.valueOf(minValue), String.valueOf(maxValue));
            }
        }
        else {
            minMaxValuesController.setEnabledAcceptButton(true);

            Utils.showInformationDialog(LoginActivity.this, false, GlobalData.ERROR_CREATING_SESSION, 1200);
        }
    }

    public void onGameSelected(TipoJuegoDM selectedGame) {
        populateTablesList(selectedGame);
    }

    public void onTableSelected(MesaDM selectedTable) {
        updateUi(selectedTable);

        gd.setMesa(selectedTable);

        btnAccept.setEnabled(true);
    }

    public void updateUi(MesaDM selectedTable) {
        if (selectedTable == null) {
            return;
        }

        String tableName = selectedTable.getNombre_juego().toUpperCase(Utils.locale) + " <b>" + selectedTable.getNumero_mesa() + "</b>" ;

        titleDialogSelectTable.setText(Html.fromHtml(tableName));

        adapterMesas.notifyDataSetChanged();
    }

    private void populateGamesList(ArrayList<TipoJuegoDM> games) {
        TipoJuegoAdapter adapterJuegos = (!games.isEmpty()) ? new TipoJuegoAdapter(this, games) : null;

        setListsAdapter(juegosListView, adapterJuegos);

        juegosListView.setOnItemClickListener(new JuegosOnItemClickListener(listenerJuegos));
    }

    private void populateTablesList(TipoJuegoDM game) {
        if (game != null) {
            titleDialogSelectTable.setText(game.getNombre_tipo_juego().toUpperCase(Utils.locale));

            adapterMesas = (!game.getMesas().isEmpty()) ? new ListaMesasAdapter(getApplicationContext(), game.getMesas()) : null;

            setListsAdapter(mesasGridView, adapterMesas);

            mesasGridView.setOnItemClickListener(new MesasOnItemClickListener(listenerMesas));
        }
    }

    private void setListsAdapter(View v, ListAdapter adapter) {
        if(v == juegosListView) {
            emptyGamesListTextView.setVisibility((adapter == null) ? View.VISIBLE : View.GONE);

            juegosListView.setAdapter(adapter);
        }
        else if(v == mesasGridView) {
            emptyTablesListTextView.setVisibility((adapter == null) ? View.VISIBLE : View.GONE);

            mesasGridView.setAdapter(adapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // TODO: Eliminar prueba
//        testElun();
    }
}
