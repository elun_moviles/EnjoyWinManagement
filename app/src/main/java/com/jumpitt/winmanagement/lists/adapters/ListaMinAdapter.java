package com.jumpitt.winmanagement.lists.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.utils.GlobalData;
import com.jumpitt.winmanagement.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressLint("InflateParams")
public class ListaMinAdapter extends BaseAdapter {
    private final List<String> lista;
    private Activity activity;
    private Dialog dialog;

    public ListaMinAdapter(Activity activity, ArrayList<String> lista, Dialog dialog) {
        this.activity = activity;
        this.lista = lista;
        this.dialog = dialog;
    }

    public ListaMinAdapter(Activity activity, int lista, Dialog dialog) {
        this.activity = activity;
        this.lista = resourceToArray(activity, lista);
        this.dialog = dialog;
    }

    private List<String> resourceToArray(Context context, int arrayResource) {
        return Arrays.asList(context.getResources().getStringArray(arrayResource));
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_min_max, null);
        }
        else {
            v = convertView;
        }

        final String item = lista.get(position);

        final TextView min = (TextView) dialog.findViewById(R.id.minValue);
        final TextView max = (TextView) dialog.findViewById(R.id.maxValue);

        final Button value = (Button) v.findViewById(R.id.value);

        value.setText(Utils.moneyFormat(item));

        value.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int minimo, maximo;

                minimo = Utils.StrToInt(value.getText().toString());
                maximo = Utils.StrToInt(max.getText().toString());

                if (minimo >= maximo && maximo != 0) {
                    Utils.showInformationDialog(activity, false, GlobalData.ERROR_VALUE_MIN_IS_HIGHER_THAN_MAX, 1200);
                }
                else {
                    min.setText(Utils.moneyFormat(item));
                }
            }
        });

        return v;
    }
}