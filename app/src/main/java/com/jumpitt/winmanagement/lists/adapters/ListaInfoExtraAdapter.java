package com.jumpitt.winmanagement.lists.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.ModificadorCompDM;
import com.jumpitt.winmanagement.ui.comps.TipoModificadorComps;

import java.util.List;

public class ListaInfoExtraAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ModificadorCompDM> list;
    private Context context;

    public ListaInfoExtraAdapter(Context context, List<ModificadorCompDM> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewIngrediente = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_info_adicional_ingrediente, parent, false);
        View viewAgregado = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_info_adicional_agregado, parent, false);

        if(viewType == 2) {
            return new ModificadorCompViewHolder(viewAgregado);
        }

        return new ModificadorCompViewHolder(viewIngrediente);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ModificadorCompDM modificadorComp = list.get(position);
        ((ModificadorCompViewHolder) holder).populateView(modificadorComp, context);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getTipoModificador().ordinal();
    }

    @SuppressWarnings("deprecation")
    public static class ModificadorCompViewHolder extends RecyclerView.ViewHolder {
        protected TextView title;

        public ModificadorCompViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
        }

        public void populateView(ModificadorCompDM modificadorComp, Context context) {
            if (modificadorComp.isSelected()) {
                title.setText(modificadorComp.getNombreModificador());

                if(modificadorComp.getTipoModificador() == TipoModificadorComps.INGREDIENTE) {
                    title.setTextColor(context.getResources().getColor(R.color.bg_btn_ingredients));
                    title.setBackgroundResource(R.drawable.shape_button_ingredient_selected);
                }
                else {
                    title.setTextColor(context.getResources().getColor(R.color.bg_btn_aditional_info));
                    title.setBackgroundResource(R.drawable.shape_button_additional_info_selected);
                }
            }
            else {
                title.setText(modificadorComp.getNombreModificador());
                title.setTextColor(context.getResources().getColor(R.color.blanco));

                if(modificadorComp.getTipoModificador() == TipoModificadorComps.INGREDIENTE) {
                    title.setBackgroundColor(context.getResources().getColor(R.color.bg_btn_ingredients));
                }
                else {
                    title.setBackgroundColor(context.getResources().getColor(R.color.bg_btn_aditional_info));

                }
            }
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}

