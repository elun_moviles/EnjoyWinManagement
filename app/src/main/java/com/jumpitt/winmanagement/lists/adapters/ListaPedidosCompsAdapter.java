package com.jumpitt.winmanagement.lists.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.lists.classes.ItemPedidoComp;
import com.jumpitt.winmanagement.ui.comps.CompsActivity;
import com.jumpitt.winmanagement.ui.comps.TipoModificadorComps;
import com.jumpitt.winmanagement.utils.GlobalData;
import com.jumpitt.winmanagement.utils.interfaces.OnAdditionalInfoButtonPressedListener;

import java.util.ArrayList;

@SuppressLint("InflateParams")
public class ListaPedidosCompsAdapter extends BaseAdapter {
    private final ArrayList<ItemPedidoComp> lista;
    private Activity activity;
    private OnAdditionalInfoButtonPressedListener listener;

    public ListaPedidosCompsAdapter(ArrayList<ItemPedidoComp> lista, Activity activity, OnAdditionalInfoButtonPressedListener listener) {
        this.lista = lista;
        this.activity = activity;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    public ItemPedidoComp getItemById(String idPedido) {
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getId_pedido().equals(idPedido)) {
                return lista.get(i);
            }
        }

        return null;
    }

    public int getItemPosition(ItemPedidoComp itemPedidoComp) {
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getId_pedido().equals(itemPedidoComp.getId_pedido())) {
                return i;
            }
        }

        return -1;
    }

    public Object remove(int position) {
        return lista.remove(position);
    }

    public void remove(ItemPedidoComp object) {
        lista.remove(object);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateContIngredientes(int position, int increment) {
        lista.get(position).contIngredientes += increment;

        if (lista.get(position).contIngredientes < 0) lista.get(position).contIngredientes = 0;

        notifyDataSetChanged();
    }

    public void updateContInfoExtra(int position, int increment) {
        lista.get(position).contInfoExtra += increment;

        if (lista.get(position).contInfoExtra < 0) lista.get(position).contInfoExtra = 0;

        notifyDataSetChanged();
    }

    public void setContIngredientes(int position, int value) {
        lista.get(position).contIngredientes = value;

        if (value < 0) lista.get(position).contIngredientes = 0;

        notifyDataSetChanged();
    }

    public void setContInfoExtra(int position, int value) {
        lista.get(position).contInfoExtra = value;

        if (value < 0) lista.get(position).contInfoExtra = 0;

        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_lista_pedidos_comps, null);
        } else {
            v = convertView;
        }

        TextView cantidadProd = (TextView) v.findViewById(R.id.cantidadComp);
        TextView nombreProd = (TextView) v.findViewById(R.id.nombreComp);
        ImageButton btnInfoAdicional = (ImageButton) v.findViewById(R.id.btnInfoAdicional);
        TextView emptyInfoList = (TextView) v.findViewById(R.id.emptyInfoList);

        final ItemPedidoComp item = lista.get(position);

        item.setEmptyInfoList(emptyInfoList);

        String strBrabs = GlobalData.EMPTY;

        if (item.listaIngredientes != null && item.listaIngredientes.size() > 0) {
            for (int i = 0; i < item.listaIngredientes.size(); i++) {
                if (item.listaIngredientes.get(i).isSelected()) {
                    if (strBrabs.isEmpty()) {
                        strBrabs = CompsActivity.capitalizeWord(item.listaIngredientes.get(i).getNombreModificador(), TipoModificadorComps.INGREDIENTE);
                    }
                    else {
                        if (item.listaIngredientes.get(i).isSelected()) {
                            strBrabs = strBrabs + " - " + CompsActivity.capitalizeWord(item.listaIngredientes.get(i).getNombreModificador(), TipoModificadorComps.INGREDIENTE);
                        }
                    }
                }
            }
        }

        if (item.listaInfoExtra != null && item.listaInfoExtra.size() > 0) {
            for (int i = 0; i < item.listaInfoExtra.size(); i++) {
                if (item.listaInfoExtra.get(i).isSelected()) {
                    if (strBrabs.isEmpty()) {
                        strBrabs = CompsActivity.capitalizeWord(item.listaInfoExtra.get(i).getNombreModificador(), TipoModificadorComps.INFO_EXTRA);
                    }
                    else {
                        strBrabs = strBrabs + " - " + CompsActivity.capitalizeWord(item.listaInfoExtra.get(i).getNombreModificador(), TipoModificadorComps.INFO_EXTRA);
                    }
                }
            }
        }

        item.setTextEmptyInfoList(strBrabs.isEmpty() ? activity.getString(R.string.empty_additional_info_list) : strBrabs);

        int cantProd = item.cantidad_producto;

        cantidadProd.setText((cantProd < 10) ? "0" + cantProd : String.valueOf(cantProd));
        nombreProd.setText(item.nombre_producto);

        btnInfoAdicional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onAddButtonPressed(item.getId_pedido());
                }
            }
        });

        return v;
    }

}