package com.jumpitt.winmanagement.lists.classes;

public class ItemListaComp
{
	private String idProd1;
	private String nombreProd1;
	private long precioProd1;
	private boolean permiteIngredientesProd1;
	private boolean permiteInfoExtraProd1;
	private String idProd2;
	private String nombreProd2;
	private long precioProd2;
	private boolean permiteIngredientesProd2;
	private boolean permiteInfoExtraProd2;
	
	public String getIdProd1()
	{
		return idProd1;
	}
	
	public void setIdProd1(String idProd1)
	{
		this.idProd1 = idProd1;
	}
	
	public String getNombreProd1()
	{
		return nombreProd1;
	}
	
	public void setNombreProd1(String nombreProd1)
	{
		this.nombreProd1 = nombreProd1;
	}
	
	public long getPrecioProd1()
	{
		return precioProd1;
	}

	public void setPrecioProd1(long precioProd1)
	{
		this.precioProd1 = precioProd1;
	}

	public boolean isPermiteIngredientesProd1() {
		return permiteIngredientesProd1;
	}

	public void setPermiteIngredientesProd1(boolean permiteIngredientesProd1) {
		this.permiteIngredientesProd1 = permiteIngredientesProd1;
	}

	public boolean isPermiteInfoExtraProd1() {
		return permiteInfoExtraProd1;
	}

	public void setPermiteInfoExtraProd1(boolean permiteInfoExtraProd1) {
		this.permiteInfoExtraProd1 = permiteInfoExtraProd1;
	}

	public String getIdProd2()
	{
		return idProd2;
	}
	
	public void setIdProd2(String idProd2)
	{
		this.idProd2 = idProd2;
	}
	
	public String getNombreProd2()
	{
		return nombreProd2;
	}
	
	public void setNombreProd2(String nombreProd2)
	{
		this.nombreProd2 = nombreProd2;
	}

	public long getPrecioProd2()
	{
		return precioProd2;
	}

	public void setPrecioProd2(long precioProd2)
	{
		this.precioProd2 = precioProd2;
	}

	public boolean isPermiteIngredientesProd2() {
		return permiteIngredientesProd2;
	}

	public void setPermiteIngredientesProd2(boolean permiteIngredientesProd2) {
		this.permiteIngredientesProd2 = permiteIngredientesProd2;
	}

	public boolean isPermiteInfoExtraProd2() {
		return permiteInfoExtraProd2;
	}

	public void setPermiteInfoExtraProd2(boolean permiteInfoExtraProd2) {
		this.permiteInfoExtraProd2 = permiteInfoExtraProd2;
	}
}
