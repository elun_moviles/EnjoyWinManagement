package com.jumpitt.winmanagement.lists.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.MesaDM;

import java.util.List;

public class ListaMesasAdapter extends ArrayAdapter<MesaDM> {
    Resources res;
    LayoutInflater inflater;

    public ListaMesasAdapter(Context c, List<MesaDM> mesas) {
        super(c, R.layout.item_lista_mesas, mesas);

        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        res = c.getResources();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MesaDM mesa = getItem(position);

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.item_lista_mesas, parent, false);
        }

        TextView nombreMesa = (TextView) convertView.findViewById(R.id.name_table);

        nombreMesa.setDuplicateParentStateEnabled(true);
        nombreMesa.setText(mesa.getNombre_completo_mesa());

        if (mesa.isSelected()) {
            nombreMesa.setBackgroundResource(R.drawable.shape_item_grid_table_checked);
            nombreMesa.setTextColor(res.getColor(R.color.blanco));
        }
        else {
            nombreMesa.setBackgroundResource(R.drawable.bg_item_grid_table_selector);
            nombreMesa.setTextColor(res.getColorStateList(R.color.table_text_state_color_list));
        }

        return convertView;
    }
}
