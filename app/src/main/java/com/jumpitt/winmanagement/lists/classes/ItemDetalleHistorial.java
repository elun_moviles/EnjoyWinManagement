package com.jumpitt.winmanagement.lists.classes;

public class ItemDetalleHistorial {
	String fechaDetalle;
	String horaDetalle;
	String tipoDetalle;
	String costoDetalle;
	String infoDetalle;
	
	public String getFechaDetalle() {
		return fechaDetalle;
	}
	public void setFechaDetalle(String fechaDetalle) {
		this.fechaDetalle = fechaDetalle;
	}
	public String getHoraDetalle() {
		return horaDetalle;
	}
	public void setHoraDetalle(String horaDetalle) {
		this.horaDetalle = horaDetalle;
	}
	public String getTipoDetalle() {
		return tipoDetalle;
	}
	public void setTipoDetalle(String tipoDetalle) {
		this.tipoDetalle = tipoDetalle;
	}
	public String getCostoDetalle() {
		return costoDetalle;
	}
	public void setCostoDetalle(String costoDetalle) {
		this.costoDetalle = costoDetalle;
	}
	public String getInfoDetalle() {
		return infoDetalle;
	}
	public void setInfoDetalle(String infoDetalle) {
		this.infoDetalle = infoDetalle;
	}
}
