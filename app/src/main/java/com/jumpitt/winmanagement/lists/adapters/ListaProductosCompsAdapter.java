package com.jumpitt.winmanagement.lists.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.lists.classes.ItemListaComp;
import com.jumpitt.winmanagement.lists.classes.SelectedItemListaComp;
import com.jumpitt.winmanagement.utils.interfaces.OnProductSelectedListener;

import java.util.ArrayList;

@SuppressLint("InflateParams")
public class ListaProductosCompsAdapter extends BaseAdapter {
    private final ArrayList<ItemListaComp> lista;
    private Activity activity;
    private OnProductSelectedListener listener;

    public ListaProductosCompsAdapter(ArrayList<ItemListaComp> lista, Activity activity, OnProductSelectedListener onProductSelectedListener) {
        this.lista = lista;
        this.activity = activity;
        this.listener = onProductSelectedListener;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_lista_productos_comps, null);
        }
        else {
            v = convertView;
        }

        TextView nombreProd1 = (TextView) v.findViewById(R.id.nombreProd1);
        TextView nombreProd2 = (TextView) v.findViewById(R.id.nombreProd2);

        final ItemListaComp item = lista.get(position);

        nombreProd1.setText(item.getNombreProd1());
        nombreProd2.setText(item.getNombreProd2());

        nombreProd1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onProductSelected(v, new SelectedItemListaComp(item.getIdProd1(), item.getNombreProd1(), item.getPrecioProd1(), item.isPermiteIngredientesProd1(), item.isPermiteInfoExtraProd1()));
            }
        });

        nombreProd2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onProductSelected(v, new SelectedItemListaComp(item.getIdProd2(), item.getNombreProd2(), item.getPrecioProd2(), item.isPermiteIngredientesProd2(), item.isPermiteInfoExtraProd2()));
            }
        });

        return v;
    }
}