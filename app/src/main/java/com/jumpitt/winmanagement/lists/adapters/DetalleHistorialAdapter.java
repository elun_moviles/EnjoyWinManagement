package com.jumpitt.winmanagement.lists.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.ItemDetalleHistorialDM;
import com.jumpitt.winmanagement.utils.Utils;

import java.util.ArrayList;

@SuppressLint("InflateParams")
public class DetalleHistorialAdapter extends BaseAdapter
{
	private final ArrayList<ItemDetalleHistorialDM> lista;
	private Context c;

	public DetalleHistorialAdapter(Context c, ArrayList<ItemDetalleHistorialDM> lista)
	{
		this.lista = lista;
		this.c = c;
	}

	@Override
	public int getCount()
	{
		return lista.size();
	}

	@Override
	public Object getItem(int position)
	{
		return lista.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;

        if (v == null)
        {
           LayoutInflater vi = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           v = vi.inflate(R.layout.item_detalle_historial, null);
        }
        else
        {
        	v = convertView;
        }

        TextView idDetalle = (TextView) v.findViewById(R.id.idDetalle);
		TextView fechaDetalle = (TextView) v.findViewById(R.id.fechaDetalle);
		TextView horaDetalle = (TextView) v.findViewById(R.id.horaDetalle);
		TextView tipoDetalle = (TextView) v.findViewById(R.id.tipoDetalle);
		TextView costoDetalle = (TextView) v.findViewById(R.id.costoDetalle);
		TextView infoDetalle = (TextView) v.findViewById(R.id.infoDetalle);

		ItemDetalleHistorialDM item = lista.get(position);

		item.fecha_operacion = item.fecha_operacion.replace("T", " ");

		idDetalle.setText(String.valueOf(item.numero_operacion));
		fechaDetalle.setText(Utils.formatDate(item.fecha_operacion, "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy"));
		horaDetalle.setText(Utils.formatDate(item.fecha_operacion, "yyyy-MM-dd HH:mm:ss", "HH:mm:ss"));
		tipoDetalle.setText(item.tipo_operacion);
		costoDetalle.setText((item.costo == 0) ? c.getString(R.string.triple_dash) : Utils.moneyFormat(item.costo));
		infoDetalle.setText(item.info_adicional_operacion);

		return v;
	}

}