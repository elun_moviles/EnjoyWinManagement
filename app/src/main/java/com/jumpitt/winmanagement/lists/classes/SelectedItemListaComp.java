package com.jumpitt.winmanagement.lists.classes;

import com.jumpitt.winmanagement.utils.GlobalData;

public class SelectedItemListaComp {
    private String id_producto;
    private String nombre_producto;
    private long precio_producto;
    private boolean permite_ingredientes;
    private boolean permite_info_extra;

    public SelectedItemListaComp() {
        this.id_producto = GlobalData.EMPTY;
        this.nombre_producto = GlobalData.EMPTY;
        this.precio_producto = 0;
        this.permite_ingredientes = false;
        this.permite_info_extra = false;
    }

    public SelectedItemListaComp(String id_producto, String nombre_producto, long precio_producto,
                                 boolean permite_ingredientes, boolean permite_info_extra) {
        this.id_producto = id_producto;
        this.nombre_producto = nombre_producto;
        this.precio_producto = precio_producto;
        this.permite_ingredientes = permite_ingredientes;
        this.permite_info_extra = permite_info_extra;
    }

    public String getId_producto() {
        return id_producto;
    }

    public void setId_producto(String id_producto) {
        this.id_producto = id_producto;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public long getPrecio_producto() {
        return precio_producto;
    }

    public void setPrecio_producto(long precio_producto) {
        this.precio_producto = precio_producto;
    }

    public boolean isPermite_ingredientes() {
        return permite_ingredientes;
    }

    public void setPermite_ingredientes(boolean permite_ingredientes) {
        this.permite_ingredientes = permite_ingredientes;
    }

    public boolean isPermite_info_extra() {
        return permite_info_extra;
    }

    public void setPermite_info_extra(boolean permite_info_extra) {
        this.permite_info_extra = permite_info_extra;
    }
}