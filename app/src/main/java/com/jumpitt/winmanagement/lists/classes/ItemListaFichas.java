package com.jumpitt.winmanagement.lists.classes;

import android.widget.TextView;

public class ItemListaFichas {
	int cantFichas;
	int contNumPres;
	boolean contNumPresFull;
	String valorFichas;
	String subtotal;
	TextView cantF;
	TextView valorF;
	TextView subtotalF;
	
	public int getCantFichas() {
		return cantFichas;
	}
	
	public void setCantFichas(int cantFichas) {
		this.cantFichas = cantFichas;
	}
	
	public String getValorFichas() {
		return valorFichas;
	}
	
	public void setValorFichas(String valorFichas) {
		this.valorFichas = valorFichas;
	}
	
	public String getSubtotal() {
		return subtotal;
	}
	
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}

	public TextView getCantF() {
		return cantF;
	}

	public void setCantF(TextView cantF) {
		this.cantF = cantF;
	}

	public TextView getValorF() {
		return valorF;
	}

	public void setValorF(TextView valorF) {
		this.valorF = valorF;
	}

	public TextView getSubtotalF() {
		return subtotalF;
	}

	public void setSubtotalF(TextView subtotalF) {
		this.subtotalF = subtotalF;
	}

	public int getContNumPres() {
		return contNumPres;
	}

	public void setContNumPres(int contNumPres) {
		this.contNumPres = contNumPres;
	}

	public boolean isContNumPresFull() {
		return contNumPresFull;
	}

	public void setContNumPresFull(boolean contNumPresFull) {
		this.contNumPresFull = contNumPresFull;
	}
}
