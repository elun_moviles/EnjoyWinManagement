package com.jumpitt.winmanagement.lists.classes;

import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.jumpitt.winmanagement.dataModel.ModificadorCompDM;
import com.jumpitt.winmanagement.utils.GlobalData;
import com.jumpitt.winmanagement.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;

public class ItemPedidoComp {
    private String id_pedido;
    public String id_producto;
    public String nombre_producto;
    public int cantidad_producto;
    public int contIngredientes;
    public int contInfoExtra;
    public long precio_producto;
    public long precio_total_productos;
    public boolean permite_ingredientes;
    public boolean permite_info_extra;
    public ArrayList<ModificadorCompDM> listaIngredientes;
    public ArrayList<ModificadorCompDM> listaInfoExtra;
    private TextView emptyInfoList;

    public ItemPedidoComp() {
        this.id_pedido = GlobalData.EMPTY;
        this.id_producto = GlobalData.EMPTY;
        this.nombre_producto = GlobalData.EMPTY;
        this.cantidad_producto = 1;
        this.contIngredientes = 0;
        this.contInfoExtra = 0;
        this.precio_producto = 0;
        this.precio_total_productos = 0;
        this.listaIngredientes = new ArrayList<>();
        this.listaInfoExtra = new ArrayList<>();
    }

    public ItemPedidoComp(String id_pedido, String id_producto, String nombre_producto, int cantidad_producto,
                          int contIngredientes, int contInfoExtra, long precio_producto, long precio_total_productos,
                          boolean permite_ingredientes, boolean permite_info_extra,
                          ArrayList<ModificadorCompDM> listaIngredientes, ArrayList<ModificadorCompDM> listaInfoExtra,
                          TextView emptyInfoList) {
        this.id_pedido = id_pedido;
        this.id_producto = id_producto;
        this.nombre_producto = nombre_producto;
        this.cantidad_producto = cantidad_producto;
        this.contIngredientes = contIngredientes;
        this.contInfoExtra = contInfoExtra;
        this.precio_producto = precio_producto;
        this.precio_total_productos = precio_total_productos;
        this.permite_ingredientes = permite_ingredientes;
        this.permite_ingredientes = permite_info_extra;
        this.listaIngredientes = listaIngredientes;
        this.listaInfoExtra = listaInfoExtra;
        this.emptyInfoList = emptyInfoList;
    }

    public TextView getEmptyInfoList() {
        return emptyInfoList;
    }

    public void setEmptyInfoList(TextView emptyInfoList) {
        this.emptyInfoList = emptyInfoList;
    }

    public void hideTextEmptyInfoList() {
        emptyInfoList.setVisibility(View.GONE);
    }

    public void showTextEmptyInfoList() {
        emptyInfoList.setVisibility(View.VISIBLE);
    }

    public void setTextEmptyInfoList(String text) {
        emptyInfoList.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
    }

    public void generarIdPedido() {
        Calendar cal = Calendar.getInstance();

        long timeInMillis = cal.getTimeInMillis();

        this.id_pedido = Utils.SHA1(String.valueOf(timeInMillis));
    }

    public String getId_pedido() {
        return id_pedido;
    }
}
