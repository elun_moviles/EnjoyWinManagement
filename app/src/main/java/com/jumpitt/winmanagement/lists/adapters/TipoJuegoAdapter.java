package com.jumpitt.winmanagement.lists.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jumpitt.winmanagement.R;
import com.jumpitt.winmanagement.dataModel.TipoJuegoDM;
import com.jumpitt.winmanagement.utils.Utils;

import java.util.ArrayList;

public class TipoJuegoAdapter extends ArrayAdapter<TipoJuegoDM> {
    LayoutInflater inflater;

    public TipoJuegoAdapter(Context c, ArrayList<TipoJuegoDM> juegos) {
        super(c, R.layout.item_lista_juegos, juegos);

        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TipoJuegoDM juego = getItem(position);

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.item_lista_juegos, parent, false);
        }

        TextView nombreJuego = (TextView) convertView.findViewById(R.id.gameName);

        nombreJuego.setText(juego.getNombre_tipo_juego().toUpperCase(Utils.locale));

        return convertView;
    }
}
